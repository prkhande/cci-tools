#!/usr/bin/python3

import ccitools.conf
import datetime
import logging
import sys


from ccitools.cmd.accounting.query import runner
from ccitools.cmd.accounting import utils
from ccitools.cmd import base
from influxdb import InfluxDBClient

# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF


class AccountingDBReporter(base.BaseCMD):
    def __init__(self):
        super(AccountingDBReporter, self).__init__(
            description=("Generates daily metrics for accounting tables"))

    def main(self, args):
        self.parser.add_argument(
            '--date',
            dest='str_date',
            help='Date that will be used by the '
                 'DBReporter extractor (DD-MM-YYYY format).',
            default=datetime.date.today().strftime('%d-%m-%Y')
        )

        args = self.parse_args(args)

        logger.info("Connecting to influxdb")
        influx_client = InfluxDBClient(
            CONF.influxdb.host,
            CONF.influxdb.port,
            CONF.influxdb.user,
            CONF.influxdb.password,
            CONF.influxdb.database,
            ssl=True
        )

        logger.info("Connecting to accounting bucket")
        s3_bucket = utils.get_bucket(
            CONF.dbreporter.access_key,
            CONF.dbreporter.secret_key,
            CONF.dbreporter.host
        )

        date = datetime.datetime.strptime(args.str_date, '%d-%m-%Y')

        influx_date = date.strftime('\'%Y-%m-%d\'')
        s3_date = date.strftime('%Y/%m/%d')
        query_runner = runner.QueryRunner(
            influx_client,
            s3_bucket,
            influx_date,
            s3_date)

        logger.info("Running stats_per_cell")
        query_runner.stats_per_cell()

        logger.info("Running stats_per_project")
        query_runner.stats_per_project()

        logger.info("Running stats_per_acc_group")
        query_runner.stats_per_acc_group()

        logger.info("Running stats_per_chargegroup")
        query_runner.stats_per_chargegroup()

        logger.info("Running magnum clusters")
        query_runner.magnum_clusters()

        logger.info("Running volume types")
        query_runner.volume_types()

        logger.info("Running volume stats per project")
        query_runner.volume_stats_per_project()

        logger.info("Running share stats per project")
        query_runner.share_stats_per_project()

        logger.info("Execution finished")


# Needs static method for setup.cfg
def main(args=None):
    AccountingDBReporter().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(-1)
