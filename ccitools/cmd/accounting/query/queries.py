CELL_USAGE = """
SELECT last(used) AS used,
       last(total) as total,
       last(used) / last(total) * 100 AS percentage
FROM hypervisors_cpu_per_cell,
     hypervisors_ram_per_cell,
     hypervisors_disk_per_cell
WHERE time >= {0} - 1h AND time < {0} - 1h + 10m
GROUP BY cell
"""

CELL_NODES = """
SELECT last(disabled) AS disabled,
       last(total) AS total,
       last(disabled) / last(total) * 100 AS percentage
FROM hypervisors_per_cell
WHERE time >= {0} - 1h AND time < {0} - 1h + 10m
GROUP BY cell
"""

CELL_VMS = """
SELECT last(count) AS count
FROM vms_per_cell
WHERE time >= {0} - 1h AND time < {0} - 1h + 10m
GROUP BY cell
"""

PROJECT_STATS = [
    """
    SELECT last(usage) AS usage,
           last(quota) AS quota,
           last(usage) / last(quota) * 100 AS percentage
    FROM instances_quota_per_project
    WHERE time >= {0} - 1h AND time < {0} - 1h + 10m
          AND accounting_group != 'unknown'
    GROUP BY project_name, accounting_group, chargegroup, chargerole
    """,
    """
    SELECT last(usage) AS usage,
           last(quota) AS quota,
           last(usage) / last(quota) * 100 AS percentage
    FROM cores_quota_per_project
    WHERE time >= {0} - 1h AND time < {0} - 1h + 10m
          AND accounting_group != 'unknown'
    GROUP BY project_name, accounting_group, chargegroup, chargerole
    """,
    """
    SELECT last(usage) AS usage,
           last(quota) AS quota,
           last(usage) / last(quota) * 100 AS percentage
    FROM snapshots_quota_per_project
    WHERE time >= {0} - 1h AND time < {0} - 1h + 10m
          AND accounting_group != 'unknown'
    GROUP BY project_name, accounting_group, chargegroup, chargerole
    """,
    """
    SELECT last(usage) AS usage,
           last(quota) AS quota,
           last(usage) / last(quota) * 100 AS percentage
    FROM volumes_quota_per_project
    WHERE time >= {0} - 1h AND time < {0} - 1h + 10m
          AND accounting_group != 'unknown'
    GROUP BY project_name, accounting_group, chargegroup, chargerole
    """,
    """
    SELECT last(usage) AS usage,
           last(quota) AS quota,
           last(usage) / last(quota) * 100 AS percentage
    FROM volumes_gb_quota_per_project
    WHERE time >= {0} - 1h AND time < {0} - 1h + 10m
          AND accounting_group != 'unknown'
    GROUP BY project_name, accounting_group, chargegroup, chargerole
    """,
    """
    SELECT last(usage) AS usage,
           last(quota) AS quota,
           last(usage) / last(quota) * 100 AS percentage
    FROM ram_quota_per_project
    WHERE time >= {0} - 1h AND time < {0} - 1h + 10m
          AND accounting_group != 'unknown'
    GROUP BY project_name, accounting_group, chargegroup, chargerole
    """
]

ACC_GROUP_STATS = """
SELECT last(usage) AS usage,
       last(quota) AS quota,
       last(usage) / last(quota) * 100 AS percentage
FROM instances_quota_per_acc_group_and_proj_type,
     cores_quota_per_acc_group_and_proj_type,
     volumes_quota_per_acc_group_and_proj_type,
     volumes_gb_quota_per_acc_group_and_proj_type
WHERE time >= {0} - 1h AND time < {0} - 1h + 10m
GROUP BY accounting_group, project_type
"""

CHARGEGROUP_STATS = """
SELECT last(usage) AS usage,
       last(quota) AS quota,
       last(usage) / last(quota) * 100 AS percentage
FROM instances_quota_per_chargegroup_and_chargerole,
     cores_quota_per_chargegroup_and_chargerole,
     volumes_quota_per_chargegroup_and_chargerole,
     volumes_gb_quota_per_chargegroup_and_chargerole
WHERE time >= {0} - 1h AND time < {0} - 1h + 10m
GROUP BY chargegroup, chargerole
"""

MAGNUM_CLUSTERS = """
SELECT last(count) AS count
FROM container_clusters_per_orchestration_engine
WHERE time >= {0} - 1h AND time < {0} - 1h + 10m
GROUP BY engine
"""

VOLUME_TYPES = """
SELECT last(size) AS size,
       last(count) AS count
FROM volumes_per_type
WHERE time >= {0} - 1h AND time < {0} - 1h + 10m
GROUP BY type
"""

VOLUME_STATS_PER_PROJECT = [
    """
    SELECT last("usage") AS "usage",
           last("quota") as "quota",
           last("usage") / last("quota") * 100 AS percentage
    FROM volumes_gb_quota_per_type_per_project
    WHERE time >= {0} - 1h AND time < {0} - 1h + 10m
    GROUP BY project_name, accounting_group, chargegroup, chargerole, type
    """,
    """
    SELECT last("usage") AS "usage",
           last("quota") as "quota",
           last("usage") / last("quota") * 100 AS percentage
    FROM volumes_quota_per_type_per_project
    WHERE time >= {0} - 1h AND time < {0} - 1h + 10m
    GROUP BY project_name, accounting_group, chargegroup, chargerole, type
    """
]

SHARES_STATS_PER_PROJECT = [
    """
    SELECT last("usage") AS "usage",
           last("quota") as "quota",
           last("usage") / last("quota") * 100 AS percentage
    FROM fileshares_gb_quota_per_type_per_project
    WHERE time >= {0} - 1h AND time < {0} - 1h + 10m
    GROUP BY project_name, accounting_group, chargegroup, chargerole,
             share_type
    """,
    """
    SELECT last("usage") AS "usage",
           last("quota") as "quota",
           last("usage") / last("quota") * 100 AS percentage
    FROM fileshares_quota_per_type_per_project
    WHERE time >= {0} - 1h AND time < {0} - 1h + 10m
    GROUP BY project_name, accounting_group, chargegroup, chargerole,
             share_type
    """
]

TABLES_TO_TITLES = {
    'hypervisors_cpu_per_cell': 'cpu_cores',
    'hypervisors_ram_per_cell': 'ram_usage',
    'hypervisors_disk_per_cell': 'disk_usage',
    'hypervisors_per_cell': 'compute_nodes',
    'vms_per_cell': 'total_vms',
    'volumes_quota_per_project': 'volumes_usage',
    'volumes_quota_per_acc_group_and_proj_type': 'volumes_usage',
    'volumes_gb_quota_per_project': 'volumes_size',
    'volumes_gb_quota_per_acc_group_and_proj_type': 'volumes_size',
    'snapshots_quota_per_project': 'snapshots_usage',
    'cores_quota_per_project': 'cores_usage',
    'cores_quota_per_acc_group_and_proj_type': 'cores_usage',
    'instances_quota_per_project': 'instances_usage',
    'instances_quota_per_acc_group_and_proj_type': 'instances_usage',
    'container_clusters_per_orchestration_engine': 'magnum_clusters',
    'volumes_per_type': 'volume_types',
    'ram_quota_per_project': 'ram_usage',
    'volumes_gb_quota_per_type_per_project': 'volume_gb',
    'volumes_quota_per_type_per_project': 'volume_number',
    'fileshares_gb_quota_per_type_per_project': 'share_gb',
    'fileshares_quota_per_type_per_project': 'share_number',
    'instances_quota_per_chargegroup_and_chargerole': 'instances_usage',
    'cores_quota_per_chargegroup_and_chargerole': 'cores_usage',
    'volumes_quota_per_chargegroup_and_chargerole': 'volumes_usage',
    'volumes_gb_quota_per_chargegroup_and_chargerole': 'volumes_size'
}
