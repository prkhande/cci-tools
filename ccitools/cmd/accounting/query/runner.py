import collections
import json

from ccitools.cmd.accounting import config
from ccitools.cmd.accounting.query.handler import QueryHandler
from ccitools.cmd.accounting.query import queries
from ccitools.cmd.accounting import utils


class QueryRunner(object):
    """Class in charge of running each of the DB reporter queries.

    Args:
        influx_client: client used to make the queries.
        s3_bucket: S3 bucket where the results will be written.
        influx_date: date formatted to make the query.
        s3_date: date formatted to build the S3 key.
    """

    def __init__(self, influx_client, s3_bucket, influx_date, s3_date):
        self.influx_client = influx_client
        self.s3_bucket = s3_bucket
        self.influx_date = influx_date
        self.s3_date = s3_date

    def stats_per_cell(self):
        """Run the stats per cell and writes the output into S3."""
        influx_dt = self.influx_date
        cell_query = QueryHandler(self.influx_client,
                                  [
                                      queries.CELL_USAGE.format(influx_dt),
                                      queries.CELL_NODES.format(influx_dt),
                                      queries.CELL_VMS.format(influx_dt)
                                  ],
                                  ['cell']).run()

        utils.write_s3(self.s3_bucket,
                       config.PATH_STATS_PER_CELL.format(self.s3_date),
                       json.dumps(cell_query, indent=2))

    def stats_per_project(self):
        """Run the stats per project and writes the output into S3."""
        influx_dt = self.influx_date
        stats_queries = [query.format(influx_dt)
                         for query in queries.PROJECT_STATS]
        project_stats = QueryHandler(self.influx_client,
                                     stats_queries,
                                     [
                                         'project_name',
                                         'accounting_group',
                                         'chargegroup',
                                         'chargerole',
                                     ]).run()
        utils.write_s3(self.s3_bucket,
                       config.PATH_STATS_PER_PROJECT.format(self.s3_date),
                       json.dumps(project_stats, indent=2))

    def stats_per_acc_group(self):
        """Run the stats per accounting group and writes the output into S3."""
        influx_dt = self.influx_date
        acc_query = QueryHandler(self.influx_client,
                                 [queries.ACC_GROUP_STATS.format(influx_dt)],
                                 ['accounting_group', 'project_type']).run()

        utils.write_s3(self.s3_bucket,
                       config.PATH_STATS_PER_ACC_GROUP.format(self.s3_date),
                       json.dumps(acc_query, indent=2))

    def stats_per_chargegroup(self):
        """Run the stats per chargegroup and writes the output into S3."""
        influx_dt = self.influx_date
        acc_query = QueryHandler(self.influx_client,
                                 [queries.CHARGEGROUP_STATS.format(influx_dt)],
                                 ['chargegroup', 'chargerole']).run()

        utils.write_s3(self.s3_bucket,
                       config.PATH_STATS_PER_CHARGEGROUP.format(self.s3_date),
                       json.dumps(acc_query, indent=2))

    def magnum_clusters(self):
        """Run the magnum clusters and writes the output into S3."""
        influx_dt = self.influx_date
        clusters = QueryHandler(self.influx_client,
                                [queries.MAGNUM_CLUSTERS.format(influx_dt)],
                                ['engine']).run()

        utils.write_s3(self.s3_bucket,
                       config.PATH_MAGNUM_CLUSTERS.format(self.s3_date),
                       json.dumps(clusters, indent=2))

    def volume_types(self):
        """Run the volume types and writes the output into S3."""
        influx_dt = self.influx_date
        volume_types = QueryHandler(self.influx_client,
                                    [queries.VOLUME_TYPES.format(influx_dt)],
                                    ['type']).run()

        utils.write_s3(self.s3_bucket,
                       config.PATH_VOLUME_TYPES.format(self.s3_date),
                       json.dumps(volume_types, indent=2))

    def volume_stats_per_project(self):
        """Run the volume stats per project and writes the output into S3."""
        influx_dt = self.influx_date
        stats_queries = [query.format(influx_dt)
                         for query in queries.VOLUME_STATS_PER_PROJECT]

        volume_stats = QueryHandler(self.influx_client,
                                    stats_queries,
                                    [
                                        'project_name',
                                        'accounting_group',
                                        'chargegroup',
                                        'chargerole',
                                        'type',
                                    ]).run()

        # Unify results under same project (the problem
        # is that we need different volume types in the same element
        # of the list)
        temp = collections.defaultdict(dict)
        f = (1024 ** 3)

        for stats in volume_stats:
            name = stats["keys"]["project_name"]
            accounting_group = stats["keys"]["accounting_group"]
            chargegroup = stats["keys"]["chargegroup"]
            chargerole = stats["keys"]["chargerole"]
            volume_type = stats["keys"]["type"]

            temp[name]["keys"] = {
                "project_name": name,
                "accounting_group": accounting_group,
                "chargegroup": chargegroup,
                "chargerole": chargerole
            }
            temp[name]["%s_volume_gb" % volume_type] = (
                stats.get('volume_gb', {"usage": 0, "quota": 0})
            )
            temp[name]["%s_volume_bytes" % volume_type] = {
                "usage": temp[name]["%s_volume_gb" % volume_type]['usage'] * f,
                "quota": temp[name]["%s_volume_gb" % volume_type]['quota'] * f
            }
            temp[name]["%s_volume_number" % volume_type] = (
                stats.get('volume_number', {"usage": 0, "quota": 0})
            )

        # We write into a s3 with the normal format
        result = []
        for project, stats in temp.items():
            result.append(stats)

        utils.write_s3(
            self.s3_bucket,
            config.PATH_VOLUME_STATS_PER_PROJECT.format(self.s3_date),
            json.dumps(result, indent=2))

    def share_stats_per_project(self):
        """Run the share stats per project and writes the output into S3."""
        influx_dt = self.influx_date
        stats_queries = [query.format(influx_dt)
                         for query in queries.SHARES_STATS_PER_PROJECT]

        share_stats = QueryHandler(self.influx_client,
                                   stats_queries,
                                   [
                                       'project_name',
                                       'accounting_group',
                                       'chargegroup',
                                       'chargerole',
                                       'share_type',
                                   ]).run()

        # Unify results under same project (the problem
        # is that we need different share types in the same element
        # of the list)
        temp = collections.defaultdict(dict)
        f = (1024 ** 3)

        for stats in share_stats:
            name = stats["keys"]["project_name"]
            accounting_group = stats["keys"]["accounting_group"]
            chargegroup = stats["keys"]["chargegroup"]
            chargerole = stats["keys"]["chargerole"]
            share_type = stats["keys"]["share_type"]

            temp[name]["keys"] = {
                "project_name": name,
                "accounting_group": accounting_group,
                "chargegroup": chargegroup,
                "chargerole": chargerole
            }
            temp[name]["%s_share_gb" % share_type] = (
                stats.get('share_gb', {"usage": 0, "quota": 0})
            )
            temp[name]["%s_share_bytes" % share_type] = {
                "usage": temp[name]["%s_share_gb" % share_type]['usage'] * f,
                "quota": temp[name]["%s_share_gb" % share_type]['quota'] * f
            }
            temp[name]["%s_share_number" % share_type] = (
                stats.get('share_number', {"usage": 0, "quota": 0})
            )

        # We write into a s3 with the normal format
        result = []
        for project, stats in temp.items():
            result.append(stats)

        utils.write_s3(
            self.s3_bucket,
            config.PATH_SHARE_STATS_PER_PROJECT.format(self.s3_date),
            json.dumps(result, indent=2))
