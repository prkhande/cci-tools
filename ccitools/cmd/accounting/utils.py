import boto
import boto.s3.connection

from ccitools.cmd.accounting import config
from collections import defaultdict
from collections import MutableMapping


def get_bucket(access_key, secret_key, host):
    """Initialize the bucket where the data will be stored.

    Args:
        s3cfg: Path where the S3 config file is located.
    Returns:
        S3 bucket.
    """
    s3_conn = boto.connect_s3(
        aws_access_key_id=access_key,
        aws_secret_access_key=secret_key,
        host=host,
        calling_format=boto.s3.connection.OrdinaryCallingFormat(),
    )

    return s3_conn.get_bucket(config.S3_BUCKET)


def write_s3(bucket, s3_key, content):
    """Write content into S3.

    Args:
        bucket: S3 bucket.
        s3_key: S3 key.
        content: Content to be written.
    """
    key = boto.s3.key.Key(bucket)
    key.key = s3_key
    key.set_contents_from_string(content)


def merge_dicts(dict_list):
    """Merge a list of dictionaries recursively.

    Args:
        dict_list: list of dictionaries to be merged.

    Return:
        Result dictionary after merging all the dicts. in dict_list.
    """
    def _recursive_merge(d1, d2):
        for k, v in d1.items():
            if k in d2:
                if all(isinstance(e, MutableMapping) for e in (v, d2[k])):
                    d2[k] = _recursive_merge(v, d2[k])
        d3 = d1.copy()
        d3.update(d2)
        return d3

    out = defaultdict(lambda: defaultdict(dict))
    for elem in dict_list:
        for key, subdict in elem.items():
            out[key] = dict(_recursive_merge(out[key], subdict))
    return dict(out)
