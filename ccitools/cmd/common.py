class Phase(object):
    def __init__(self, description, func=None):
        self.description = description
        self.status = " -- "
        self.details = "Not executed"
        self.func = func

    def run(self, args):
        if self.func:
            self.func(args, self)

    def fail(self, reason):
        self.status = "FAIL"
        self.details = reason

    def ok(self, details):
        self.status = " OK "
        self.details = details

    def __str__(self):
        """Return string representation of the phase."""
        return "[{0}]: {1} ({2})".format(
            self.status,
            self.description,
            self.details)


class Phases(object):
    def __init__(self, phases):
        self.phases = phases

    def execute(self, start_from, args):
        for num, phase in enumerate(self.phases, start=1):
            if num >= start_from:
                phase.run(args)

    def __str__(self):
        """Return string representation of the project creation."""
        str = ""
        for num, phase in enumerate(self.phases, start=1):
            str += "Step {0}: {1}\n".format(num, phase)
        return str
