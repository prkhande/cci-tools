#!/usr/bin/python3

import ccitools.conf
import getpass
import logging
import re
import socket
import sys
import tenacity
import yaml


from ccitools.cmd.base.cloud import BaseCloudCMD
from ccitools.cmd.common import Phase
from ccitools.cmd.common import Phases
from ccitools import common
from ccitools.utils.fim import FIMClient
from ccitools.utils.fim import FIMCodes
from ccitools.utils.servicenowv2 import ServiceNowClient


# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF

FIM_TIMEOUT = 3600
FIM_RETRY_INTERVAL = 60

PROJECT_PROPERTIES_MSG = """Dear HW Resources manager,

Please make sure the "project type" property in the SNOW form
is filled in before creating the project.

Best regards,
Cloud Infrastructure Team"""


class CreationPhases(Phases):
    def __init__(self):
        self.project_fim = Phase(
            "Project creation call to FIM")
        self.project_creation = Phase(
            "Cornerstone synchronization")
        self.project_defaults = Phase(
            "Setting defaults for project")
        self.project_quota = Phase(
            "Quota update for Nova & Cinder & Manila & S3")
        self.project_properties = Phase(
            "Set project properties via Cornerstone")
        self.project_members = Phase(
            "Project members configuration via Cornerstone")
        self.project_cells = Phase(
            "Configure project cell mapping (Manual Step)")

        super().__init__([
            self.project_fim,
            self.project_creation,
            self.project_properties,
            self.project_defaults,
            self.project_quota,
            self.project_members,
            self.project_cells
        ])


class CreateProjectCMD(BaseCloudCMD):
    def __init__(self):
        super(CreateProjectCMD, self).__init__(
            description="Creates a new OpenStack project "
                        "based on different sources")

    def fim_create_project(self, fimclient, project_name, description,
                           owner, safe_exec_mode, phase):
        logger.info("Creating Project via FIM...")

        if fimclient.is_valid_owner(owner):
            logger.info("User '%s' is a valid primary account", owner)
        else:
            msg = "User '%s' is not a primary account" \
                  "Please check RP values." % owner
            phase.fail(msg)
            raise Exception(msg)

        # create the project
        return_code = fimclient.create_project(
            project_name, description, owner)

        # check result
        if return_code == FIMCodes.SUCCESS:
            info_msg = "Project '%s' created " \
                       "successfully in FIM" % (project_name)
            phase.ok(info_msg)
            logger.info(info_msg)
        elif return_code == FIMCodes.ERROR_PROJECT_EXISTS:
            msg = "Project '%s' already exists" % project_name

            if safe_exec_mode:
                # if we are running in default mode we fail
                phase.fail(msg)
                logger.error(msg)
                raise Exception(msg)
            else:
                # in --force we are safe to go
                phase.ok(msg)
                logger.info(msg)
        elif return_code == FIMCodes.ERROR_INVALID_PARAMS:
            error_msg = "Some fields are either empty or " \
                        "contain characters that are invalid"
            phase.fail(error_msg)
            logger.error(error_msg)
            raise Exception(error_msg)
        elif return_code == FIMCodes.INTERNAL_ERROR:
            error_msg = "Internal FIM exception"
            phase.fail(error_msg)
            logger.error(error_msg)
            raise Exception(error_msg)
        else:
            error_msg = "Unknown FIM error. Code '%s'" % return_code
            phase.fail(error_msg)
            logger.error(error_msg)
            raise Exception(error_msg)

    @tenacity.retry(stop=tenacity.stop_after_delay(FIM_TIMEOUT),
                    wait=tenacity.wait_fixed(FIM_RETRY_INTERVAL),
                    before_sleep=tenacity.before_sleep_log(
                        logger,
                        logging.WARNING))
    def wait_for_project(self, target_project, region):
        project = self.cloudclient.find_project(target_project)
        # retrieve the fim-sync property of the project
        # If fim-sync does not exist, the project is not created yet
        # If fim-sync is false, the initialization has not run yet
        # If fim-sync is True, the project is ready
        if not hasattr(project, 'fim-sync'):
            logger.error('Project is still not fully created')
            raise tenacity.TryAgain
        elif getattr(project, 'fim-sync') != 'True':
            logger.error('Project is still not fully initialized')
            raise tenacity.TryAgain
        else:
            logger.info('Project is ready')

    def retrieve_project(self, target_project, region, phase):
        try:
            self.wait_for_project(target_project, region)
        except Exception:
            error_msg = ("Project '%s' could not be found. "
                         "Please check FIM logs") % (target_project)
            phase.fail(error_msg)
            logger.error(error_msg)
            raise Exception(error_msg)
        else:
            project = self.cloudclient.find_project(target_project)
            info_msg = "Project '%s' found with id '%s'" % (
                target_project, project.id)
            phase.ok(info_msg)
            logger.info(info_msg)
            return project

    def initialize_region(self, project, region):
        # Fetch the region in which the project is configured
        region_old = self.cloudclient.find_region_project(project)

        if (region != region_old):
            # Unset quotas on old region
            # as the new quotas will be set later
            self.cloudclient.delete_nova_project_quota(
                project.id, region_old)
            self.cloudclient.delete_cinder_project_quota(
                project.id, region_old)
            self.cloudclient.delete_manila_project_quota(
                project.id, region_old)

        # Replace the base endpoint and apply the region based one
        self.cloudclient.change_region_project(project, region_old, region)

    def configure_defaults(self, project, region, metadata, egroup, phase):
        try:
            if (not metadata
                    or 'default' not in metadata
                    or not metadata['default']
                    or not egroup):
                phase.ok("Defaults not configured or missing input data,"
                         " skipping...")
                return

            if ('egroup_responsible' in metadata['default']
                    and metadata['default']['egroup_responsible']):
                self.cloudclient.set_project_property(
                    project,
                    'landb-responsible',
                    egroup)

            if ('egroup_mainuser' in metadata['default']
                    and metadata['default']['egroup_mainuser']):
                self.cloudclient.set_project_property(
                    project,
                    'landb-mainuser',
                    egroup)

            phase.ok("Defaults configured successfully")
        except Exception as ex:
            error_msg = "Failed to set property: '%s'" % (str(ex))
            phase.fail(error_msg)
            raise Exception(error_msg)

    def update_quotas(self, project, region, metadata, phase):
        """Update the project quotas."""
        try:
            self.cloudclient.set_project_quota(
                project_id=project.id,
                quota=metadata['quota'])
            info_msg = "Project quotas updated"
            logger.info(info_msg)
            phase.ok(info_msg)
        except Exception as ex:
            error_msg = "Setting quota failed: %s" % (str(ex))
            phase.fail(error_msg)
            raise Exception(error_msg)

    def configure_members(self, project, egroup, phase):
        """Add an egroup as 'Member' of the project.

        :project: project object
        :egroup: egroup name to add
        :phase: phase object where we write the result of this step.
        """
        if self.cloudclient.is_group(egroup):
            try:
                self.cloudclient.add_group_member(project, egroup)
                info_msg = "E-group '%s' added to project" % (egroup)
                logger.info(info_msg)
                phase.ok(info_msg)
            except Exception as ex:
                error_msg = "Failed to add egroup: %s" % (str(ex))
                phase.fail(error_msg)
                raise Exception(error_msg)
        else:
            error_msg = "Is '%s' an existing e-group name?" % (egroup)
            phase.fail(error_msg)
            logger.error(error_msg)
            raise Exception(error_msg)

    def set_project_properties(self, project, data, project_type, phase):
        try:
            if hasattr(data, 'accounting_group'):
                self.cloudclient.set_project_property(project,
                                                      'accounting-group',
                                                      data.accounting_group)
            self.cloudclient.set_project_property(project,
                                                  'type',
                                                  project_type.lower())
            if hasattr(data, 'chargegroup') and data.chargegroup:
                if common.is_uuid(data.chargegroup):
                    chargegroup = data.chargegroup
                else:
                    chargegroup = common.get_chargegroups(
                        name=data.chargegroup)[0]['uuid']

                self.cloudclient.set_project_property(project,
                                                      'chargegroup',
                                                      chargegroup)
            if hasattr(data, 'chargerole') and data.chargerole:
                self.cloudclient.set_project_property(project,
                                                      'chargerole',
                                                      data.chargerole)
            phase.ok("Project properties set successfully")
        except Exception as ex:
            error_msg = "Failed to set property: '%s'" % (str(ex))
            phase.fail(error_msg)
            raise Exception(error_msg)

    def validate_egroup_name(self, egroup):
        """Remove unnecessary whitespaces and @cern.ch from egroup name.

        :egroup: egroup name
        """
        egroup = ' '.join(egroup.split())
        if re.search("(.*)@cern.ch", egroup):
            egroup = re.search("(.*)@cern.ch", egroup).group(1)
            logger.warning("Removed @cern.ch from egroup name '%s'", egroup)
        return egroup

    def create_main(self, data, metadata, safe_exec_mode, phases_status,
                    project_type, region):
        """Create a new OpenStack project based.

        :snowclient: servicenow client instance
        :ticket: servicenow ticket number
        """
        egroup = []
        for e in data.egroup.split(','):
            egroup.append(self.validate_egroup_name(e.strip()))

        # Merge data and metadata entries if passed as parameter
        metadata = self.cloudclient.mergeQuotaMetadata(data, metadata)

        fimclient = FIMClient(CONF.fim.webservice)

        self.fim_create_project(
            fimclient,
            data.project_name,
            data.description,
            data.owner,
            safe_exec_mode,
            phases_status.project_fim)

        project = self.retrieve_project(
            data.project_name,
            region,
            phases_status.project_creation)

        self.set_project_properties(
            project,
            data,
            project_type,
            phases_status.project_properties)

        self.initialize_region(project, region)

        self.configure_defaults(
            project,
            region,
            metadata,
            egroup[0],
            phases_status.project_defaults)

        self.update_quotas(
            project,
            region,
            metadata,
            phases_status.project_quota)

        for egroup in egroup:
            self.configure_members(
                project,
                egroup,
                phases_status.project_members)

    def create_from_snow(self, args, phases_status):
        snowclient = ServiceNowClient(instance=args.instance)
        rp = snowclient.get_project_creation_rp(
            args.ticket_number)

        try:
            metadata = yaml.safe_load(
                rp.metadata if hasattr(rp, 'metadata') else {'quota': {}})
            self.create_main(rp,
                             metadata,
                             args.safe_exec_mode, phases_status,
                             args.project_type, args.region)
        finally:
            work_note = "%s\n\n%s" % (
                args.execution_reference, str(phases_status))
            # FIXME(luis): it takes some time to enable the project, so for
            # some reason you get a 401 when doing SOAP requests again in SNOW
            # (due to timeouts, most likely). That's why I need to force again
            # to get a new session, reinitializing the object and the
            # tickets/record producers. It's worth to check with David from
            # the SNOW team why this is happening
            snowclient = ServiceNowClient(instance=args.instance)
            record_producer = snowclient.get_project_creation_rp(
                args.ticket_number)
            record_producer.request.add_work_note(work_note)
            record_producer.request.save()
            print("\n%s" % work_note)

    def create_from_input(self, args, phases_status):
        try:
            self.create_main(args, args.metadata,
                             args.safe_exec_mode, phases_status,
                             args.project_type, args.region)
        finally:
            work_note = "%s\n\n%s" % (
                args.execution_reference, str(phases_status))
            print("\n%s" % work_note)

    def main(self, args=None):
        subparsers = self.parser.add_subparsers(metavar='<subcommand>')

        behaviour = self.parser.add_mutually_exclusive_group()
        behaviour.add_argument('--default',
                               dest="safe_exec_mode",
                               action='store_true')
        behaviour.add_argument('--force',
                               dest="safe_exec_mode",
                               action='store_false')
        self.parser.add_argument(
            "--execution-reference",
            help="adds a execution reference to the work notes",
            default="Executed by '%s' on '%s'" % (
                getpass.getuser(), socket.getfqdn()))

        parser_snow = subparsers.add_parser(
            'from-snow',
            help="Create a project based on the "
                 "information provided via "
                 "snow ticket.")
        parser_snow.add_argument(
            "--instance", default="cern", help="Service now instance")
        parser_snow.add_argument("--ticket-number", required=True)
        parser_snow.add_argument(
            '--project-type',
            required=True,
            help="Define the project type (compute or service)")
        parser_snow.add_argument(
            "--region",
            default='cern', nargs='?',
            help="Region in which the project is created")
        parser_snow.set_defaults(func=self.create_from_snow)

        parser_input = subparsers.add_parser(
            'from-input',
            help="Create a project based on the "
            "information provided as arguments.")
        parser_input.add_argument(
            "--project-name", required=True,
            help="Name of the project. "
                 "Example: Build service, Web development, etc")
        parser_input.add_argument("--description",
                                  required=True,
                                  help="Project description")
        parser_input.add_argument("--owner",
                                  required=True,
                                  help="Project owner")
        parser_input.add_argument("--egroup",
                                  required=True,
                                  help="e-group of project members")
        parser_input.add_argument(
            '--project-type', required=True,
            help="Define the project type (compute or service)")
        parser_input.add_argument(
            "--accounting-group",
            required=True,
            help="Project account group")
        parser_input.add_argument("--chargegroup",
                                  help="Project chargegroup")
        parser_input.add_argument("--chargerole",
                                  help="Project chargerole")
        parser_input.add_argument(
            "--region", default='cern', nargs='?',
            help="Region in which the project is created")
        # TODO(jcastro) Start of deprecated parameters to be removed
        parser_input.add_argument(
            "--cores",
            default=10,
            help="Number of cores")
        parser_input.add_argument(
            "--instances",
            default=5,
            help="Number of virtual machines")
        parser_input.add_argument(
            "--ram",
            default=20,
            help="Amount in RAM [GB]")
        parser_input.add_argument(
            '--cp1-gigabytes',
            default=0,
            help="GB quota for cp1 volumes")
        parser_input.add_argument(
            '--cp1-volumes',
            default=0,
            help="Volumes quota for cp1 volumes")
        parser_input.add_argument(
            '--cpio1-gigabytes',
            default=0,
            help="GB quota for cpio1 volumes")
        parser_input.add_argument(
            '--cpio1-volumes',
            default=0,
            help="Volumes quota for cpio1 volumes")
        parser_input.add_argument(
            '--io1-gigabytes',
            default=0,
            help="GB quota for io1 volumes")
        parser_input.add_argument(
            '--io1-volumes',
            default=0,
            help="Volumes quota for io1 volumes")
        parser_input.add_argument(
            '--standard-gigabytes',
            default=0,
            help="GB quota for standard volumes")
        parser_input.add_argument(
            '--standard-volumes',
            default=0,
            help="Volumes quota for standard volumes")
        parser_input.add_argument(
            '--vault-100-gigabytes',
            default=0,
            help="GB quota for vault-100 volumes")
        parser_input.add_argument(
            '--vault-100-volumes',
            default=0,
            help="Volumes quota for vault-100 volumes")
        parser_input.add_argument(
            '--vault-500-gigabytes',
            default=0,
            help="GB quota for vault-500 volumes")
        parser_input.add_argument(
            '--vault-500-volumes',
            default=0,
            help="Volumes quota for vault-500 volumes")
        parser_input.add_argument(
            '--meyrin-shares',
            default=0,
            help="Shares quota for meyrin shares")
        parser_input.add_argument(
            '--meyrin-gigabytes',
            default=0,
            help="GB quota for meyrin shares")
        parser_input.add_argument(
            '--geneva_testing-shares',
            default=0,
            help="Shares quota for geneva testing shares")
        parser_input.add_argument(
            '--geneva_testing-gigabytes',
            default=0,
            help="GB quota for geneva testing shares")
        parser_input.add_argument(
            '--s3-buckets',
            default=0,
            help="Buckets quota in S3")
        parser_input.add_argument(
            '--s3-gigabytes',
            default=0,
            help="GB quota for S3")
        parser_input.add_argument(
            '--loadbalancer',
            default=0,
            help="LoadBbalancers quota")
        parser_input.add_argument(
            '--floatingip',
            default=0,
            help="Floating IPs quota")
        # TODO(jcastro) End of deprecated parameters to be removed as they
        # will be merged into the following metadata parameter
        parser_input.add_argument(
            "--metadata", dest="metadata",
            help="Input metadata for the project (JSON or YAML dict)",
            type=common.YAMLParse,
            default={})
        parser_input.set_defaults(func=self.create_from_input)

        args = self.parse_args(args)

        try:
            call = args.func
        except AttributeError:
            self.parser.error("Too few arguments")

        call(args, CreationPhases())


# Needs static method for setup.cfg
def main(args=None):
    CreateProjectCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
