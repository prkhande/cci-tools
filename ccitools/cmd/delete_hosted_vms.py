#!/usr/bin/python3

import ccitools.conf
import logging
import sys
import time

from ccitools.cmd.base.cloud import BaseCloudCMD

# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF

DELETION_TIMEOUT = 120
DELETION_RETRY_INTERVAL = 10


class bcolors(object):
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    BOLD = '\033[1m'
    ENDC = '\033[0m'


class DeleteHostedVMsCMD(BaseCloudCMD):

    def __init__(self):
        super(DeleteHostedVMsCMD, self).__init__(
            description="Delete VMs hosted in the hypervisor")

    def _wait_for_deletion(self, host, cloud):
        """Wait for deletion of VMs residing in the specified host.

        :param host: Host that contains VMs to be deleted
        :param cloud: CloudClient object
        """
        timeout = time.time() + DELETION_TIMEOUT
        while time.time() < timeout:
            if cloud.get_servers_by_hypervisor(host):
                logger.info("Waiting for Nova to delete all servers...")
                time.sleep(DELETION_RETRY_INTERVAL)
            else:
                logger.info("All servers deleted")
                return
        error_msg = "There was an error deleting '%s' hosted VMs" % (host)
        raise Exception(error_msg)

    def delete_hosted_vms(self, cloud, behaviour, hosts):

        logger.info("Deleting hosted VMs on '%s'...", hosts)
        for host in hosts.split():
            try:
                logger.info("Getting list of servers from host '%s'...", host)
                servers = cloud.get_servers_by_hypervisor(host)
                logger.info("'%s' has %s servers running", host, len(servers))

                if behaviour == 'perform':
                    for server in servers:
                        logger.info("Deleting server '%s'...", server.name)
                        server.delete()
                    self._wait_for_deletion(host, cloud)
                else:
                    logger.info("[DRYRUN] Would've deleted %s VMs running "
                                "on '%s'", len(servers), host)

            except Exception as ex:
                logger.error("%s%s%s", bcolors.BOLD, ex, bcolors.ENDC)

        time.sleep(1)  # log messages sync

    def main(self, args=None):
        self.parser.add_argument(
            "--behaviour", required=True, help="Execution as dryrun/perform")
        self.parser.add_argument(
            "--hosts", required=True, help="List of target hypervisors")

        args = self.parse_args(args)

        self.delete_hosted_vms(
            self.cloudclient,
            args.behaviour,
            args.hosts
        )


# Needs static method for setup.cfg
def main(args=None):
    DeleteHostedVMsCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
