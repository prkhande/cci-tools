#!/usr/bin/python3

import ccitools.conf
import logging
import sys
import tenacity

from ccitools.cmd.base.cloud import BaseCloudCMD
from ccitools import common
from ccitools.utils.fim import FIMClient
from ccitools.utils.fim import FIMCodes
from ccitools.utils.servicenowv2 import ServiceNowClient

# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF

FIM_TIMEOUT = 5400  # 1.5 hour
FIM_RETRY_INTERVAL = 300  # must be 300, 5 minutes

RESOLVE_MESSAGE = """Dear {caller_first_name},

Your OpenStack project has now been created and is ready for use.
The OpenStack User Guide is available from here: https://cern.ch/clouddocs

Please note that this ticket should no longer be updated, as
it has been automatically resolved. If you have any further queries,
please do not hesitate to open a new ticket by using [code]
<a href="https://cern.service-now.com/service-portal?id=sc_cat_item&name=incident&fe=cloud-infrastructure">
the following link</a>.</br></br>
[/code]

Best regards,
Cloud Infrastructure Team.
"""  # noqa

REQUIRED_INTERVENTION_MESSAGE = """Project '{project_name}' NOT enabled.
Manual intervention is required. The project will be enabled manually
by one of the Cloud Infrastructure members.
"""


class EnableProjectCMD(BaseCloudCMD):
    def __init__(self):
        super(EnableProjectCMD, self).__init__(
            description="Enables an OpenStack project")

    @tenacity.retry(stop=tenacity.stop_after_delay(FIM_TIMEOUT),
                    wait=tenacity.wait_fixed(FIM_RETRY_INTERVAL),
                    before_sleep=tenacity.before_sleep_log(
                        logger,
                        logging.WARNING))
    def wait_for_project(self, cloud, project_name):
        project = cloud.find_project(project_name)

        if not project.enabled:
            logger.info("Project '%s' not enabled yet. Retrying...",
                        project_name)
            raise tenacity.TryAgain
        logger.info("Project '%s' enabled in Openstack", project_name)

    def enable_project(self, fimclient, cloudclient, enable, project_name):
        # enable project
        if enable:
            logger.info("Enabling project '%s' in FIM", project_name)
            result = fimclient.set_project_status(project_name, enabled=True)

            if result != FIMCodes.SUCCESS:
                logger.error("Project '%s' not enabled. Please verify "
                             "existance.", project_name)
                raise Exception("Project '%s' not enabled. Please verify "
                                "existance.", project_name)

            logger.info("Project '%s' enabled in FIM", project_name)

            self.wait_for_project(cloudclient, project_name)
        else:
            logger.info("Project '%s' was not enabled as requested",
                        project_name)

    def enable_from_snow(self, args, enable, fimclient, cloudclient):

        snowclient = ServiceNowClient(instance=args.instance)

        # get info
        record_producer = snowclient.get_project_creation_rp(
            args.ticket_number
        )

        # get service now info
        resolver_info = snowclient.user.get_user_info_by_user_name(
            args.resolver)
        caller_info = snowclient.user.get_user_info_by_sys_id(
            record_producer.request.info.u_caller_id)
        project_name = record_producer.project_name
        first_name = (
            caller_info.u_preferred_first_name or caller_info.first_name)
        logger.info(
            "Enabling project from the values got from the SNOW ticket")
        logger.info(
            "Is the project going to be enabled?: %s", enable)
        logger.info(
            "Which SNOW instance are we using?: %s", args.instance)
        logger.info(
            "Who is going to resolve the ticket?: %s", resolver_info.name)

        # enable project
        self.enable_project(fimclient, cloudclient, enable, project_name)

        # FIXME(luis): it takes some time to enable the project, so for
        # some reason you get a 401 when doing SOAP requests again in SNOW
        # (due to timeouts, most likely). That's why I need to force again
        # to get a new session, reinitializing the object and the
        # tickets/record producers. It's worth to check with David from
        # the SNOW team why this is happening

        # renew the session + get again the record producer information
        snowclient = ServiceNowClient(instance=args.instance)
        record_producer = snowclient.get_project_creation_rp(
            args.ticket_number
        )

        # perform snow operations
        if enable:
            record_producer.request.resolve(
                RESOLVE_MESSAGE.format(
                    caller_first_name=first_name),
                resolver_info.name)
        else:
            record_producer.request.add_work_note(
                REQUIRED_INTERVENTION_MESSAGE.format(
                    project_name=project_name))

        # put all the changes in the instance
        record_producer.request.save()

    def enable_from_values(self, args, enable, fimclient,
                           cloudclient):
        logger.info(
            "Enabling project from the values got from CLI parameters")
        # enable project
        self.enable_project(fimclient, cloudclient, enable, args.project_name)

    def main(self, args=None):
        subparsers = self.parser.add_subparsers(metavar='<subcommand>')

        self.parser.add_argument(
            "--enable-project", type=common.str2bool, required=True)

        parser_values = subparsers.add_parser(
            'from-values',
            help="Enables a project from the values passed "
                 "as parameters.")

        parser_values.add_argument("--project-name", required=True)
        parser_values.set_defaults(func=self.enable_from_values)

        parser_snow = subparsers.add_parser(
            'from-snow',
            help="Enables a project from the SNOW ticket.")

        parser_snow.add_argument(
            "--ticket-number", required=True,
            help="Service Now incident reference. "
                 "E.g: RQFXXXX")
        parser_snow.add_argument(
            "--instance", default="cern", help="Service-Now instance")
        parser_snow.add_argument(
            "--resolver", required=True,
            help="User who will resolve the ticket")
        parser_snow.set_defaults(func=self.enable_from_snow)

        args = self.parse_args(args)

        # initialize fim client
        fimclient = FIMClient(CONF.fim.webservice)

        args.func(args,
                  enable=args.enable_project,
                  fimclient=fimclient,
                  cloudclient=self.cloudclient)


# Needs static method for setup.cfg
def main(args=None):
    EnableProjectCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
