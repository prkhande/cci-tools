#!/usr/bin/python3

import ccitools.conf
import logging
import sys

from ccitools.cmd import base
from ccitools.common import ssh_executor

# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF


class GetHostsUptime(base.BaseCMD):

    def __init__(self):
        super(GetHostsUptime, self).__init__(
            description="SSH into hosts and get it's uptime")

    def create_sorted_uptime_hosts(self, uptime_dict):
        # reverse sort by value
        output_list = []
        sorted_list = sorted(uptime_dict.items(),
                             key=lambda x: x[1],
                             reverse=True)
        for key, value in sorted_list:
            val = key + ":" + str(value)
            output_list.append(val)
        return output_list

    def ssh_uptime(self, hosts, exec_mode):
        """Check if hosts are sshable."""
        uptime_dict = {}
        hosts = hosts.split()
        if not exec_mode:
            for host in hosts:
                uptime_dict[host] = 0.0

        for host in hosts:
            try:
                # SSH and get uptime
                output, error = ssh_executor(host, "cat /proc/uptime")
                logger.info("output : {}".format(output))
                logger.info("error : {}".format(error))
                if error:
                    logger.info("Error executing command {}".format(error))
                # Map uptime to host
                if output:
                    uptime = str(output[0])
                    logger.info("uptime : {}".format(uptime))
                    uptime = uptime.split(' ')
                    uptime_dict[host] = float(uptime[0])
                # Map Hosts to invalid time
                else:
                    uptime_dict[host] = float(-10.0)
            except Exception:
                logger.error("Can't connect to %s" % host)

        # sort the dict and create list
        return self.create_sorted_uptime_hosts(uptime_dict)

    def main(self, args=None):
        self.parser.add_argument("--hosts",
                                 required=True,
                                 help="List of hosts")

        behaviour = self.parser.add_mutually_exclusive_group()
        behaviour.add_argument('--perform',
                               dest="exec_mode",
                               action='store_true')
        behaviour.add_argument('--dryrun',
                               dest="exec_mode",
                               action='store_false')

        args = self.parse_args(args)

        try:
            data = self.ssh_uptime(args.hosts, args.exec_mode)
        except Exception as e:
            logger.info("Unable to do SSH into hosts %s", e)
            sys.exit(1)
        else:
            logger.info("All hosts are up and are sshable")
            logger.info("RUNDECK:DATA:HOSTS_UPTIME={}"
                        .format(data))


# Needs static method for setup.cfg
def main(args=None):
    GetHostsUptime().main(args)


if __name__ == "__main__":
    main()
