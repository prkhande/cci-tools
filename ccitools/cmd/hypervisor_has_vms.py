#!/usr/bin/python3

import ccitools.conf
import logging
import prettytable
import sys

from ccitools.cmd.base.cloud import BaseCloudCMD

# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF


class HypervisorHasVMsCMD(BaseCloudCMD):

    def __init__(self):
        super(HypervisorHasVMsCMD, self).__init__(
            description="Checks if the hypervisor has VMs")

    def hypervisor_has_vms(self, cloudclient, ignore, hypervisors):
        # Build pretty table with VMs per host
        servers_table = prettytable.PrettyTable(["Hypervisor", "VM"])
        servers_table.align["Hypervisor"] = 'c'
        servers_table.align["VM"] = 'c'

        hosts_servers = []  # List of host,server tuples

        # Perform action on list of servers
        for hv_name in hypervisors.split():
            servers = cloudclient.get_servers_by_hypervisor(hv_name)
            if servers:
                server_names = [server.name for server in servers]
                for server_name in server_names:
                    hosts_servers.append((hv_name, server_name))
                    servers_table.add_row([hv_name, server_name])

        if hosts_servers:
            if ignore:
                logger.info(
                    "Ignore the following found VMs. Keep going...\n%s",
                    servers_table.get_string(sortby="Hypervisor"))
            else:
                logger.error(
                    "The following VMs were found in the provided "
                    "list of hypervisors.\n%s",
                    servers_table.get_string(sortby="Hypervisor"))
                sys.exit(1)
        else:
            logger.info(
                "Did NOT found VMs in the provided list of hypervisors.")

    def main(self, args=None):
        self.parser.add_argument(
            "--ignore", action='store_true',
            help="Do NOT fail when VMs are found.")
        self.parser.add_argument(
            "--hypervisors", required=True,
            help="List of hosts to search in.")

        args = self.parse_args(args)

        self.hypervisor_has_vms(
            self.cloudclient,
            args.ignore,
            args.hypervisors
        )


# Needs static method for setup.cfg
def main(args=None):
    HypervisorHasVMsCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
