#!/usr/bin/python3

import logging
import prettytable

from ccitools.cmd.base.cloud import BaseCloudCMD
from ccitools import common
from neutronclient.common import exceptions as neutron_exceptions

logger = logging.getLogger(__name__)


class ManageNeutronAgent(BaseCloudCMD):
    def __init__(self):
        super(ManageNeutronAgent, self).__init__(
            description="Manages neutron agents in the system")

    def manage_neutron_agent(self, args):
        ok_hosts = 0
        ko_hosts = 0

        # Iterate over the hosts
        for host in args.hosts.split():
            fqdn = common.normalize_fqdn(host)
            try:
                agentlist = self.cloudclient.get_neutron_agents_list(
                    host=fqdn)
            except neutron_exceptions.NeutronClientException as e:
                logger.exception('Failed to fetch agent lists for %s',
                                 fqdn, e)
                ko_hosts = ko_hosts + 1
                continue

            if not agentlist:
                logger.error('Not found. Has %s been deleted already?', fqdn)
                ko_hosts = ko_hosts + 1
                continue

            agent = agentlist.pop()

            if args.exec_mode:
                logger.info('Trying to %s neutron-agent on %s ...',
                            args.action,
                            fqdn)
                try:
                    if args.action in ['enable', 'disable']:
                        logger.info('Executing: neutron agent-%s %s',
                                    args.action, fqdn)
                        status = True if args.action == 'enable' else False

                        self.cloudclient.change_status_neutron_agent(
                            agent, status)
                        ok_hosts = ok_hosts + 1

                    if args.action == 'delete':
                        logger.info('Executing: neutron agent-%s %s',
                                    args.action, fqdn)
                        self.cloudclient.delete_neutron_agent(agent)
                        ok_hosts = ok_hosts + 1

                except neutron_exceptions.NeutronClientException as e:
                    logger.exception('Failed to %s neutron agent on %s',
                                     args.action,
                                     fqdn, e)
                    ko_hosts = ko_hosts + 1
            else:
                logger.info('DRYRUN neutron agent-%s %s',
                            args.action,
                            fqdn)
                ok_hosts = ok_hosts + 1

        output_table = prettytable.PrettyTable(
            ["Total Hosts", "Success", "Error"])
        output_table.add_row([ok_hosts + ko_hosts, ok_hosts, ko_hosts])
        logger.info("Summary neutron agent-%s:\n%s",
                    args.action,
                    output_table.get_string())

        if ko_hosts > 0:
            logger.error('neutron agent-%s did not work for some hosts.'
                         'Please check logs...',
                         args.action,)
            raise SystemExit(3)

    def main(self, args=None):
        self.parser.add_argument("--hosts",
                                 required=True,
                                 help="List of hosts")

        behaviour = self.parser.add_mutually_exclusive_group()
        behaviour.add_argument('--perform',
                               dest="exec_mode",
                               action='store_true')
        behaviour.add_argument('--dryrun',
                               dest="exec_mode",
                               action='store_false')

        action = self.parser.add_mutually_exclusive_group()
        action.add_argument('--enable',
                            dest="action",
                            action='store_const',
                            const='enable')
        action.add_argument('--disable',
                            dest="action",
                            action='store_const',
                            const='disable')
        action.add_argument('--delete',
                            dest="action",
                            action='store_const',
                            const='delete')
        self.manage_neutron_agent(self.parse_args(args))


# Needs static method for setup.cfg
def main(args=None):
    ManageNeutronAgent().main(args)


if __name__ == "__main__":
    main()
