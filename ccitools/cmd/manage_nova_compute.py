#!/usr/bin/python3

import logging
import prettytable

from ccitools.cmd.base.cloud import BaseCloudCMD
from ccitools import common
from novaclient import exceptions as nova_exceptions


logger = logging.getLogger(__name__)


class ManageNovaCompute(BaseCloudCMD):

    def __init__(self):
        super(ManageNovaCompute, self).__init__(
            description="Manages nova compute agents in the system")

    def manage_nova_compute(self, args):
        ok_hosts = 0
        ko_hosts = 0

        # Iterate over the hosts
        for host in args.hosts.split():
            fqdn = common.normalize_fqdn(host)
            try:
                servicelist = self.cloudclient.get_nova_services_list(
                    host=fqdn)
            except nova_exceptions.ClientException as e:
                logger.exception('Failed to fetch service lists for %s',
                                 fqdn, e)
                ko_hosts = ko_hosts + 1
                continue

            if not servicelist:
                logger.error('Not found. Has %s been deleted already?', fqdn)
                ko_hosts = ko_hosts + 1
                continue

            service = servicelist.pop()

            if args.exec_mode:
                logger.info('Trying to %s nova-compute on %s ...',
                            args.action,
                            fqdn)
                try:
                    if args.action == 'enable':
                        logger.info('Enabling nova-compute node %s', fqdn)
                        self.cloudclient.enable_nova_service(service)
                        ok_hosts = ok_hosts + 1

                    if args.action == 'disable':
                        logger.info('Disabling nova-compute node %s', fqdn)
                        self.cloudclient.disable_nova_service(
                            service, args.reason)
                        ok_hosts = ok_hosts + 1

                    if args.action == 'delete':
                        logger.info('Removing nova-compute node %s', fqdn)
                        self.cloudclient.delete_nova_service(service)
                        ok_hosts = ok_hosts + 1

                except nova_exceptions.ClientException as e:
                    logger.exception('Failed to %s nova-compute on %s',
                                     args.action,
                                     fqdn, e)
                    ko_hosts = ko_hosts + 1
            else:
                logger.info('DRYRUN nova service-%s %s --reason %s',
                            args.action,
                            fqdn,
                            args.reason)
                ok_hosts = ok_hosts + 1

        output_table = prettytable.PrettyTable(
            ["Total Hosts", "Success", "Error"])
        output_table.add_row([ok_hosts + ko_hosts, ok_hosts, ko_hosts])
        logger.info("Summary nova service-%s:\n%s",
                    args.action,
                    output_table.get_string())

        if ko_hosts > 0:
            logger.error('nova service-%s did not work for some hosts.'
                         'Please check logs...',
                         args.action,)
            raise SystemExit(3)

    def main(self, args=None):
        self.parser.add_argument("--hosts",
                                 required=True,
                                 help="List of hosts")

        behaviour = self.parser.add_mutually_exclusive_group()
        behaviour.add_argument('--perform',
                               dest="exec_mode",
                               action='store_true')
        behaviour.add_argument('--dryrun',
                               dest="exec_mode",
                               action='store_false')

        action = self.parser.add_mutually_exclusive_group()
        action.add_argument('--enable',
                            dest="action",
                            action='store_const',
                            const='enable')
        action.add_argument('--disable',
                            dest="action",
                            action='store_const',
                            const='disable')
        action.add_argument('--delete',
                            dest="action",
                            action='store_const',
                            const='delete')
        self.parser.add_argument("--reason",
                                 help="Reason of the action")
        self.manage_nova_compute(self.parse_args(args))


# Needs static method for setup.cfg
def main(args=None):
    ManageNovaCompute().main(args)


if __name__ == "__main__":
    main()
