#!/usr/bin/python3

import ccitools.conf
import logging
import sys
import urllib.request

from ccitools.cmd.base.cloud import BaseCloudCMD
from ccitools.utils.sendmail import MailClient
from ccitools.utils.xldap import XldapClient
from datetime import datetime
from string import Template

# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF


class NotifyUsersCMD(BaseCloudCMD):

    def __init__(self):
        super(NotifyUsersCMD, self).__init__(
            description="Sends a notification email to the affected users")

    def get_owners_projects_vms(self, servers, cloud, xldap):
        """Match users, projects and VMs.

        :param servers List of compute servers
        :param cloud ccitools.utils.cloud CloudClient
        :param xldap ccitools.utils.xldap XldapClient
        """
        dict = {}
        for server in servers:
            project_name = cloud.get_project(server.tenant_id).name
            try:
                owner = xldap.get_vm_owner(server.name)
                if owner not in dict.keys():
                    dict[owner] = {}
                if project_name not in dict[owner].keys():
                    dict[owner][project_name] = []
                dict[owner][project_name].append(server.name)
            except Exception:
                logger.warning(
                    "Impossible to determinate '%s''s owner", server.name)
        return dict

    def get_members_projects(self, projects, cloud):
        """Match members and projects.

        :param projects List of projects
        :param cloud ccitools.utils.cloud CloudClient
        """
        dict = {}
        for project_reference in projects:
            project = cloud.find_project(project_reference)
            for member in cloud.get_project_members(project):
                if member not in dict.keys():
                    dict[member] = {}
                if project.name not in dict[member].keys():
                    dict[member][project.name] = []
        return dict

    def readTemplate(self, url):
        """Read content of a remote template.

        :param url URL where the template is located
        """
        output = urllib.request.urlopen(url)  # nosec
        if output.getcode() == 200:
            return output.read()
        else:
            raise Exception("Template '%s' not found" % url)

    def substitute_template_common_values(self, date, duration,
                                          subject_template, body_template):
        template_values = {}
        template_values['duration'] = duration
        if date:
            date = datetime.strptime(date, '%d-%m-%Y %H:%M')
            template_values['date'] = date.strftime("%A, %d %B")
            template_values['hour'] = date.strftime("%H:%M")

        return (subject_template.safe_substitute(template_values),
                body_template.safe_substitute(template_values))

    def substitute_template_user_values(self, username, projects,
                                        subject_template, body_template):
        subject_template = Template(subject_template)
        body_template = Template(body_template)

        template_values = {}
        template_values['vms'] = []
        template_values['project'] = []
        template_values['user'] = username
        for key in projects.keys():
            template_values['project'].append(key)
            for vm in projects[key]:
                template_values['vms'].append(vm)

        # convert lists in a plain string
        template_values['vms'] = "\n".join(template_values['vms'])
        template_values['project'] = ', '.join(template_values['project'])

        return (subject_template.safe_substitute(template_values),
                body_template.safe_substitute(template_values))

    def notify(self, dict, subject_template, body_template, cloud,
               mailclient, xldap, exec_mode):
        mail_body = body_template
        for user in dict.keys():
            mail_to = xldap.get_email(user)
            mail_subject, mail_body = self.substitute_template_user_values(
                user, dict[user], subject_template, body_template)
            if exec_mode:
                try:
                    mailclient.send_mail(
                        mail_to, mail_subject, mail_body,
                        "noreply@cern.ch", "", "", "plain")
                    logger.info("Email sent successfully to %s", mail_to)
                except Exception as ex:
                    logger.error("Impossible to send mailto '%s'. "
                                 "Error message: '%s'", mail_to, ex)

                logger.info("To: %-20s Subject: \"%s\"", mail_to, mail_subject)
            else:
                logger.info("[DRYRUN] To: %-20s Subject: \"%s\"",
                            mail_to, mail_subject)
        logger.info("Example mail:\n%s", mail_body)

    def notify_members(self, args, subject_template, body_template,
                       cloud, mailclient, xldap):
        servers_list = cloud.get_servers_by_projects(cloud, args.projects)

        members_projects = self.get_members_projects(args.projects, cloud)
        owners_projects_vms = self.get_owners_projects_vms(
            servers_list, cloud, xldap)
        dict = members_projects.copy()

        if owners_projects_vms:
            dict.update(owners_projects_vms)

        self.notify(dict, subject_template, body_template,
                    cloud, mailclient, xldap, args.exec_mode)

    def notify_owners(self, args, subject_template, body_template,
                      cloud, mailclient, xldap):
        servers_list = cloud.get_servers_by_names(cloud, args.vms)
        servers_list += cloud.get_servers_by_hypervisors(
            cloud, args.hypervisors)
        servers_list += cloud.get_servers_by_projects(cloud, args.projects)
        d = self.get_owners_projects_vms(servers_list, cloud, xldap)
        self.notify(d, subject_template, body_template,
                    cloud, mailclient, xldap, args.exec_mode)

    def main(self, args=None):
        subparsers = self.parser.add_subparsers(metavar='<subcommand>')
        self.parser.add_argument(
            "--subject", required=True, help="email subject")
        self.parser.add_argument(
            "--body", required=True,
            help="Url to the template to be used as email body")
        self.parser.add_argument(
            "--date", help="String date format dd-mm-aaaa hh:mm")
        self.parser.add_argument("--duration", help="Duration")

        behaviour = self.parser.add_mutually_exclusive_group()
        behaviour.add_argument('--perform',
                               dest="exec_mode",
                               action='store_true',
                               help="If present, sends email")
        behaviour.add_argument('--dryrun',
                               dest="exec_mode",
                               action='store_false')

        parser_members = subparsers.add_parser('members')
        parser_members.add_argument(
            "--projects", nargs='*', default=[], help="List of project names")
        parser_members.set_defaults(func=self.notify_members)

        parser_owners = subparsers.add_parser('owners')
        parser_owners.add_argument(
            "--hypervisors", nargs='*', default=[],
            help="List of hypervisor names")
        parser_owners.add_argument(
            "--projects", nargs='*', default=[], help="List of project names")
        parser_owners.add_argument(
            "--vms", nargs='*', default=[], help="List of vms")
        parser_owners.set_defaults(func=self.notify_owners)

        args = self.parse_args(args)

        mailclient = MailClient("localhost")
        xldap = XldapClient(CONF.ldap.server)

        body_template = Template(self.readTemplate(args.body))
        subject_template = Template(args.subject)
        subject_template, body_template = (
            self.substitute_template_common_values(
                args.date, args.duration, subject_template, body_template))
        args.func(args, subject_template, body_template,
                  self.cloudclient, mailclient, xldap)


# Needs static method for setup.cfg
def main(args=None):
    NotifyUsersCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(-1)
