#!/usr/bin/python3

import ccitools.conf
import dateutil.parser
import getpass
import logging
import prettytable
import re
import socket
import sys
import time

from ccitools.cmd.base.cloud import BaseCloudCMD
from ccitools import common
from ccitools.utils.servicenowv2 import ServiceNowClient
from datetime import datetime
from datetime import timezone

from magnumclient.common.apiclient import exceptions as magnum_exceptions


# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF

date_format_cinder = "%Y-%m-%dT%H:%M:%S.%f"
date_format_nova = "%Y-%m-%dT%H:%M:%SZ"

CLOUD_FE_NAME = "Cloud Infrastructure"
CLOUD_3RD_LINE_AG_NAME = "Cloud Infrastructure 3rd Line Support"
RALLY_REGEX = re.compile('^rally-')
MAGNUM_PROJECT_ID = "c197dee4-64da-452a-9a96-a28d79ef4c38"
MAGNUM_T_PROJECT_ID = "2164c5ef-03c3-4279-94c0-f32fad788b80"


class RallyCleanupCMD(BaseCloudCMD):

    def __init__(self):
        super(RallyCleanupCMD, self).__init__(
            description="Cleans old Rally jobs")

    def check_servers_state(self, cloud, servers_pending):
        """Check confirmation of deletion of servers from a given list.

        :param cloud: CloudClient object
        :param servers_pending: List of servers in deletion process
        :returns: List of servers that could not be deleted
        """
        broken_servers = []
        for server in cloud.get_servers(user_name='svcprobe'):
            if server in servers_pending:
                broken_servers.append(server)
        return broken_servers

    def check_volumes_state(self, cloud, volumes_pending):
        """Check confirmation of deletion of volumes from a given list.

        :param cloud: CloudClient object
        :param volumes_pending: List of volumes in deletion process
        :returns: List of volumes that could not be deleted
        """
        broken_volumes = []
        for volume in cloud.get_volumes(user_name='svcprobe'):
            if volume in volumes_pending:
                broken_volumes.append(volume)
        return broken_volumes

    def get_pending_servers(self, cloud):
        """Get a list of servers considered as rally leftovers.

        :param cloud: CloudClient object
        :returns: List of servers to delete
        """
        servers_pending = []
        logger.info("Looking for old servers...")
        for server in cloud.get_servers(user_name='svcprobe'):
            if RALLY_REGEX.match(server.name):
                creation = datetime.strptime(
                    server.created, date_format_nova)
                if (datetime.now() - creation).days >= 1:
                    servers_pending.append(server)

        return servers_pending

    def get_pending_volumes(self, cloud):
        """Get a list of volumes considered as rally leftovers.

        :param cloud: CloudClient object
        :returns: List of volumes to delete
        """
        volumes_pending = []
        logger.info("Looking for old volumes...")
        for volume in cloud.get_volumes(user_name='svcprobe'):
            if RALLY_REGEX.match(volume.name):
                creation = datetime.strptime(
                    volume.created_at, date_format_cinder)
                if (datetime.now() - creation).days >= 1:
                    volumes_pending.append(volume)
        return volumes_pending

    def get_leftover_clusters(self, cloud):
        """Get list of clusters that were not cleaned up by the task context.

        :param cloud: CloudClient object
        :returns: List of clusters
        """
        leftover_clusters = []
        logger.info("Looking for leftover clusters")

        clusters = cloud.get_clusters(MAGNUM_PROJECT_ID) \
            + cloud.get_clusters(MAGNUM_T_PROJECT_ID)

        for cluster in clusters:
            created_at = dateutil.parser.parse(cluster.created_at)
            days_since_created = (datetime.now(timezone.utc) - created_at).days
            if RALLY_REGEX.match(cluster.name) and days_since_created >= 1:
                leftover_clusters.append(cluster)

        return leftover_clusters

    def check_stuck_clusters(self, cloud, leftover_clusters):
        """Check to see if clusters could not be deleted.

        Check if any leftover clusters still exist after
        attempting to delete them.

        :param cloud: CloudClient object
        :param leftover_clusters: Clusters that should have been deleted
        :returns: List of clusters that still exist
        """
        stuck_clusters = []

        # If the cluster has changed status it will no longer be equal,
        # so compare by the UUIDs.
        leftover_cluster_uuids = [c.uuid for c in leftover_clusters]

        clusters = cloud.get_clusters(MAGNUM_PROJECT_ID) \
            + cloud.get_clusters(MAGNUM_T_PROJECT_ID)

        for cluster in clusters:
            if cluster.uuid in leftover_cluster_uuids:
                stuck_clusters.append(cluster)

        return stuck_clusters

    def get_leftover_templates(self, cloud):
        """Get list of templates that were not cleaned up.

        :param cloud: CloudClient object
        :returns: List of cluster templates
        """
        leftover_templates = []
        logger.info("Looking for leftover cluster templates")

        templates = cloud.get_cluster_templates(MAGNUM_PROJECT_ID) \
            + cloud.get_cluster_templates(MAGNUM_T_PROJECT_ID)

        for template in templates:
            created_at = dateutil.parser.parse(template.created_at)
            days_since_created = (datetime.now(timezone.utc) - created_at).days
            if RALLY_REGEX.match(template.name) and days_since_created >= 1:
                leftover_templates.append(template)

        return leftover_templates

    def check_stuck_templates(self, cloud, leftover_templates):
        """Check to see if cluster templates could not be deleted.

        Check if any leftover clusters templates still exist
        after attempting to delete them.

        :param cloud: CloudClient object
        :param leftover_templates: Templates that should have been deleted
        :returns: List of cluster templates that still exist
        """
        stuck_templates = []

        leftover_template_uuids = [t.uuid for t in leftover_templates]

        templates = cloud.get_cluster_templates(MAGNUM_PROJECT_ID) \
            + cloud.get_cluster_templates(MAGNUM_T_PROJECT_ID)

        for template in templates:
            if template.uuid in leftover_template_uuids:
                stuck_templates.append(template)

        return stuck_templates

    def format_clusters_table(self, clusters):
        """Format table of clusters.

        :param clusters: List of leftover clusters
        :returns: Formatted table of clusters
        """
        for cluster in clusters:
            cluster.region = cluster.manager.api.region_name
        fields = [
            ("ID", "uuid"),
            ("Name", "name"),
            ("Region", "region"),
            ("Created", "created_at"),
            ("Status", "status"),
            ("Reason", "status_reason"),
        ]
        clusters_table = common.get_resources_info_table(
            clusters, fields)

        return clusters_table

    def format_templates_table(self, templates):
        """Format table of cluster templates.

        :param templates: List of cluster templates
        :returns: Formatted table of templates
        """
        for template in templates:
            template.region = template.manager.api.region_name
        fields = [
            ("ID", "uuid"),
            ("Name", "name"),
            ("Region", "region"),
            ("Created", "created_at"),
        ]
        templates_table = common.get_resources_info_table(
            templates, fields)

        return templates_table

    def print_pending_resources(
            self, cloud, servers_pending, volumes_pending):
        """Print a table that shows information about all rally leftovers.

        :param cloud: CloudClient object
        :param servers_pending: List of rally servers
        :param volumes_pending: List of rally volumes
        """
        server_fields = [
            ('ID', 'id'),
            ('Name', 'name'),
            ('Created', 'created'),
            ('Project ID', 'tenant_id'),
            ('Host ID', 'hostId'),
        ]
        servers_table = common.get_resources_info_table(
            servers_pending, server_fields)

        volume_fields = [
            ('ID', 'id'),
            ('Name', 'name'),
            ('Created', 'created_at'),
            ('Project ID', 'os-vol-tenant-attr:tenant_id'),
        ]
        volumes_table = common.get_resources_info_table(
            volumes_pending, volume_fields)
        return servers_table, volumes_table

    def clean_old_rally_jobs(self, args, cloud, snowclient):
        """Function that executes all phases of Rally leftovers cleanup.

        1- Obtains lists of servers and volumes to be deleted
        2- Displays information about these target resources
        3- Forces deletion of found rally leftovers
        4- Waits for an specified time interval (in seconds)
        5- Checks confirmation of deletion of all these resources

        :param cloud: CloudClient object
        """
        # Step 1
        servers_pending = self.get_pending_servers(cloud)
        volumes_pending = self.get_pending_volumes(cloud)
        leftover_clusters = self.get_leftover_clusters(cloud)
        leftover_templates = self.get_leftover_templates(cloud)

        # Step 2
        servers_table, volumes_table = self.print_pending_resources(
            cloud, servers_pending, volumes_pending)
        clusters_table = self.format_clusters_table(leftover_clusters)
        templates_table = self.format_templates_table(leftover_templates)
        if servers_pending:
            logger.info("################################")
            logger.info("#### PENDING SERVERS")
            logger.info("################################\n%s",
                        servers_table)
        else:
            logger.info("No pending Rally servers found!")

        if volumes_pending:
            logger.info("################################")
            logger.info("#### PENDING VOLUMES")
            logger.info("################################\n%s",
                        volumes_table)
        else:
            logger.info("No pending Rally volumes found!")

        if leftover_clusters:
            logger.info("################################")
            logger.info("#### LEFTOVER CLUSTERS")
            logger.info("################################\n%s",
                        clusters_table)
        else:
            logger.info("No leftover Magnum clusters found!")

        if leftover_templates:
            logger.info("################################")
            logger.info("#### LEFTOVER CLUSTER TEMPLATES")
            logger.info("################################\n%s",
                        templates_table)
        else:
            logger.info("No leftover Magnum cluster templates found!")

        if not servers_pending and not volumes_pending \
                and not leftover_clusters and not leftover_templates:
            logger.info("There is nothing to clean up, exiting.")
            return

        # Step 3
        cloud.delete_servers(servers_pending)
        cloud.delete_volumes(volumes_pending)
        cloud.delete_clusters(leftover_clusters)

        # Step 4
        logger.info("Waiting %s seconds for deletion "
                    "of Rally leftovers..." % args.timeout)
        time.sleep(float(args.timeout))

        # Deleting a template returns an error if the cluster was not deleted,
        # so delete one by one to not be blocked by a broken cluster/template.
        # Ignoring the error is fine as the template will be reported.
        for tmpl in leftover_templates:
            try:
                cloud.delete_cluster_templates([tmpl])
            except magnum_exceptions.BadRequest:
                pass

        # Template deletion should be quick, don't need the full timeout.
        # Take the min to not block the tests, which use a short timeout.
        time.sleep(min(10, float(args.timeout)))

        # Step 5
        broken_servers = self.check_servers_state(cloud, servers_pending)
        broken_volumes = self.check_volumes_state(cloud, volumes_pending)
        stuck_clusters = self.check_stuck_clusters(cloud, leftover_clusters)
        stuck_templates = self.check_stuck_templates(cloud, leftover_templates)
        if broken_servers or broken_volumes \
                or stuck_clusters or stuck_templates:
            logger.error("Ooops! For some reason the following resources "
                         "could not deleted:")

            logger.info("Building SNOW ticket for further investigations...")
            description = "[Rundeck] Failed to clean Rally leftovers"
            incident = snowclient.ticket.create_INC(
                short_description=description,
                functional_element=CLOUD_FE_NAME,
                assignment_group=CLOUD_3RD_LINE_AG_NAME
            )
            logger.info("Created incident number: %s", incident.info.number)
            incident.add_comment(args.execution_reference)
            incident.save()

            if broken_servers:
                broken_servers_table = prettytable.PrettyTable(
                    ["ID", "Name", "Project ID", "Host ID",
                     "Status", "Task State", "Updated"])
                broken_servers_table.border = True
                broken_servers_table.header = True
                broken_servers_table.align = 'l'
                for broken_server in broken_servers:
                    broken_servers_table.add_row([
                        broken_server.id,
                        broken_server.name,
                        broken_server.tenant_id,
                        broken_server.hostId,
                        broken_server.status,
                        broken_server.__dict__[
                            'OS-EXT-STS:task_state'],
                        broken_server.updated])

                logger.error("################################")
                logger.error("#### BROKEN SERVERS")
                logger.error("################################\n%s",
                             broken_servers_table)

                servers_error = "Could not clean the following list " \
                                "of Rally servers:\n[code]<pre>%s</pre>" \
                                "[/code]" \
                                % broken_servers_table.get_string().replace(
                                    '\n', "<br/>")
                incident.add_comment(servers_error)
                incident.save()

            if broken_volumes:
                broken_volumes_table = prettytable.PrettyTable(
                    ["ID", "Name", "Project ID", "Status"])
                broken_volumes_table.border = True
                broken_volumes_table.header = True
                broken_volumes_table.align = 'l'
                for broken_volume in broken_volumes:
                    broken_volumes_table.add_row([
                        broken_volume.id,
                        broken_volume.name,
                        broken_volume.__dict__[
                            'os-vol-tenant-attr:tenant_id'],
                        broken_volume.status])

                logger.error("################################")
                logger.error("#### BROKEN VOLUMES")
                logger.error("################################\n%s",
                             broken_volumes_table)

                vol_error = "Could not clean the following list " \
                            "of Rally volumes:\n[code]<pre>%s</pre>[/code]" \
                            % broken_volumes_table.get_string().replace(
                                '\n', "<br/>")
                incident.add_comment(vol_error)
                incident.save()

            if stuck_clusters:
                stuck_clusters_table = self.format_clusters_table(
                    stuck_clusters)
                logger.error("################################")
                logger.error("#### STUCK CLUSTERS")
                logger.error("################################\n%s",
                             stuck_clusters_table)

                clusters_error = "Could not clean the following list " \
                                 "of Magnum clusters:\n[code]<pre>%s</pre>" \
                                 "[/code]" \
                                 % stuck_clusters_table.get_string().replace(
                                     '\n', "<br/>")
                incident.add_comment(clusters_error)
                incident.save()

            if stuck_templates:
                stuck_templates_table = self.format_templates_table(
                    stuck_templates)

                logger.error("################################")
                logger.error("#### STUCK CLUSTER TEMPLATES")
                logger.error("################################\n%s",
                             stuck_templates_table)

                templates_error = (
                    "Could not clean the following list "
                    "of Magnum cluster templates:\n[code]<pre>{0}</pre>"
                    "[/code]").format(
                        stuck_templates_table.get_string().replace(
                            '\n', "<br/>"))
                incident.add_comment(templates_error)
                incident.save()

            logger.error("Manual intervention is required. "
                         "Please, check logs for more detailed information")
            sys.exit(1)
        else:
            logger.info("All Rally leftovers have been cleaned successfully!")

    def main(self, args=None):
        self.parser.add_argument("--execution-reference",
                                 help="Adds an execution reference to the "
                                      "work notes",
                                 default="Executed by '%s' on '%s'" % (
                                     getpass.getuser(), socket.getfqdn()))
        self.parser.add_argument("--instance", default="cern",
                                 help="Service now instance")

        self.parser.add_argument("--timeout", default="300",
                                 help="Waiting time interval")

        args = self.parse_args(args)

        self.clean_old_rally_jobs(
            args,
            self.cloudclient,
            ServiceNowClient(instance=args.instance)
        )


# Needs static method for setup.cfg
def main(args=None):
    RallyCleanupCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
