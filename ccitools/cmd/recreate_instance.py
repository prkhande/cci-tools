#!/usr/bin/python3

import argparse
import ccitools.conf
import functools
import logging
import os
import re
import requests
import socket
import sys
import tempfile
import tenacity

from ccitools.cmd.base import BaseCMD
from ccitools.cmd.common import Phase
from ccitools.cmd.common import Phases
from ccitools import common
from ccitools.utils.cloud import CloudRegionClient


# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF

INSTANCE_DELETION_TIMEOUT = 3600
INSTANCE_DELETION_RETRY_INTERVAL = 120

INSTANCE_CREATION_TIMEOUT = 7200
INSTANCE_CREATION_RETRY_INTERVAL = 120

SNAPSHOT_TIMEOUT = 72000
SNAPSHOT_RETRY_INTERVAL = 300

VOLUME_CREATION_TIMEOUT = 36000
VOLUME_CREATION_RETRY_INTERVAL = 300

SHUTDOWN_TIMEOUT = 600
SHUTDOWN_RETRY_INTERVAL = 20

APPLIANCE_URL = ('http://download.libguestfs.org/binaries/appliance/'
                 'appliance-1.40.1.tar.xz')


class RecreateInstance(BaseCMD):

    def __init__(self):
        super(RecreateInstance, self).__init__(
            description=("Delete and recreate the VM"))

        self.parser.add_argument(
            '--os-cloud',
            metavar='<cloud-config-name>',
            dest='cloud',
            default=common.env('OS_CLOUD', default='cern'),
            help='Cloud name in clouds.yaml (Env: OS_CLOUD)', )
        self.parser.add_argument(
            "--vm",
            required=True,
            help="VM that will be recreated")
        self.parser.add_argument(
            '--recreate-from',
            default='-',
            choices=['-', 'volume', 'image'])
        self.parser.add_argument(
            '--suffix',
            default=common.env('RD_JOB_EXECID', default=''),
            help=('Suffix used to create the resources.'
                  'If not set, defaults to $RD_JOB_EXECID')
        )
        self.parser.add_argument(
            '--new-project-id',
            nargs='?',
            help=('Specify a project if necessary, '
                  'otherwise it will be ignored')
        )
        self.parser.add_argument(
            '--flavor_map',
            default=r'm1\.large:m2.xlarge,m1\.tiny:m2.small,m1\.*:m2.',
            type=common.flavor_map,
        )
        self.parser.add_argument(
            '--new_name',
            default='',
            help=('New name in case we want to rename the instance')
        )
        self.parser.add_argument(
            '--volume-type',
            default='standard',
            choices=['standard', 'io1', 'cp1', 'cpio1'],
            help=('Type of volume used for recreation')
        )
        self.parser.add_argument(
            '--sysprep',
            default=False
        )
        self.parser.add_argument(
            '--sysprep-snapshot',
            default=False
        )
        self.parser.add_argument(
            '--sysprep-params',
            default=("--operations net-hwaddr,udev-persistent-net,customize "
                     "--delete '/etc/krb5.keytab' "
                     "--delete '/var/lib/cern-private-cloud-addons/state'"),
            help=('Parameters to be passed to the virt-sysprep command')
        )
        self.parser.add_argument(
            "--start-from",
            default=1,
            help="Start from step indicated in case of a previous execution")

    def parse_args(self, args):
        args = super(RecreateInstance, self).parse_args(args)
        self.cloud = args.cloud
        return args

    def get_cloudclient(self, project_name=None, project_id=None):
        namespace = None
        if project_id is not None or project_name is not None:
            logger.info("Getting new CloudClient for project: '%s'",
                        project_id if project_id is not None else project_name)
            namespace = argparse.Namespace(
                project_id=project_id,
                project_name=project_name)
        return CloudRegionClient(cloud=self.cloud, namespace=namespace)

    @tenacity.retry(
        stop=tenacity.stop_after_delay(INSTANCE_DELETION_TIMEOUT),
        wait=tenacity.wait_fixed(INSTANCE_DELETION_RETRY_INTERVAL))
    def wait_for_deletion(self, vm_name):
        try:
            socket.gethostbyname(vm_name)
            logger.info(
                "Server '%s' still present on DNS. "
                "Waiting %s seconds before checking again.",
                vm_name,
                INSTANCE_DELETION_RETRY_INTERVAL
            )
            raise tenacity.TryAgain
        except socket.gaierror:
            logger.info("VM not present on DNS anymore. Moving on !!")

    @tenacity.retry(stop=tenacity.stop_after_delay(SHUTDOWN_TIMEOUT),
                    wait=tenacity.wait_fixed(SHUTDOWN_RETRY_INTERVAL))
    def wait_until_stopped(self, vm):
        vm_data = vm.manager.get(vm.id)
        if not vm_data.status == 'SHUTDOWN':
            logger.info(
                "Server '%s' still in '%s' status",
                vm_data.name,
                vm_data.status
            )
            raise tenacity.TryAgain

    @tenacity.retry(
        stop=tenacity.stop_after_delay(INSTANCE_CREATION_TIMEOUT),
        wait=tenacity.wait_fixed(INSTANCE_CREATION_RETRY_INTERVAL))
    def wait_until_active(self, vm):
        vm_data = vm.manager.get(vm.id)
        if not vm_data.status == 'ACTIVE':
            logger.info(
                "Server '%s' still in '%s' status",
                vm_data.name,
                vm_data.status
            )
            raise tenacity.TryAgain

    @tenacity.retry(
        stop=tenacity.stop_after_delay(VOLUME_CREATION_TIMEOUT),
        wait=tenacity.wait_fixed(VOLUME_CREATION_RETRY_INTERVAL))
    def wait_until_volume_available(self, volume):
        volume_data = volume.manager.get(volume.id)
        if not volume_data.status == 'available':
            logger.info(
                "Volume '%s' still in '%s' status",
                volume_data.name,
                volume_data.status
            )
            raise tenacity.TryAgain

    @tenacity.retry(stop=tenacity.stop_after_delay(SNAPSHOT_TIMEOUT),
                    wait=tenacity.wait_fixed(SNAPSHOT_RETRY_INTERVAL))
    def wait_until_volume_snapshot_available(self, snapshot, cloud):
        snapshot_data = snapshot.manager.get(snapshot.id)
        if not snapshot_data.status == 'available':
            logger.info(
                "Volume Snapshot '%s' still in '%s' status",
                snapshot_data.name,
                snapshot_data.status
            )
            raise tenacity.TryAgain

    @tenacity.retry(stop=tenacity.stop_after_delay(SNAPSHOT_TIMEOUT),
                    wait=tenacity.wait_fixed(SNAPSHOT_RETRY_INTERVAL))
    def wait_until_image_active(self, image_id, cloud):
        image = cloud.get_images_by_id(image_id)
        if not image.status == 'active':
            logger.info(
                "Server '%s' still in '%s' status",
                image.name,
                image.status
            )
            raise tenacity.TryAgain

    def get_compatible_flavor(self, name, flavor_map):
        result = name
        for k, v in flavor_map:
            if re.match(k, name):
                result = re.sub(k, v, name)
                break
        return result

    def main_recreate(self, args):

        # Layout the different commands and phases to run
        cmd = Phases([
            Phase(
                "Check if the operation could be executed",
                func=self.precheck),
            Phase(
                "Adapt quotas if required",
                func=self.fix_quota),
            Phase(
                "Add service account as project member",
                func=self.add_account),
            Phase(
                "Shutdown machine",
                func=self.shutdown_machine),
            Phase(
                "Sysprep machine",
                func=self.sysprep_machine),
            Phase(
                "Create snapshot of instance",
                func=self.create_snapshot),
            Phase(
                "Create volume from snapshot",
                func=self.create_volume),
            Phase(
                "Recreate VM",
                func=self.recreate_instance)
        ])

        # Execute the phases in order
        cmd.execute(args.start_from, args)

        # Finally log the status of the execution
        logger.info(str(cmd))

    def precheck(self, args, phase):
        logger.info("Checking Information about VM '%s'...", args.vm)
        cloud = self.get_cloudclient()
        vm = cloud.get_servers_by_name(args.vm)
        phase.ok('Precheck executed successfully')
        return vm.tenant_id

    def fix_quota(self, args, phase):
        cloud = self.get_cloudclient()
        vm = cloud.get_servers_by_name(args.vm)

        if args.recreate_from != 'volume':
            logger.info("There is no need to update the quota. "
                        "No changes needed. Moving on !!")
            phase.ok('Fix quota skipped')
            return

        if not vm.image:
            logger.info("There is no need to update quota as"
                        "we are reusing the volume. Moving on !!")
            phase.ok('Fix quota skipped')
            return

        cinder_quota = cloud.get_cinder_project_quota(
            project_id=vm.tenant_id,
            region='cern',
            usage=True
        )

        volumes_needed = 1
        gigabytes_needed = int(vm.flavor['disk']) + 1

        volumes_key = 'volumes_{0}'.format(args.volume_type)
        volumes_limit = int(getattr(cinder_quota, volumes_key)['limit'])
        volumes_inuse = int(getattr(cinder_quota, volumes_key)['in_use'])

        gigabytes_key = 'volumes_{0}'.format(args.volume_type)
        gigabytes_limit = int(getattr(cinder_quota, gigabytes_key)['limit'])
        gigabytes_inuse = int(getattr(cinder_quota, gigabytes_key)['in_use'])

        update_quota = False

        if (volumes_inuse + volumes_needed - volumes_limit > 0):
            volume_update = volumes_limit + volumes_needed
            logger.warning(
                "Not enough quota for creating one extra volume."
                "Should increase volumes from '%s' to '%s'",
                volumes_limit, volume_update)
            update_quota = True
        else:
            volume_update = volumes_limit

        if (gigabytes_inuse + gigabytes_needed - gigabytes_limit > 0):
            gigabytes_update = gigabytes_limit + gigabytes_needed
            logger.warning(
                "Not enough space for creating one extra volume."
                "Should increase gigabytes from '%s' to '%s'",
                gigabytes_limit, gigabytes_update)
            update_quota = True
        else:
            gigabytes_update = gigabytes_limit

        if update_quota:
            project = cloud.find_project(vm.tenant_id)
            logger.info("Increasing quota of '%s'...", project.name)
            cloud.update_quota_cinder(
                vm.tenant_id,
                volume_update,
                gigabytes_update,
                volume_update * 2,
                args.volume_type,
                'cern')
        else:
            logger.info("Project has enough quota\n"
                        "No changes needed. Moving on !!")
        phase.ok('Precheck executed successfully')

    def add_account(self, args, phase):
        try:
            cloud = self.get_cloudclient()
            project_id = cloud.get_servers_by_name(args.vm).tenant_id
            logger.info("Adding account to project '%s'...", project_id)
            user_id = cloud.session.auth.get_user_id(cloud.session)
            cloud.grant_role_in_project(
                project_id=project_id,
                user_id=user_id)
            if args.new_project_id:
                cloud.grant_role_in_project(
                    project_id=args.new_project_id,
                    user_id=user_id)
            phase.ok('Account added successfully')
        except Exception as ex:
            error_msg = "Add account failed: %s" % (str(ex))
            phase.fail(error_msg)
            raise ex

    def shutdown_machine(self, args, phase):
        try:
            logger.info("Stopping VM '%s'...", args.vm)
            cloud = self.get_cloudclient()
            vm = cloud.get_servers_by_name(args.vm)
            vm.stop()
            self.wait_until_stopped(vm)
            phase.ok('VM stopped successfully')
        except Exception as ex:
            error_msg = "VM stop failed: %s" % (str(ex))
            phase.fail(error_msg)
            raise ex

    def sysprep_machine(self, args, phase):
        try:
            fd, path = tempfile.mkstemp()
            if not args.sysprep:
                logger.info("Sysprep not configured, skipping....")
                phase.ok('Sysprep skipped')
                return

            cloud_admin = self.get_cloudclient()
            project_id = cloud_admin.get_servers_by_name(args.vm).tenant_id

            if args.sysprep_snapshot:
                logger.info("Creating sysprep snapshot of VM '%s'...", args.vm)
                cloud = self.get_cloudclient(project_id=project_id)
                vm = cloud.get_servers_by_name(args.vm)

                if not vm.image:
                    # Boot from volume => volume snapshot
                    attachments = [vol['id'] for vol in getattr(
                        vm, 'os-extended-volumes:volumes_attached')]

                    boot_volume = None
                    for volume in cloud.get_volumes(project_id=project_id):
                        if (volume.id in attachments
                                and volume.attachments[0][
                                    'device'] == '/dev/vda'):
                            boot_volume = volume
                            break

                    snapshot_id = cloud.create_volume_snapshot(
                        boot_volume,
                        "{0}_base_{1}".format(
                            vm.name.lower(),
                            args.suffix.lower()))
                    self.wait_until_volume_snapshot_available(
                        snapshot_id, cloud)
                else:
                    # Boot from image => instance snapshot
                    snapshot_id = vm.create_image(
                        "{0}_base_{1}".format(
                            vm.name.lower(),
                            args.suffix.lower()),
                    )
                    self.wait_until_image_active(snapshot_id, cloud)

            # Now that the snapshot has been executed we can start sysprep
            vm = cloud_admin.get_servers_by_name(args.vm)

            logger.info('Downloading appliance file into tempfile')
            r = requests.get(APPLIANCE_URL, stream=True)
            with os.fdopen(fd, 'wb') as tmp:
                file_written = 0
                total_length = int(r.headers.get('content-length'))
                for chunk in r.iter_content(chunk_size=4194304):
                    if chunk:
                        file_written += tmp.write(chunk)
                        logger.info(
                            "%s MB out of %s MB downloaded",
                            round((file_written / 1024**2) + 1),
                            round((total_length / 1024**2) + 1))
                logger.info('Appliance file downloaded')
                host = getattr(vm, 'OS-EXT-SRV-ATTR:host')
                domain = getattr(vm, 'OS-EXT-SRV-ATTR:instance_name')
                # Copy the appliance file into the server
                common.scp_file(host, path, path)
                # Extract it into /tmp
                common.ssh_executor(
                    host,
                    'tar xvfJ {0} -C /tmp/ && rm {0}'.format(path))
                # Execute the sysprep command
                common.ssh_executor(
                    host,
                    ('LIBGUESTFS_PATH=/tmp/appliance/'
                     'virt-sysprep {0} -d {1}').format(
                        args.sysprep_params,
                        domain
                    )
                )
            phase.ok('Sysprep executed successfully')
        except Exception as ex:
            error_msg = "Sysprep failed: %s" % (str(ex))
            phase.fail(error_msg)
            raise ex
        finally:
            os.remove(path)

    def create_snapshot(self, args, phase):
        try:
            logger.info("Creating snapshot of VM '%s'...", args.vm)
            cloud_admin = self.get_cloudclient()
            project_id = cloud_admin.get_servers_by_name(args.vm).tenant_id
            cloud = self.get_cloudclient(project_id=project_id)
            vm = cloud.get_servers_by_name(args.vm)

            if not vm.image:
                # Boot from volume => volume snapshot
                attachments = [vol['id'] for vol in getattr(
                    vm, 'os-extended-volumes:volumes_attached')]
                boot_volume = None
                for volume in cloud.get_volumes(project_id=project_id):
                    if (volume.id in attachments
                            and volume.attachments[0][
                                'device'] == '/dev/vda'):
                        boot_volume = volume
                        break

                snapshot = cloud.create_volume_snapshot(
                    boot_volume,
                    "{0}_snap_{1}".format(
                        vm.name.lower(),
                        args.suffix.lower()))
                self.wait_until_volume_snapshot_available(
                    snapshot, cloud)
            else:
                # Boot from image => instance snapshot
                snapshot_id = vm.create_image(
                    "{0}_snap_{1}".format(
                        vm.name.lower(),
                        args.suffix.lower()),
                )
                self.wait_until_image_active(snapshot_id, cloud)

                if args.new_project_id:
                    cloud_admin.update_image(snapshot_id, visibility='shared')
                    cloud_admin.image_add_project(snapshot_id,
                                                  args.new_project_id)

            phase.ok('Snapshot created successfully')
        except Exception as ex:
            error_msg = "Snapshot failed: %s" % (str(ex))
            phase.fail(error_msg)
            raise ex

    def create_volume(self, args, phase):
        if args.recreate_from != 'volume':
            logger.info('Step only needed when recreating from volume')
            phase.ok('Step only needed when recreating from volume')
            return

        try:
            cloud_admin = self.get_cloudclient()
            project_id = cloud_admin.get_servers_by_name(args.vm).tenant_id
            cloud = self.get_cloudclient(project_id=project_id)
            vm = cloud_admin.get_servers_by_name(args.vm)

            if not vm.image:
                logger.info('Skip volume creation on boot from volume VM')
                phase.ok('Skip Volume creation on boot from volume VM')
            else:
                logger.info("Creating volume for VM '%s'...", args.vm)
                image = cloud.get_images_by_name(
                    "{0}_snap_{1}".format(
                        vm.name.lower(),
                        args.suffix.lower()))[0]

                volume_size = functools.reduce(max, [
                    round(image.size / (1024.0**3)),
                    vm.flavor['disk'],
                    image.min_disk])

                volume = cloud.create_volume(
                    name="{0}_vol_{1}".format(
                        vm.name.lower(),
                        args.suffix.lower()),
                    size=volume_size,
                    volume_type=args.volume_type,
                    image=image)
                self.wait_until_volume_available(volume)

                phase.ok('Volume created successfully')
        except Exception as ex:
            error_msg = "Volume creation failed: %s" % (str(ex))
            phase.fail(error_msg)
            raise ex

    def recreate_instance(self, args, phase):
        try:
            if args.recreate_from.lower() != '-':
                cloud = self.get_cloudclient()
                vm = cloud.get_servers_by_name(args.vm)

                vm_metadata = vm.metadata
                vm_az = getattr(vm, 'OS-EXT-AZ:availability_zone')
                vm_volumes = getattr(
                    vm, 'os-extended-volumes:volumes_attached')

                # If we try to use '-' as availability zone, it collapses
                if vm_az == '-':
                    vm_az = None

                new_flavor = self.get_compatible_flavor(
                    vm.flavor['original_name'],
                    args.flavor_map)

                flavors = []
                flavors_public = vm.manager._list(
                    "/flavors/detail?is_public=true",
                    "flavors")
                flavors_non_public = vm.manager._list(
                    "/flavors/detail?is_public=false",
                    "flavors")
                flavors.extend(flavors_public)
                flavors.extend(flavors_non_public)

                flavor = [
                    flavor for flavor in flavors if flavor.name == new_flavor
                ][0]

                logger.info("Setting VM properties...")
                if 'landb-mainuser' not in vm_metadata.keys():
                    logger.info(
                        "'landb-mainuser' not set. "
                        "Setting value to '%s'...",
                        vm.user_id)
                    vm_metadata['landb-mainuser'] = vm.user_id

                if 'landb-responsible' not in vm_metadata.keys():
                    logger.info(
                        "'landb-responsible' not set. "
                        "Setting value to '%s'...", vm.user_id)
                    vm_metadata['landb-responsible'] = vm.user_id

                # Calculate block device mapping
                block_dev_mapping = []
                for volume in vm_volumes:
                    block_dev_mapping.append({
                        'uuid': volume['id'],
                        'source_type': 'volume',
                        'destination_type': 'volume',
                        'delete_on_termination': False
                    })

                if not vm.image:
                    logger.info("Recreating boot from volume")
                    # The original VM is boot from volume
                    # let's recreate it as is
                    image = ''
                    volumes = cloud.get_volumes(project_id=vm.tenant_id)

                    bfv_dict = None
                    for mapping in block_dev_mapping:
                        entry = [
                            vol for vol in volumes
                            if vol.id == mapping['uuid']
                        ][0]
                        if not entry.attachments:
                            continue
                        device = entry.attachments[0]['device']
                        if device == '/dev/vda':
                            bfv_dict = mapping
                            break

                    if not bfv_dict:
                        logger.error('Could not find volume to boot into')
                        raise Exception('Could not find volume to boot into')

                    block_dev_mapping.remove(bfv_dict)
                    bfv_dict['boot_index'] = 0
                    block_dev_mapping.insert(0, bfv_dict)

                # The machine is a boot from image
                elif args.recreate_from == 'volume':
                    logger.info(
                        "Recreating boot from image as boot from volume")
                    image = ''

                    vol_name = "%s_vol_%s" % (
                        vm.name.lower(),
                        args.suffix.lower())
                    volumes = cloud.get_volumes(project_id=vm.tenant_id)

                    volume = [
                        vol for vol in volumes if vol_name == vol.name.lower()
                    ][0]

                    bfv_dict = {
                        'uuid': volume.id,
                        'source_type': 'volume',
                        'destination_type': 'volume',
                        'boot_index': 0,
                        'delete_on_termination': False
                    }
                    block_dev_mapping.insert(0, bfv_dict)

                elif args.recreate_from == 'image':
                    logger.info("Recreating boot from image")
                    img_name = '%s_snap_%s' % (
                        vm.name.lower(),
                        args.suffix.lower())
                    images = cloud.get_images_by_name(img_name.lower())
                    image = [
                        img for img in images if img_name == img.name.lower()
                    ][0]

                if args.new_project_id:
                    cloud = self.get_cloudclient(
                        project_id=args.new_project_id)
                else:
                    cloud = self.get_cloudclient(project_id=vm.tenant_id)

                # To avoid recreation of the computer account we set a property
                # before deletion
                vm.manager.set_meta(vm, {"cern-activedirectory": "false"})

                # All the info required to rebuild the instance has been
                # fetched. Triggering the deletion of the instance
                logger.info("Deleting VM '%s'...", args.vm)
                vm.delete()
                self.wait_for_deletion(args.vm)

                if args.new_project_id and len(vm_volumes) > 0:
                    # We need to transfer the volumes from source_project
                    # to target
                    volumes_to_transfer = [
                        volume['id'] for volume in vm_volumes]
                    cloud.transfer_volumes(volumes_to_transfer,
                                           vm.tenant_id,
                                           args.new_project_id)

                # And now create the machine
                cloud.create_server(
                    name=args.new_name if args.new_name else args.vm,
                    image=image,
                    flavor=flavor,
                    block_device_mapping_v2=block_dev_mapping,
                    nics='auto',
                    availability_zone=vm_az
                )

                logger.info("VM Launched, waiting until it becomes active")
                vm = self.get_cloudclient().get_servers_by_name(
                    args.new_name if args.new_name else args.vm
                )
                self.wait_until_active(vm)

                logger.info("VM is active setting metadata information")
                vm.manager.set_meta(vm, vm_metadata)

                logger.info("Done :)")
                phase.ok("VM recreated successfully")
            else:
                logger.info("'%s' NOT recreated", args.vm)
                phase.ok("VM not recreated as requested")
        except Exception as ex:
            error_msg = "Volume creation failed: %s" % (str(ex))
            phase.fail(error_msg)
            raise ex

    def main(self, args=None):
        try:
            args = self.parse_args(args)
            self.main_recreate(args)
        except Exception as e:
            logger.exception(e)
            sys.exit(1)


# Needs static method for setup.cfg
def main(args=None):
    RecreateInstance().main(args)


if __name__ == "__main__":
    main()
