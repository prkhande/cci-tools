#!/usr/bin/python3

import logging
import prettytable
import sys

from ccitools.cmd import base
from ccitools.utils import aihelper
from cryptography.fernet import Fernet

logger = logging.getLogger(__name__)


class ResetFernetKeysCmd(base.BaseCMD):

    def __init__(self):
        super(ResetFernetKeysCmd, self).__init__(
            description="Reset all keystone "
                        "fernet keys on Teigi")

    def main_reset_keys(self, args, aiclient):
        keys = args.keys
        table = prettytable.PrettyTable(["teigi_secret_tag", "Old", "New"])

        old_values = [0] * (keys + 1)
        new_values = [0] * (keys + 1)

        logger.info("Reseting all Keystone Fernet Keys on TEIGI...")

        for i in range(0, keys + 1):
            new_values[i] = Fernet.generate_key()
            old_values[i] = aiclient.get_teigi_key(args, i)
            logger.info(old_values[i])

        for i in range(0, keys + 1):
            logger.info(new_values[i])
            aiclient.update_teigi_key(args, i, new_values[i])
            tag = "%s_%s" % (args.teigi_tag, i)
            table.add_row([tag, old_values[i], new_values[i]])

    def main(self, args=None):
        self.parser.add_argument("--hostgroup", required=True,
                                 help="Hostgroup")
        self.parser.add_argument("--keys", type=int, default=6,
                                 help="Number of fernet keys")
        self.parser.add_argument("--teigi-tag",
                                 help="Teigi tag. Ex: keystone_fernet_")

        args = self.parse_args(args)

        aiclient = aihelper.AIClient()
        self.main_reset_keys(args, aiclient)


# Needs static method for setup.cfg
def main(args=None):
    ResetFernetKeysCmd().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.error(e)
        sys.exit(-1)
