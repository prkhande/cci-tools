#!/usr/bin/python3

import logging
import tenacity
import time

from ccitools.cmd import base
from ccitools.common import ssh_executor

logger = logging.getLogger(__name__)

TIMEOUT = 3600 * 3               # total waiting time: 3 hours
CHECK_WAIT_TIME = 600            # check if machine is ssh every: 10min
TIME_BEFORE_CHECKING = 3600 * 2  # we wait until we start checking: 2 hours


class WaitUtilMachineIsSSHable(base.BaseCMD):

    def __init__(self):
        super(WaitUtilMachineIsSSHable, self).__init__(
            description="Waits until the machine is available from SSH")

    # we retry for total_timeout - initial waiting time (1 hour)
    @tenacity.retry(
        stop=tenacity.stop_after_delay(TIMEOUT - TIME_BEFORE_CHECKING),
        wait=tenacity.wait_fixed(CHECK_WAIT_TIME),
        before_sleep=tenacity.before_sleep_log(
            logger,
            logging.WARNING))
    def wait_ssh(self, hosts):
        """Check if hosts are sshable."""
        hosts = hosts.split()
        invalid_hosts = []

        for host in hosts:
            try:
                # execute random command to check if
                ssh_executor(host, "uptime")
            except Exception:
                logger.error("Can't connect to %s" % host)
                invalid_hosts.append(host)

        if invalid_hosts:
            logger.error("The following hosts aren't available yet: %s"
                         % ",".join(invalid_hosts))
            logger.error("Waiting %d seconds before retrying", CHECK_WAIT_TIME)
            raise tenacity.TryAgain

    def main(self, args=None):
        self.parser.add_argument("--hosts",
                                 required=True,
                                 help="List of hosts")

        behaviour = self.parser.add_mutually_exclusive_group()
        behaviour.add_argument('--perform',
                               dest="exec_mode",
                               action='store_true')
        behaviour.add_argument('--dryrun',
                               dest="exec_mode",
                               action='store_false')

        args = self.parse_args(args)

        if args.exec_mode:
            logger.info(
                "Waiting %d seconds before starting to check if machine "
                "sshable" % TIME_BEFORE_CHECKING)
            time.sleep(TIME_BEFORE_CHECKING)
            try:
                self.wait_ssh(args.hosts)
            except tenacity.RetryError:
                raise SystemExit("Some hosts are not SSHable. Exiting...")
            else:
                logger.info("All hosts are up and are sshable")
        else:
            logger.info("[DRYRUN] Finished checking if hosts are SSHable")


# Needs static method for setup.cfg
def main(args=None):
    WaitUtilMachineIsSSHable().main(args)


if __name__ == "__main__":
    main()
