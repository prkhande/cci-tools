import argparse
import arrow
import json
import logging
import os
import paramiko
import prettytable
import random
import re
import requests
import scp
import stat
import string
import subprocess  # nosec
import time
import yaml
import yaml.parser

from arrow.parser import ParserError
from ccitools import conf
from ccitools.errors import SSHException
from http.cookiejar import MozillaCookieJar
from uuid import UUID
from zeep.client import Client
from zeep.transports import Transport

# configure logging
logger = logging.getLogger(__name__)

CONF = conf.CONF


# Generally useful stuff often found in a utils module
def env(*vars, **kwargs):
    """Search for the first defined of possibly many env vars.

    Returns the first environment variable defined in vars, or
    returns the default defined in kwargs.
    """
    for v in vars:
        value = os.environ.get(v, None)
        if value:
            return value
    return kwargs.get('default', '')


def flavor_map(arg):
    try:
        fl_map = []
        for elem in arg.split(','):
            k, v = map(str, elem.split(':'))
            fl_map.append((k, v))
        return fl_map
    except Exception:
        raise argparse.ArgumentTypeError(
            "Flavor Map must have the format k1:v1, ... ,kn:vn")


def escaped_json_loads(arg):
    try:
        return json.loads(re.sub(r"\s+", " ", arg))
    except Exception:
        raise argparse.ArgumentTypeError("Failure to load escaped string")


def generate_random_string(length=8):
    return ''.join(random.SystemRandom().choice(string.ascii_letters)
                   for _ in range(length))


def add_logging_arguments(parser):
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    parser.add_argument("-d", "--debug", help="increase output "
                        "to debug messages", action="store_true")


def cern_get_sso_cookie(server, cookie_file=None):
    """Get CERN SSO cookies."""
    if not cookie_file:
        cookie_file = "/tmp/cookie-" + generate_random_string()  # nosec

    args = ["cern-get-sso-cookie", "--reprocess", "--url", server,
            "--outfile", cookie_file]
    p = subprocess.Popen(  # nosec
        args,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    p.wait()
    os.chmod(cookie_file, stat.S_IRUSR)
    cookie = MozillaCookieJar(cookie_file)
    cookie.load()
    os.remove(cookie_file)
    return cookie


def auth_get_sso_cookie(server, cookie_file=None):
    """Get AUTH SSO cookies."""
    if not cookie_file:
        cookie_file = "/tmp/cookie-" + generate_random_string()  # nosec

    args = ["auth-get-sso-cookie", "-u", server, "-o", cookie_file]
    p = subprocess.Popen(  # nosec
        args,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    p.wait()
    os.chmod(cookie_file, stat.S_IRUSR)
    cookie = MozillaCookieJar(cookie_file)
    cookie.load()
    os.remove(cookie_file)
    return cookie


def date_parser(datetime):
    """Check date format and parses it to meet ISO-8601 criteria.

    :param datetime: Datetime introduced by user
    :returns: Arrow object with datetime formatted to ISO-8601
    """
    try:
        logger.info("Parsing introduced datetime...")
        arrow.get(datetime).isoformat()
    except ParserError:
        # Input date follows common format instead of ISO-8601
        datetime_iso = arrow.get(datetime,
                                 'DD-MM-YYYY HH:mm'
                                 ).replace(tzinfo='Europe/Zurich')
        logger.info("Introduced datetime follows common "
                    "format DD-MM-YYYY HH:mm. Datetime requires to be "
                    "formatted to ISO-8601.")
    else:
        datetime_iso = arrow.get(datetime).replace(
            tzinfo='Europe/Zurich')
        logger.info("Introduced datetime follows ISO-8601."
                    "No need to format.")

    return datetime_iso


def date_validator(datetime):
    """Check whether provided date is in the future.

    :param datetime: Datetime introduced by user
    :returns: booelan of date validation
    """
    now = arrow.now('Europe/Zurich')
    if datetime > now:
        return True
    else:
        return False


def ssh_executor(host, command, connect_timeout=10, banner_timeout=15,
                 session_timeout=600, keep_alive_interval=None):
    # connect to the machine
    # Retry a few times if it fails.
    retries = 1
    while True:
        logger.info("Trying to connect to %s (%i/3)", host, retries)
        try:
            client = paramiko.SSHClient()
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            client.connect(host,
                           username="root",
                           banner_timeout=banner_timeout,
                           timeout=connect_timeout,  # ConnectTimeout 10
                           gss_auth=True)  # use krb to connect
            logger.info("Success! Connected to '%s'", host)
            break
        except paramiko.AuthenticationException:
            logger.error("Authentication failed when connecting to %s", host)
        except paramiko.ChannelException:
            logger.warning("ssh: Could not access hostname %s", host)
        # If we could not connect within time limit
        if retries == 3:
            raise SSHException("Could not connect to %s. Giving up." % host)
        else:
            retries += 1
            time.sleep(5)

    # Set ServerAliveInterval if provided
    if keep_alive_interval:
        client.get_transport().set_keepalive(keep_alive_interval)

    # Send command
    logger.info("Sent command '%s'", command)
    stdin, stdout, stderr = client.exec_command(command)  # nosec

    # Wait for the command to terminate
    start = time.time()
    while time.time() < start + session_timeout:
        if stdout.channel.exit_status_ready():
            break
        time.sleep(1)
    else:
        client.close()
        raise SSHException("Command -> '%s' timed out on host %s"
                           % (command, host))

    # Close channel
    logger.info("Command done! Closing SSH connection.")
    total_output = stdout.readlines()
    total_error = stderr.readlines()
    client.close()

    return total_output, total_error


def scp_file(host, localfile, remote_path, connect_timeout=10,
             banner_timeout=15, session_timeout=600, keep_alive_interval=None):
    # connect to the machine
    # Retry a few times if it fails.
    retries = 1
    while True:
        logger.info("Trying to connect to %s (%i/3)", host, retries)
        try:
            client = paramiko.SSHClient()
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            client.connect(host,
                           username="root",
                           banner_timeout=banner_timeout,
                           timeout=connect_timeout,  # ConnectTimeout 10
                           gss_auth=True)  # use krb to connect
            logger.info("Success! Connected to '%s'", host)
            break
        except paramiko.AuthenticationException:
            logger.error("Authentication failed when connecting to %s", host)
        except paramiko.ChannelException:
            logger.warning("ssh: Could not access hostname %s", host)
        # If we could not connect within time limit
        if retries == 3:
            raise SSHException("Could not connect to %s. Giving up." % host)
        else:
            retries += 1
            time.sleep(5)

    # Set ServerAliveInterval if provided
    if keep_alive_interval:
        client.get_transport().set_keepalive(keep_alive_interval)

    # Send the file
    scp_client = scp.SCPClient(client.get_transport())
    scp_client.put(localfile, remote_path)

    # Close channel
    logger.info("File copied! Closing SSH connection.")
    scp_client.close()
    client.close()


def ping(host):
    """Return True if host (str) responds to a ping request."""
    args = ['/usr/bin/ping', '-c', '1', host]
    output = subprocess.call(
        args,
        shell=False,  # nosec
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    return True if output == 0 else False


def normalize_hostname(hostname):
    """Normalize hostname for checks by setting to lower cases.

    :param hostname: Name of the host
    :returns: Normalized hostname
    """
    # Lower-case the name
    hostname = hostname.lower()

    # Strip cern domain for cleanliness
    match = re.search("(.*).cern.ch", hostname)
    return match.group(1) if match else hostname


def normalize_fqdn(hostname):
    """Normalize fqdn for checks by setting to lower cases.

    :param hostname: Name of the host
    :returns: Normalized fqdn
    """
    # Lower-case the name
    hostname = hostname.lower()

    # Add cern domain in the fqdn if not present
    if not re.search("(.*).cern.ch", hostname):
        hostname = "%s.cern.ch" % hostname
    return hostname


def truncate(string, width=20):
    """Add dots to string if is longer than with.

    :param width: maximum width of the string
    :returns: a shortened string
    """
    if len(string) > width:
        string = string[:width - 3] + '...'
    return string


def merge_dicts(x, y):
    """Merge two dicts into a new dict as a shallow copy."""
    z = x.copy()
    z.update(y)
    return z


def get_soap_client(instance, table, transport):
    """Get SOAP client for a specific table in a CERN service now.

    :param instance: the CERN instance of service now (e.g: `cern` or
        `cerntest`)
    :returns: a zeep client to perform operations over the service now table
    """
    url = "https://%s.service-now.com/%s.do?WSDL" % (instance, table)

    return Client(url, transport=transport).service


def get_transport(url):
    """Get a transport for the url SSO validated.

    :param url: url to get a valid zeep transport for
    :returns: a zeep transport to be used to do operations over service now
    """
    session = requests.Session()
    # It will require a valid KRB token
    session.cookies = cern_get_sso_cookie(url)
    return Transport(session=session)


def get_resources_info_table(resources, fields):
    """Generate a printable table with.

    detailed information about the given resources

    Server example => resources = list_of_server_objects
                      fields = {
                        'ID': 'id',
                        'Name': 'name',
                        'Created': 'created',
                        'Status': 'status',
                        'Project ID': 'tenant_id',
                        'Host ID': 'hostId',
                      }

    Volume example => resources = list_of_volume_objects
                      fields = {
                        'ID': 'id',
                        'Name': 'name',
                        'Created': 'created_at',
                        'Status': 'status',
                        'Project ID': 'os-vol-tenant-attr:tenant_id',
                        'Host ID': 'os-vol-host-attr:host',
                      }

    :param resources: List of resources (MUST BE SAME TYPE e.g. 'Server')
    :param fields: List of tuples that contains information fields to display,
    where each key is the name of a column and each value is its corresponding
    attribute name for the resource (as declared by OpenStack API)
    :returns: Table with detailed information
    """
    row_list = []
    columns = []
    for resource in resources:
        row_values = []
        for column, field in fields:
            try:
                row_values.append(getattr(resource, field))
                if column not in columns:
                    columns.append(column)
            except Exception:  # nosec
                # In case of non-existing attribute ignore its column
                continue
        row_list.append(row_values)

    table = prettytable.PrettyTable(columns)
    for r in row_list:
        table.add_row(r)

    table.border = True
    table.header = True
    table.align = 'l'
    return table


def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def YAMLParse(string):
    value = {}
    try:
        data = yaml.safe_load(string)
        if isinstance(data, str):
            raise yaml.parser.ParserError("String '%s' doesn't look like a "
                                          "dictionary." % string)
        value.update(data)
    except yaml.parser.ParserError:
        args = [keypair.split("=", 1)
                for keypair in string.split(",")]
        if len([a for a in args if len(a) != 1]) != len(args):
            raise yaml.parser.ParserError("Value has to be YAML or JSON")
        else:
            value.update(dict(args))
    return value


def calculate_variation(current, requested):
    if current:
        return requested - current, (
            float(requested - current) / current) * 100
    else:
        if requested:
            return requested, 100
    return 0, 0


def summary_callback(region, service, key, resource, meta, table):
    if region in meta['quota'] and service in meta['quota'][region]:
        if not resource and key in meta['quota'][region][service]:
            table.add_row([
                region,
                service,
                key,
                meta['quota'][region][service][key]
            ])
        elif resource in meta['quota'][region][service] and \
                key in meta['quota'][region][service][resource]:
            table.add_row([
                region,
                service,
                '%s - (%s)' % (key, resource),
                meta['quota'][region][service][resource][key]
            ])


def summary_update_callback(region, service, key, resource, meta, table):
    if region in meta['quota'] and service in meta['quota'][region]:
        if not resource and key in meta['current'][region][service] and \
                key in meta['quota'][region][service]:
            diff, percent = calculate_variation(
                meta['current'][region][service][key],
                meta['quota'][region][service][key])
            variation = "%+d (%+d%%)" % (diff, percent)
            table.add_row([
                region,
                service,
                key,
                meta['current'][region][service][key],
                meta['quota'][region][service][key],
                variation
            ])
        elif resource in meta['current'][region][service] and \
                resource in meta['quota'][region][service] and \
                key in meta['current'][region][service][resource] and \
                key in meta['quota'][region][service][resource]:
            diff, percent = calculate_variation(
                meta['current'][region][service][resource][key],
                meta['quota'][region][service][resource][key])
            variation = "%+d (%+d%%)" % (diff, percent)
            table.add_row([
                region,
                service,
                '%s - (%s)' % (key, resource),
                meta['current'][region][service][resource][key],
                meta['quota'][region][service][resource][key],
                variation
            ])


def is_uuid(value, version=4):
    try:
        uuid_obj = UUID(value, version=version)
    except ValueError:
        return False
    return str(uuid_obj) == value


def get_chargegroups(uuid=None, name=None):
    params = {}
    if uuid:
        params['uuid'] = uuid
    if name:
        params['name'] = name

    try:
        response = requests.get(
            CONF.chargegroup.url,
            params=params,
            verify=CONF.chargegroup.verify
        )

        # Filter out the ones that are disabled and only
        # pass the properties we are using
        chargegroups = [
            {
                'uuid': ch['uuid'],
                'name': ch['name'],
                'category': ch['category'],
                'org_unit': ch['org_unit']
            } for ch in json.loads(response.text)['data']
            if ch.get('active', 'False') == 'True']
    except Exception:
        chargegroups = []

    # Just in case it returns all entries, we need to filter them out
    if uuid:
        chargegroups = [ch for ch in chargegroups if ch['uuid'] == uuid]
    if name:
        chargegroups = [ch for ch in chargegroups if ch['name'] == name]

    return chargegroups


def get_user_mapping():

    try:
        response = requests.get(
            CONF.chargegroup.resolver_url,
            verify=CONF.chargegroup.verify
        )

        user_mapping = json.loads(response.text)

    except Exception:
        user_mapping = {}

    return user_mapping
