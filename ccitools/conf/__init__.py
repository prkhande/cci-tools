from oslo_config import cfg

from ccitools.conf import chargegroup
from ccitools.conf import cornerstone
from ccitools.conf import dbreporter
from ccitools.conf import fim
from ccitools.conf import foreman
from ccitools.conf import influxdb
from ccitools.conf import jira
from ccitools.conf import keystone
from ccitools.conf import ldap
from ccitools.conf import loadbalancer
from ccitools.conf import megabus
from ccitools.conf import puppetdb
from ccitools.conf import rundeck
from ccitools.conf import s3

conf_modules = [
    chargegroup,
    cornerstone,
    dbreporter,
    fim,
    foreman,
    influxdb,
    jira,
    keystone,
    ldap,
    megabus,
    puppetdb,
    rundeck,
    s3,
    loadbalancer,
]

CONF = cfg.CONF

for module in conf_modules:
    module.register_opts(CONF)

CONF(args=[], project='ccitools')
