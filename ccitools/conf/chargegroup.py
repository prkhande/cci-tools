from oslo_config import cfg


url = cfg.StrOpt(
    'url',
    default='https://gar.cern.ch/public/list_full',
    help="URL to fetch chargegroups")

resolver_url = cfg.StrOpt(
    'resolver_url',
    default='https://gar.cern.ch/public/user_resolver/list_all',
    help="URL to resolve user chargegroup mapping")

verify = cfg.BoolOpt(
    'verify',
    default=False,
    help='Verify SSL connection')

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    url,
    resolver_url,
    verify
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
