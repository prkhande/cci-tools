from oslo_config import cfg


endpoint = cfg.StrOpt(
    'endpoint',
    default='https://cornerstone.cern.ch/api/wsdl',
    help="cornerstone endpoint url")

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    endpoint
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
