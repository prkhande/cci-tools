from oslo_config import cfg


host = cfg.StrOpt(
    'host',
    default='s3.cern.ch',
    help="Host to use for s3")

access_key = cfg.StrOpt(
    'access_key',
    default='access_key',
    help="access key for s3")

secret_key = cfg.StrOpt(
    'secret_key',
    default='secret_key',
    help='secret key for s3'
)

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    host,
    access_key,
    secret_key,
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
