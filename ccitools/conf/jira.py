from oslo_config import cfg


username = cfg.StrOpt(
    'username',
    default='username',
    help="username to connect to JIRA")

password = cfg.StrOpt(
    'password',
    default='password',
    help="password to connect to JIRA")

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    username,
    password
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
