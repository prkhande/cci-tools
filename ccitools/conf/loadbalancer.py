from oslo_config import cfg


listeners_per_lb = cfg.IntOpt(
    'listeners_per_lb',
    default=5,
    help="Number of listeners per load balancer")


members_per_lb = cfg.IntOpt(
    'members_per_lb',
    default=25,
    help="Number of members per load balancer")


GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    listeners_per_lb,
    members_per_lb
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
