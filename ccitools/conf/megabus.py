from oslo_config import cfg


server = cfg.StrOpt(
    'server',
    default='tcp://agileinf-mb.cern.ch:61213',
    help="megabus server")

user = cfg.StrOpt(
    'user',
    default='fake_user',
    help="megabus user")

passw = cfg.StrOpt(
    'passw',
    default='fake_pass',
    help="megabus user")

ttl = cfg.IntOpt(
    'ttl',
    default=172800,
    help="ttl for megabus")

queue = cfg.StrOpt(
    'queue',
    default='/topic/ai.hypervisor.drain',
    help="megabus queue")

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    server,
    user,
    passw,
    ttl,
    queue,
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
