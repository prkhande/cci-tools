from oslo_config import cfg


host = cfg.StrOpt(
    'host',
    default='constable.cern.ch',
    help="Host to use for PuppetDB")

port = cfg.StrOpt(
    'port',
    default='9081',
    help="Port to use for PuppetDB")

timeout = cfg.IntOpt(
    'timeout',
    default=15,
    help="Timeout to use for PuppetDB")

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    host,
    port,
    timeout,
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
