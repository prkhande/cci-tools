from oslo_config import cfg


alias = cfg.StrOpt(
    'alias',
    default='cci-rundeck.cern.ch',
    help="alias for rundeck")

api_token = cfg.StrOpt(
    'api_token',
    default='secret_token',
    help="secret token for rundeck")

connection = cfg.StrOpt(
    'connection',
    default='mysql+pymysql://user:pass@127.0.0.1:3306/rundeck',
    help="mysql connection url")

port = cfg.StrOpt(
    'port',
    default='443',
    help='port for rundeck')

verify = cfg.BoolOpt(
    'verify',
    default=False,
    help='verify SSL connection')

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    alias,
    api_token,
    connection,
    port,
    verify,
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
