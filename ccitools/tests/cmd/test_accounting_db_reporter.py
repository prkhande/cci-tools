import logging

from ccitools.cmd.accounting.dbreporter import main
from unittest import TestCase


class TestAccountingDBReporter(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self):
        with self.assertRaises(Exception):
            main([])
