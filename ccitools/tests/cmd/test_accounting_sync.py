import logging
import requests_mock

from ccitools.cmd.accounting.sync import main
from ccitools.tests.cmd import fixtures
from unittest import mock
from unittest import TestCase


class TestAccountingSync(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args_no_auth(self):
        with self.assertRaises(Exception):
            main([])

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.utils.cloud.keystone_client')
    @requests_mock.Mocker()
    def test_no_projects(self, mock_kc, mock_ks, mock_cc, mock_req):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value
        mock_req.get(
            'https://gar.cern.ch/public/user_resolver/list_all',
            text=fixtures.USER_CHARGEGROUPS_MAPPING)
        mock_req.get(
            'https://gar.cern.ch/public/list_full',
            text=fixtures.CHARGEGROUPS)
        mock_client.projects.list.return_value = []

        main([])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.list.assert_called_with(
            domain=None,
            parent=None,
            user=None,
            tags_any=['expiration'])

        mock_client.projects.update.assert_not_called()

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.utils.cloud.keystone_client')
    @requests_mock.Mocker()
    def test_projects(self, mock_kc, mock_ks, mock_cc, mock_req):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value
        mock_req.get(
            'https://gar.cern.ch/public/user_resolver/list_all',
            text=fixtures.USER_CHARGEGROUPS_MAPPING)
        mock_req.get(
            'https://gar.cern.ch/public/list_full',
            text=fixtures.CHARGEGROUPS)
        mock_client.projects.list.return_value = (
            fixtures.ACCOUNTING_PROJECTS
        )

        main([])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.list.assert_called_with(
            domain=None,
            parent=None,
            user=None,
            tags_any=['expiration'])

        mock_client.projects.update.assert_called_with(
            project=fixtures.ACCOUNTING_PROJECTS[0],
            chargegroup='4714e675-60b9-4fc7-80b2-61acd213f478'
        )
