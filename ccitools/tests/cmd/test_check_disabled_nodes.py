import logging

from datetime import datetime
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.utils.cloud.cloud_config')
@mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
@mock.patch('ccitools.cmd.base.foreman.ForemanClient')
@mock.patch('ccitools.utils.sendmail.MailClient')
class TestCheckDisabledNodesCMD(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)
        self.aitools_mock = mock.MagicMock()
        modules = {
            'aitools': self.aitools_mock,
            'aitools.foreman': self.aitools_mock.foreman,
        }
        self.patcher = mock.patch.dict('sys.modules', modules)
        self.patcher.start()
        self.addCleanup(self.patcher.stop)

    def test_default_args(self, mock_mc, mock_fc, mock_cl, mock_cc):
        from ccitools.cmd.check_disabled_nodes import main

        with self.assertRaises(SystemExit) as cm:
            main([])
        self.assertEqual(cm.exception.code, 2)

    def test_proper_data(self, mock_mc, mock_fc, mock_cl, mock_cc):
        from ccitools.cmd.check_disabled_nodes import main
        mock_cl = mock_cl.return_value
        mock_fc = mock_fc.return_value
        mock_cl.get_nova_aggregates.return_value = (
            mock.MagicMock(name='fake_n')
        )
        mock_cl.get_nova_services_list.return_value = (
            [
                mock.MagicMock(
                    status='disabled',
                    host='fake_h',
                    updated_at=datetime(2019, 5, 25, 12, 25),
                    state='down'
                )
            ]
        )
        mock_fc.gethost.return_value = (
            mock.MagicMock(operatingsystem_name='fake_name')
        )

        main([
            '--mailto', 'fakemail',
        ])

    def test_state_up(self, mock_mc, mock_fc, mock_cl, mock_cc):
        from ccitools.cmd.check_disabled_nodes import main
        mock_cl = mock_cl.return_value
        mock_fc = mock_fc.return_value
        mock_cl.get_nova_aggregates.return_value = (
            mock.MagicMock(name='fake_n')
        )
        mock_cl.get_nova_services_list.return_value = (
            [
                mock.MagicMock(
                    status='disabled',
                    host='fake_h',
                    updated_at=datetime(2019, 5, 25, 12, 25),
                    state='up'
                )
            ]
        )
        mock_fc.gethost.return_value = (
            mock.MagicMock(operatingsystem_name='fake_name')
        )

        main([
            '--mailto', 'fakemail',
        ])

    def test_no_state(self, mock_mc, mock_fc, mock_cl, mock_cc):
        from ccitools.cmd.check_disabled_nodes import main
        mock_cl = mock_cl.return_value
        mock_fc = mock_fc.return_value
        mock_cl.get_nova_aggregates.return_value = (
            mock.MagicMock(name='fake_n')
        )
        mock_cl.get_nova_services_list.return_value = (
            [
                mock.MagicMock(
                    status='disabled',
                    host='fake_h',
                    updated_at=datetime(2019, 5, 25, 12, 25),
                    state='fake'
                )
            ]
        )
        mock_fc.gethost.return_value = (
            mock.MagicMock(operatingsystem_name='fake_name')
        )

        main([
            '--mailto', 'fakemail',
        ])

    def test_exception_error(self, mock_mc, mock_fc, mock_cl, mock_cc):
        from ccitools.cmd.check_disabled_nodes import main
        mock_cl = mock_cl.return_value
        mock_fc = mock_fc.return_value
        mock_cl.get_nova_aggregates.return_value = (
            mock.MagicMock(name='fake_n')
        )
        mock_fc.gethost.return_value = None
        main([
            '--mailto', 'fakemail',
        ])

    def test_no_status(self, mock_mc, mock_fc, mock_cl, mock_cc):
        from ccitools.cmd.check_disabled_nodes import main
        mock_cl = mock_cl.return_value
        mock_fc = mock_fc.return_value
        mock_cl.get_nova_aggregates.return_value = (
            mock.MagicMock(name='fake_n')
        )
        mock_cl.get_nova_services_list.return_value = (
            [
                mock.MagicMock(
                    status='fake',
                    host='fake_h',
                    updated_at=datetime(2019, 5, 25, 12, 25),
                    state='fake'
                )
            ]
        )
        mock_fc.gethost.return_value = (
            mock.MagicMock(operatingsystem_name='fake_name')
        )

        main([
            '--mailto', 'fakemail',
        ])
