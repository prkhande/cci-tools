import logging

from ccitools.cmd.dump_record_producer import main
from ccitools.tests.cmd import fixtures
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.cmd.dump_record_producer.ServiceNowClient')
class TestDumpRecordProducer(TestCase):

    def setUp(self):
        logging.disable(logging.INFO)

    @mock.patch('ccitools.utils.cloud.cloud_config')
    def test_default_args(self, mock_snc, mock_cc):
        with self.assertRaises(SystemExit) as cm:
            main([])
        self.assertEqual(cm.exception.code, 2)

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    def test_quota_update(self, mock_crc, mock_snc):
        mock_snow = mock_snc.return_value

        mock_snow.get_quota_update_rp.return_value = (
            fixtures.SNOW_UPDATE_RP
        )
        mock_snow.user.get_user_info_by_sys_id.return_value.name = (
            'test_caller'
        )

        main([
            'quota-update',
            '--ticket-number', 'fake_ticket',
            '--instance', 'fake_cern',
        ])

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    def test_project_deletion(self, mock_crc, mock_snc):
        mock_snow = mock_snc.return_value

        mock_snow.get_project_deletion_rp.return_value = (
            fixtures.SNOW_DELETE_RP
        )
        mock_snow.user.get_user_info_by_sys_id.return_value.name = (
            'test_caller'
        )

        main([
            'project-deletion',
            '--ticket-number', 'fake_ticket',
            '--instance', 'fake_cern',
        ])

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    def test_project_request(self, mock_crc, mock_snc):
        mock_snow = mock_snc.return_value

        mock_snow.get_project_creation_rp.return_value = (
            fixtures.SNOW_CREATE_RP
        )
        mock_snow.user.get_user_info_by_sys_id.return_value.name = (
            'test_caller'
        )

        main([
            'project-request',
            '--ticket-number', 'fake_ticket',
            '--instance', 'fake_cern',
        ])
