import logging

from ccitools.cmd.enroll_physical_nodes import main
from unittest import TestCase


class TestEnrollPhysicalNodes(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self):
        main([])
