import logging

from ccitools.cmd.rally_cleanup import MAGNUM_PROJECT_ID
from ccitools.cmd.rally_cleanup import MAGNUM_T_PROJECT_ID
from ccitools.cmd.rally_cleanup import main
from ccitools.tests.cmd import fixtures
from unittest import mock
from unittest import TestCase

MAGNUM_PROJECT_CALLS = [mock.call(MAGNUM_PROJECT_ID),
                        mock.call(MAGNUM_T_PROJECT_ID)]


@mock.patch('ccitools.utils.cloud.keystone_session')
@mock.patch('ccitools.utils.cloud.cloud_config')
@mock.patch('ccitools.utils.cloud.CloudRegionClient.get_clusters')
@mock.patch('ccitools.utils.cloud.CloudRegionClient.get_cluster_templates')
@mock.patch('ccitools.utils.cloud.CloudRegionClient.delete_clusters')
@mock.patch('ccitools.utils.cloud.CloudRegionClient.delete_cluster_templates')
@mock.patch('ccitools.utils.cloud.CloudRegionClient.get_servers')
@mock.patch('ccitools.utils.cloud.CloudRegionClient.get_volumes')
@mock.patch('ccitools.utils.cloud.CloudRegionClient.delete_servers')
@mock.patch('ccitools.utils.cloud.CloudRegionClient.delete_volumes')
@mock.patch('ccitools.cmd.rally_cleanup.ServiceNowClient')
class TestRallyCleanup(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self,
                          mock_snow,
                          mock_del_vol,
                          mock_del_ser,
                          mock_get_vol,
                          mock_get_serv,
                          mock_del_tmpl,
                          mock_del_cluster,
                          mock_get_tmpl,
                          mock_get_cluster,
                          mock_cc,
                          mock_ks):
        mock_get_serv.return_value = []
        mock_get_vol.return_value = []
        mock_get_cluster.return_value = []
        mock_get_tmpl.return_value = []

        main(['--timeout', '1'])

        mock_snow.assert_called_with(instance='cern')

        mock_get_serv.assert_called_with(user_name='svcprobe')
        mock_get_vol.assert_called_with(user_name='svcprobe')
        mock_get_cluster.assert_has_calls(MAGNUM_PROJECT_CALLS)
        mock_get_tmpl.assert_has_calls(MAGNUM_PROJECT_CALLS)

        mock_get_serv.assert_called_with(user_name='svcprobe')

    def test_default_args_with_output(self,
                                      mock_snow,
                                      mock_del_vol,
                                      mock_del_ser,
                                      mock_get_vol,
                                      mock_get_serv,
                                      mock_del_tmpl,
                                      mock_del_cluster,
                                      mock_get_tmpl,
                                      mock_get_cluster,
                                      mock_cc,
                                      mock_ks):
        mock_get_serv.side_effect = iter([
            [
                fixtures.SERVER,
                fixtures.BROKEN_SERVER,
                fixtures.BROKEN_SERVER_CREATING,
                fixtures.BROKEN_SERVER_DELETING
            ],
            []
        ])
        mock_get_vol.side_effect = iter([
            [
                fixtures.VOLUME,
                fixtures.BROKEN_VOLUME,
                fixtures.BROKEN_VOLUME_CREATING,
                fixtures.BROKEN_VOLUME_DELETING
            ],
            []
        ])
        mock_get_cluster.return_value = []
        mock_get_tmpl.return_value = []

        main(['--timeout', '1'])

        mock_snow.assert_called_with(instance='cern')

    def test_default_args_with_output_broke_server(self,
                                                   mock_snow,
                                                   mock_del_vol,
                                                   mock_del_ser,
                                                   mock_get_vol,
                                                   mock_get_serv,
                                                   mock_del_tmpl,
                                                   mock_del_cluster,
                                                   mock_get_tmpl,
                                                   mock_get_cluster,
                                                   mock_cc,
                                                   mock_ks):
        mock_get_serv.side_effect = iter([
            [
                fixtures.SERVER,
                fixtures.BROKEN_SERVER,
                fixtures.BROKEN_SERVER_CREATING,
                fixtures.BROKEN_SERVER_DELETING
            ],
            [
                fixtures.SERVER,
                fixtures.BROKEN_SERVER,
                fixtures.BROKEN_SERVER_CREATING,
                fixtures.BROKEN_SERVER_DELETING
            ],
            []
        ])
        mock_get_vol.return_value = []
        mock_get_cluster.return_value = []
        mock_get_tmpl.return_value = []

        with self.assertRaises(SystemExit) as context:
            main(['--timeout', '1'])
        self.assertEqual(context.exception.code, 1)

        mock_snow.assert_called_with(instance='cern')

    def test_default_args_with_output_broke_volume(self,
                                                   mock_snow,
                                                   mock_del_vol,
                                                   mock_del_ser,
                                                   mock_get_vol,
                                                   mock_get_serv,
                                                   mock_del_tmpl,
                                                   mock_del_cluster,
                                                   mock_get_tmpl,
                                                   mock_get_cluster,
                                                   mock_cc,
                                                   mock_ks):
        mock_get_serv.return_value = []
        mock_get_vol.side_effect = iter([
            [
                fixtures.VOLUME,
                fixtures.BROKEN_VOLUME,
                fixtures.BROKEN_VOLUME_CREATING,
                fixtures.BROKEN_VOLUME_DELETING
            ],
            [
                fixtures.VOLUME,
                fixtures.BROKEN_VOLUME,
                fixtures.BROKEN_VOLUME_CREATING,
                fixtures.BROKEN_VOLUME_DELETING
            ],
            []
        ])
        mock_get_cluster.return_value = []
        mock_get_tmpl.return_value = []

        with self.assertRaises(SystemExit) as context:
            main(['--timeout', '1'])
        self.assertEqual(context.exception.code, 1)

        mock_snow.assert_called_with(instance='cern')

    def test_no_clusters_or_templates(self,
                                      mock_snow,
                                      mock_del_vol,
                                      mock_del_ser,
                                      mock_get_vol,
                                      mock_get_serv,
                                      mock_del_tmpl,
                                      mock_del_cluster,
                                      mock_get_tmpl,
                                      mock_get_cluster,
                                      mock_cc,
                                      mock_ks):
        mock_get_serv.return_value = []
        mock_get_vol.return_value = []
        mock_get_cluster.return_value = []
        mock_get_tmpl.return_value = []

        main(['--timeout', '1'])

        mock_get_cluster.assert_has_calls(MAGNUM_PROJECT_CALLS)
        mock_get_tmpl.assert_has_calls(MAGNUM_PROJECT_CALLS)

    def test_leftover_cluster(self,
                              mock_snow,
                              mock_del_vol,
                              mock_del_ser,
                              mock_get_vol,
                              mock_get_serv,
                              mock_del_tmpl,
                              mock_del_cluster,
                              mock_get_tmpl,
                              mock_get_cluster,
                              mock_cc,
                              mock_ks):
        mock_get_serv.return_value = []
        mock_get_vol.return_value = []

        # The call order is
        # 1. Get clusters, magnum project
        # 2. Get clusters, magnum_t project
        # 3. Check stuck clusters, magnum project
        # 4. Check stuck clusters, magnum_t project
        mock_get_cluster.side_effect = iter([
            [fixtures.MAGNUM_CLUSTER],
            [],
            [],
            [],
        ])
        mock_get_tmpl.return_value = []

        main(['--timeout', '1'])

        mock_get_cluster.assert_has_calls(MAGNUM_PROJECT_CALLS)
        mock_get_tmpl.assert_has_calls(MAGNUM_PROJECT_CALLS)

        mock_del_cluster.assert_called_with([fixtures.MAGNUM_CLUSTER])

    def test_leftover_cluster_fails(self,
                                    mock_snow,
                                    mock_del_vol,
                                    mock_del_ser,
                                    mock_get_vol,
                                    mock_get_serv,
                                    mock_del_tmpl,
                                    mock_del_cluster,
                                    mock_get_tmpl,
                                    mock_get_cluster,
                                    mock_cc,
                                    mock_ks):
        mock_get_serv.return_value = []
        mock_get_vol.return_value = []
        mock_get_cluster.side_effect = iter([
            [fixtures.MAGNUM_CLUSTER],
            [],
            [fixtures.MAGNUM_CLUSTER_DELETE_FAILED],
            [],
        ])
        mock_get_tmpl.return_value = []

        with self.assertRaises(SystemExit) as context:
            main(['--timeout', '1'])
        self.assertEqual(context.exception.code, 1)

        mock_get_cluster.assert_has_calls(MAGNUM_PROJECT_CALLS)
        mock_get_tmpl.assert_has_calls(MAGNUM_PROJECT_CALLS)

        mock_del_cluster.assert_called_with([fixtures.MAGNUM_CLUSTER])
        mock_snow.assert_called()

    def test_leftover_template(self,
                               mock_snow,
                               mock_del_vol,
                               mock_del_ser,
                               mock_get_vol,
                               mock_get_serv,
                               mock_del_tmpl,
                               mock_del_cluster,
                               mock_get_tmpl,
                               mock_get_cluster,
                               mock_cc,
                               mock_ks):
        mock_get_serv.return_value = []
        mock_get_vol.return_value = []
        mock_get_cluster.return_value = []
        mock_get_tmpl.side_effect = iter([
            [fixtures.MAGNUM_CLUSTER_TEMPLATE],
            [],
            [],
            [],
        ])

        main(['--timeout', '1'])

        mock_get_cluster.assert_has_calls(MAGNUM_PROJECT_CALLS)
        mock_get_tmpl.assert_has_calls(MAGNUM_PROJECT_CALLS)

        mock_del_tmpl.assert_called_with([fixtures.MAGNUM_CLUSTER_TEMPLATE])

    def test_leftover_template_fails(self,
                                     mock_snow,
                                     mock_del_vol,
                                     mock_del_ser,
                                     mock_get_vol,
                                     mock_get_serv,
                                     mock_del_tmpl,
                                     mock_del_cluster,
                                     mock_get_tmpl,
                                     mock_get_cluster,
                                     mock_cc,
                                     mock_ks):
        mock_get_serv.return_value = []
        mock_get_vol.return_value = []
        mock_get_cluster.return_value = []
        mock_get_tmpl.side_effect = iter([
            [fixtures.MAGNUM_CLUSTER_TEMPLATE],
            [],
            [fixtures.MAGNUM_CLUSTER_TEMPLATE],
            [],
        ])

        with self.assertRaises(SystemExit) as context:
            main(['--timeout', '1'])
        self.assertEqual(context.exception.code, 1)

        mock_get_cluster.assert_has_calls(MAGNUM_PROJECT_CALLS)
        mock_get_tmpl.assert_has_calls(MAGNUM_PROJECT_CALLS)

        mock_del_tmpl.assert_called_with([fixtures.MAGNUM_CLUSTER_TEMPLATE])
        mock_snow.assert_called()
