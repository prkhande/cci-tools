import io
import logging

from ccitools.tests.cmd import fixtures
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.utils.aihelper.TrustedBagClient')
class TestRotateKeys(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)
        self.aitools_mock = mock.MagicMock()
        modules = {
            'aitools': self.aitools_mock,
            'aitools.trustedbag': self.aitools_mock.trustedbag,
        }
        self.patcher = mock.patch.dict('sys.modules', modules)
        self.patcher.start()
        self.addCleanup(self.patcher.stop)

    def test_main_no_args(self, mock_tb):
        from ccitools.cmd.rotate_fernet_keys import main

        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 2)

    @mock.patch('builtins.open',
                return_value=io.StringIO(fixtures.TBAG_AI_CONF))
    def test_main_default_args(self, mock_open, mock_tb):
        from ccitools.cmd.rotate_fernet_keys import main

        main([
            '--hostgroup', 'fake_jhost_group',
            '--teigi-tag', 'fernet_'
        ])

    @mock.patch('builtins.open',
                return_value=io.StringIO(fixtures.TBAG_AI_CONF))
    def test_main_get_exception(self, mock_open, mock_tb):
        from ccitools.cmd.rotate_fernet_keys import main
        from ccitools.utils import aihelper

        mock_tb.return_value.get_key.side_effect = iter([Exception('fail')])
        aihelper.TEIGI_TIMEOUT = 1
        aihelper.TEIGI_RETRY_INTERVAL = 1

        with self.assertRaises(Exception) as cm:
            main([
                '--hostgroup', 'fake_host_group',
                '--teigi-tag', 'fernet_'
            ])
        self.assertEqual(
            str(cm.exception),
            "Impossible to get 'fernet__0' from TEIGI after 1 seconds waiting")

    @mock.patch('builtins.open',
                return_value=io.StringIO(fixtures.TBAG_AI_CONF))
    def test_main_add_exception(self, mock_open, mock_tb):
        from ccitools.cmd.rotate_fernet_keys import main
        from ccitools.utils import aihelper

        mock_tb.return_value.add_key.side_effect = iter([Exception('fail')])
        aihelper.TEIGI_TIMEOUT = 1
        aihelper.TEIGI_RETRY_INTERVAL = 1

        with self.assertRaises(Exception) as cm:
            main([
                '--hostgroup', 'fake_host_group',
                '--teigi-tag', 'fernet_'
            ])
        self.assertEqual(
            str(cm.exception),
            "Impossible to UPDATE 'fernet__0' TEIGI after 1 seconds waiting")
