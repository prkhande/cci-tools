import logging

from ccitools.cmd import s3_quota
from ccitools.tests.cmd import fixtures
from keystoneclient.exceptions import NotFound
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.utils.cloud.cloud_config')
@mock.patch('ccitools.utils.cloud.keystone_session')
@mock.patch('ccitools.utils.cloud.keystone_client')
class TestS3Quota(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self, mock_kc, mock_ks, mock_cc):
        with self.assertRaises(SystemExit) as cm:
            s3_quota.main([])
        self.assertEqual(cm.exception.code, 2)

    def test_show_all_no_projects(self, mock_kc, mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value

        mock_client.projects.list.return_value = []

        with self.assertRaises(SystemExit) as cm:
            s3_quota.main(['show', '--all'])
        self.assertEqual(cm.exception.code, 1)

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.list.assert_called_with(
            domain='default',
            parent=None,
            user=None,
            tags_any='s3quota'
        )

    @mock.patch('ccitools.utils.cloud.CloudRegionClient.get_s3_project_usage')
    @mock.patch('ccitools.utils.cloud.CloudRegionClient.get_s3_project_quota')
    def test_show_all(self, mock_quota, mock_usage, mock_kc, mock_ks,
                      mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value

        mock_client.projects.list.return_value = [fixtures.PROJECT_OUTPUT]
        mock_usage.return_value = (1, 5)
        mock_quota.return_value = (10, 20)

        s3_quota.main(['show', '--all'])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.list.assert_called_with(
            domain='default',
            parent=None,
            user=None,
            tags_any='s3quota'
        )
        mock_usage.assert_called_with(
            project_id='1',
            region='cern'
        )
        mock_quota.assert_called_with(
            project_id='1',
            region='cern'
        )

    @mock.patch('ccitools.utils.cloud.CloudRegionClient.get_s3_project_usage')
    @mock.patch('ccitools.utils.cloud.CloudRegionClient.get_s3_project_quota')
    def test_show_all_only_id(self, mock_quota, mock_usage, mock_kc, mock_ks,
                              mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value

        mock_client.projects.list.return_value = [fixtures.PROJECT_OUTPUT]
        mock_usage.return_value = (1, 5)
        mock_quota.return_value = (10, 20)

        s3_quota.main(['show', '--all', '-c', 'ID'])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.list.assert_called_with(
            domain='default',
            parent=None,
            user=None,
            tags_any='s3quota'
        )
        mock_usage.assert_called_with(
            project_id='1',
            region='cern'
        )
        mock_quota.assert_called_with(
            project_id='1',
            region='cern'
        )

    @mock.patch('ccitools.utils.cloud.CloudRegionClient.get_s3_project_usage')
    @mock.patch('ccitools.utils.cloud.CloudRegionClient.get_s3_project_quota')
    def test_show_all_wrong_columns(self, mock_quota, mock_usage, mock_kc,
                                    mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value

        mock_client.projects.list.return_value = [fixtures.PROJECT_OUTPUT]
        mock_usage.return_value = (1, 5)
        mock_quota.return_value = (10, 20)

        with self.assertRaises(ValueError):
            s3_quota.main(['show', '--all', '-c', 'fake'])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.list.assert_called_with(
            domain='default',
            parent=None,
            user=None,
            tags_any='s3quota'
        )
        mock_usage.assert_called_with(
            project_id='1',
            region='cern'
        )
        mock_quota.assert_called_with(
            project_id='1',
            region='cern'
        )

    @mock.patch('ccitools.utils.cloud.CloudRegionClient.get_s3_project_usage')
    @mock.patch('ccitools.utils.cloud.CloudRegionClient.get_s3_project_quota')
    def test_show_all_yaml(self, mock_quota, mock_usage, mock_kc, mock_ks,
                           mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value

        mock_client.projects.list.return_value = [fixtures.PROJECT_OUTPUT]
        mock_usage.return_value = (1, 5)
        mock_quota.return_value = (10, 20)

        # Change manually the formater_default
        s3_quota._formatter_default = 'fake'

        s3_quota.main(['show', '--all', '-f', 'yaml'])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.list.assert_called_with(
            domain='default',
            parent=None,
            user=None,
            tags_any='s3quota'
        )
        mock_usage.assert_called_with(
            project_id='1',
            region='cern'
        )
        mock_quota.assert_called_with(
            project_id='1',
            region='cern'
        )

    def test_show_project_raise_exception(self, mock_kc, mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value

        mock_client.projects.find.side_effect = iter([NotFound])
        mock_client.projects.get.side_effect = iter([NotFound])

        with self.assertRaises(SystemExit) as cm:
            s3_quota.main(['show', '--project', '1'])
        self.assertEqual(cm.exception.code, 1)

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.find.assert_called_with(
            name='1'
        )
        mock_client.projects.get.assert_called_with(
            '1'
        )

    @mock.patch('ccitools.utils.cloud.CloudRegionClient.get_s3_project_usage')
    @mock.patch('ccitools.utils.cloud.CloudRegionClient.get_s3_project_quota')
    def test_show_project(self, mock_quota, mock_usage, mock_kc, mock_ks,
                          mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value

        mock_client.projects.find.side_effect = iter([NotFound])
        mock_client.projects.get.return_value = fixtures.PROJECT_OUTPUT
        mock_usage.return_value = (1, 5)
        mock_quota.return_value = (10, 20)

        s3_quota.main(['show', '--project', '1'])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)

        mock_client.projects.find.assert_called_with(
            name='1'
        )
        mock_client.projects.get.assert_called_with(
            '1'
        )
        mock_usage.assert_called_with(
            project_id='1',
            region='cern'
        )
        mock_quota.assert_called_with(
            project_id='1',
            region='cern'
        )

    def test_update_project_raise_exception(self, mock_kc, mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value

        mock_client.projects.find.side_effect = iter([NotFound])
        mock_client.projects.get.side_effect = iter([NotFound])

        with self.assertRaises(SystemExit) as cm:
            s3_quota.main([
                'update',
                '--project', '1',
            ])
        self.assertEqual(cm.exception.code, 1)

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.find.assert_called_with(
            name='1'
        )
        mock_client.projects.get.assert_called_with(
            '1'
        )

    @mock.patch('ccitools.utils.cloud.CloudRegionClient.set_s3_project_quota')
    @mock.patch('ccitools.utils.cloud.CloudRegionClient.get_s3_project_quota')
    def test_update_project_novalues(self, mock_get, mock_set, mock_kc,
                                     mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value

        mock_client.projects.find.side_effect = iter([NotFound])
        mock_client.projects.get.return_value = fixtures.PROJECT_OUTPUT
        mock_get.return_value = (10, 20)

        s3_quota.main([
            'update',
            '--project', '1',
        ])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.find.assert_called_with(
            name='1'
        )
        mock_client.projects.get.assert_called_with(
            '1'
        )
        mock_get.assert_called_with(
            project_id='1',
            region='cern'
        )
        mock_set.assert_called_with(
            project_id='1',
            containers=10,
            size_gb=20,
            region='cern'
        )

    @mock.patch('ccitools.utils.cloud.CloudRegionClient.set_s3_project_quota')
    @mock.patch('ccitools.utils.cloud.CloudRegionClient.get_s3_project_quota')
    def test_update_project(self, mock_get, mock_set, mock_kc, mock_ks,
                            mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value

        mock_client.projects.find.side_effect = iter([NotFound])
        mock_client.projects.get.return_value = fixtures.PROJECT_OUTPUT
        mock_get.return_value = (10, 20)

        s3_quota.main([
            'update',
            '--project', '1',
            '--containers', '110',
            '--size', '120'
        ])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.find.assert_called_with(
            name='1'
        )
        mock_client.projects.get.assert_called_with(
            '1'
        )
        mock_get.assert_called_with(
            project_id='1',
            region='cern'
        )
        mock_set.assert_called_with(
            project_id='1',
            containers=110,
            size_gb=120,
            region='cern'
        )
