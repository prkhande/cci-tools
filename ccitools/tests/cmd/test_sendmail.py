import logging

from ccitools.cmd.sendmail import main
from unittest import mock
from unittest import TestCase


class TestSendmail(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 2)

    @mock.patch('ccitools.utils.sendmail.smtplib')
    def test_no_send_to_addr(self, mock_smtp):
        main([
            '--mailto', 'user@cern.ch'
        ])

    @mock.patch('ccitools.utils.sendmail.smtplib')
    def test_send_to_addr(self, mock_smtp):

        main([
            '--mailto', 'user@cern.ch',
            '--sendmail'
        ])

        mock_smtp.SMTP.assert_called_with('localhost')
