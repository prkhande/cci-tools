import logging

from ccitools.cmd import update_fim_project
from ccitools.tests.cmd import fixtures
from unittest import mock
from unittest import TestCase


class TestUpdateFIMProject(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.update_fim_project.FIMClient')
    def test_default_args(self,
                          mock_fc,
                          mock_crc):

        mock_cl = mock_crc.return_value
        update_fim_project.main([])

        mock_cl.find_project.assert_not_called()
        mock_cl.get_projects_by_role.assert_not_called()

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.update_fim_project.FIMClient')
    def test_search_by_name_and_owner_raises_exception(self,
                                                       mock_fc,
                                                       mock_crc):
        mock_cl = mock_crc.return_value
        mock_cl.find_project.side_effect = Exception()

        with self.assertRaises(SystemExit) as cm:
            update_fim_project.main([
                '--project-name', 'project-fake',
                '--owner', 'fake_owner'
            ])
        self.assertEqual(cm.exception.code, 2)

        mock_cl.find_project.assert_not_called()
        mock_cl.get_projects_by_role.assert_not_called()

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.update_fim_project.FIMClient')
    def test_search_by_name_no_project(self,
                                       mock_fc,
                                       mock_crc):
        mock_cl = mock_crc.return_value
        mock_cl.find_project.side_effect = Exception()

        with self.assertRaises(SystemExit) as cm:
            update_fim_project.main([
                '--project-name', 'project-fake',
            ])
        self.assertEqual(cm.exception.code, 1)

        mock_cl.find_project.assert_called_with(
            'project-fake'
        )
        mock_cl.get_projects_by_role.assert_not_called()

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.update_fim_project.FIMClient')
    def test_search_by_name_no_exclude(self,
                                       mock_fc,
                                       mock_crc):
        mock_cl = mock_crc.return_value
        mock_cl.find_project.return_value = fixtures.PROJECT_OUTPUT

        update_fim_project.main([
            '--project-name', 'project-fake',
            '--exclude-type', 'service'
        ])

        mock_cl.find_project.assert_called_with(
            'project-fake'
        )
        mock_cl.get_projects_by_role.assert_not_called()

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.update_fim_project.FIMClient')
    def test_search_by_name_no_changes(self,
                                       mock_fc,
                                       mock_crc):
        mock_cl = mock_crc.return_value
        mock_cl.find_project.return_value = fixtures.PROJECT_OUTPUT

        update_fim_project.main([
            '--project-name', 'project-fake',
        ])

        mock_cl.find_project.assert_called_with(
            'project-fake'
        )
        mock_cl.get_projects_by_role.assert_not_called()

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.update_fim_project.FIMClient')
    def test_search_by_name_dryrun(self,
                                   mock_fc,
                                   mock_crc):
        mock_cl = mock_crc.return_value
        mock_cl.find_project.return_value = fixtures.PROJECT_OUTPUT

        update_fim_project.main([
            '--project-name', 'project-fake',
            '--set-name', 'new_name',
            '--set-description', 'new_description',
            '--set-owner', 'new_owner',
            '--set-enabled'
        ])

        mock_cl.find_project.assert_called_with(
            'project-fake'
        )
        mock_cl.get_projects_by_role.assert_not_called()

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.update_fim_project.FIMClient')
    def test_search_by_name_execute(self,
                                    mock_fc,
                                    mock_crc):
        mock_cl = mock_crc.return_value
        mock_cl.find_project.return_value = fixtures.PROJECT_OUTPUT
        mock_fim = mock_fc.return_value
        mock_fim.set_project_name.return_value = 0
        mock_fim.set_project_description.return_value = 0
        mock_fim.set_project_owner.return_value = 0
        mock_fim.set_project_status.return_value = 0

        update_fim_project.main([
            '--project-name', 'project-fake',
            '--set-name', 'new_name',
            '--set-description', 'new_description',
            '--set-owner', 'new_owner',
            '--set-enabled',
            '--execute'
        ])

        mock_cl.find_project.assert_called_with(
            'project-fake'
        )
        mock_cl.get_projects_by_role.assert_not_called()
        mock_fim.set_project_name.assert_called_with(
            'project-fake',
            'new_name'
        )
        mock_fim.set_project_description.assert_called_with(
            'project-fake',
            'new_description'
        )
        mock_fim.set_project_owner.assert_called_with(
            'project-fake',
            'new_owner'
        )
        mock_fim.set_project_status.assert_called_with(
            'project-fake',
            True
        )

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.update_fim_project.FIMClient')
    def test_search_by_name_fail_update_name(self,
                                             mock_fc,
                                             mock_crc):
        mock_cl = mock_crc.return_value
        mock_cl.find_project.return_value = fixtures.PROJECT_OUTPUT
        mock_fim = mock_fc.return_value
        mock_fim.set_project_name.return_value = -1

        with self.assertRaises(SystemExit) as cm:
            update_fim_project.main([
                '--project-name', 'project-fake',
                '--set-name', 'new_name',
                '--set-description', 'new_description',
                '--set-owner', 'new_owner',
                '--set-enabled',
                '--execute'
            ])
        self.assertEqual(cm.exception.code, 1)

        mock_cl.find_project.assert_called_with(
            'project-fake'
        )
        mock_cl.get_projects_by_role.assert_not_called()
        mock_fim.set_project_name.assert_called_with(
            'project-fake',
            'new_name'
        )
        mock_fim.set_project_description.assert_not_called()
        mock_fim.set_project_owner.assert_not_called()
        mock_fim.set_project_status.assert_not_called()

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.update_fim_project.FIMClient')
    def test_search_by_name_fail_update_description(self,
                                                    mock_fc,
                                                    mock_crc):
        mock_cl = mock_crc.return_value
        mock_cl.find_project.return_value = fixtures.PROJECT_OUTPUT
        mock_fim = mock_fc.return_value
        mock_fim.set_project_name.return_value = 0
        mock_fim.set_project_description.return_value = -1

        with self.assertRaises(SystemExit) as cm:
            update_fim_project.main([
                '--project-name', 'project-fake',
                '--set-name', 'new_name',
                '--set-description', 'new_description',
                '--set-owner', 'new_owner',
                '--set-enabled',
                '--execute'
            ])
        self.assertEqual(cm.exception.code, 1)

        mock_cl.find_project.assert_called_with(
            'project-fake'
        )
        mock_cl.get_projects_by_role.assert_not_called()
        mock_fim.set_project_name.assert_called_with(
            'project-fake',
            'new_name'
        )
        mock_fim.set_project_description.assert_called_with(
            'project-fake',
            'new_description'
        )
        mock_fim.set_project_owner.assert_not_called()
        mock_fim.set_project_status.assert_not_called()

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.update_fim_project.FIMClient')
    def test_search_by_name_fail_update_owner(self,
                                              mock_fc,
                                              mock_crc):
        mock_cl = mock_crc.return_value
        mock_cl.find_project.return_value = fixtures.PROJECT_OUTPUT
        mock_fim = mock_fc.return_value
        mock_fim.set_project_name.return_value = 0
        mock_fim.set_project_description.return_value = 0
        mock_fim.set_project_owner.return_value = -1

        with self.assertRaises(SystemExit) as cm:
            update_fim_project.main([
                '--project-name', 'project-fake',
                '--set-name', 'new_name',
                '--set-description', 'new_description',
                '--set-owner', 'new_owner',
                '--set-enabled',
                '--execute'
            ])
        self.assertEqual(cm.exception.code, 1)

        mock_cl.find_project.assert_called_with(
            'project-fake'
        )
        mock_cl.get_projects_by_role.assert_not_called()
        mock_fim.set_project_name.assert_called_with(
            'project-fake',
            'new_name'
        )
        mock_fim.set_project_description.assert_called_with(
            'project-fake',
            'new_description'
        )
        mock_fim.set_project_owner.assert_called_with(
            'project-fake',
            'new_owner'
        )
        mock_fim.set_project_status.assert_not_called()

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.update_fim_project.FIMClient')
    def test_search_by_name_fail_update_status(self,
                                               mock_fc,
                                               mock_crc):
        mock_cl = mock_crc.return_value
        mock_cl.find_project.return_value = fixtures.PROJECT_OUTPUT
        mock_fim = mock_fc.return_value
        mock_fim.set_project_name.return_value = 0
        mock_fim.set_project_description.return_value = 0
        mock_fim.set_project_owner.return_value = 0
        mock_fim.set_project_status.return_value = -1

        with self.assertRaises(SystemExit) as cm:
            update_fim_project.main([
                '--project-name', 'project-fake',
                '--set-name', 'new_name',
                '--set-description', 'new_description',
                '--set-owner', 'new_owner',
                '--set-enabled',
                '--execute'
            ])
        self.assertEqual(cm.exception.code, 1)

        mock_cl.find_project.assert_called_with(
            'project-fake'
        )
        mock_cl.get_projects_by_role.assert_not_called()
        mock_fim.set_project_name.assert_called_with(
            'project-fake',
            'new_name'
        )
        mock_fim.set_project_description.assert_called_with(
            'project-fake',
            'new_description'
        )
        mock_fim.set_project_owner.assert_called_with(
            'project-fake',
            'new_owner'
        )
        mock_fim.set_project_status.assert_called_with(
            'project-fake',
            True
        )

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.update_fim_project.FIMClient')
    def test_search_by_owner_no_projects(self,
                                         mock_fc,
                                         mock_crc):
        mock_cl = mock_crc.return_value
        mock_cl.get_projects_by_role.return_value = []

        update_fim_project.main([
            '--owner', 'old_owner',
            '--set-owner', 'new_owner',
        ])

        mock_cl.find_project.assert_not_called()
        mock_cl.get_projects_by_role.assert_called_with(
            user='old_owner',
            rolename='owner',
        )

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.update_fim_project.FIMClient')
    def test_search_by_owner_exclude(self,
                                     mock_fc,
                                     mock_crc):
        mock_cl = mock_crc.return_value
        mock_cl.get_projects_by_role.return_value = []

        update_fim_project.main([
            '--owner', 'old_owner',
            '--set-owner', 'new_owner',
            '--exclude-id', '1'
        ])

        mock_cl.find_project.assert_not_called()
        mock_cl.get_projects_by_role.assert_called_with(
            user='old_owner',
            rolename='owner',
        )

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.update_fim_project.FIMClient')
    def test_search_by_owner_no_changes(self,
                                        mock_fc,
                                        mock_crc):
        mock_cl = mock_crc.return_value
        mock_cl.get_projects_by_role.return_value = [fixtures.PROJECT_OUTPUT]

        update_fim_project.main([
            '--owner', 'old_owner',
            '--set-owner', 'new_owner',
        ])

        mock_cl.find_project.assert_not_called()
        mock_cl.get_projects_by_role.assert_called_with(
            user='old_owner',
            rolename='owner',
        )

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.update_fim_project.FIMClient')
    def test_search_by_owner_dry_run(self,
                                     mock_fc,
                                     mock_crc):
        mock_cl = mock_crc.return_value
        mock_cl.get_projects_by_role.return_value = [fixtures.PROJECT_OUTPUT]

        update_fim_project.main([
            '--owner', 'old_owner',
            '--set-name', 'new_name',
            '--set-description', 'new_description',
            '--set-owner', 'new_owner',
            '--set-enabled'
        ])

        mock_cl.find_project.assert_not_called()
        mock_cl.get_projects_by_role.assert_called_with(
            user='old_owner',
            rolename='owner',
        )

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.update_fim_project.FIMClient')
    def test_search_by_owner_fail_update_name(self,
                                              mock_fc,
                                              mock_crc):
        mock_cl = mock_crc.return_value
        mock_cl.get_projects_by_role.return_value = [fixtures.PROJECT_OUTPUT]
        mock_fim = mock_fc.return_value
        mock_fim.set_project_name.return_value = -1

        with self.assertRaises(SystemExit) as cm:
            update_fim_project.main([
                '--owner', 'old_owner',
                '--set-name', 'new_name',
                '--set-description', 'new_description',
                '--set-owner', 'new_owner',
                '--set-enabled',
                '--execute'
            ])
        self.assertEqual(cm.exception.code, 1)

        mock_cl.find_project.assert_not_called()
        mock_cl.get_projects_by_role.assert_called_with(
            user='old_owner',
            rolename='owner',
        )
        mock_fim.set_project_name.assert_called_with(
            'project-fake',
            'new_name'
        )
        mock_fim.set_project_description.assert_not_called()
        mock_fim.set_project_owner.assert_not_called()
        mock_fim.set_project_status.assert_not_called()

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.update_fim_project.FIMClient')
    def test_search_by_owner_fail_update_description(self,
                                                     mock_fc,
                                                     mock_crc):
        mock_cl = mock_crc.return_value
        mock_cl.get_projects_by_role.return_value = [fixtures.PROJECT_OUTPUT]
        mock_fim = mock_fc.return_value
        mock_fim.set_project_name.return_value = 0
        mock_fim.set_project_description.return_value = -1

        with self.assertRaises(SystemExit) as cm:
            update_fim_project.main([
                '--owner', 'old_owner',
                '--set-name', 'new_name',
                '--set-description', 'new_description',
                '--set-owner', 'new_owner',
                '--set-enabled',
                '--execute'
            ])
        self.assertEqual(cm.exception.code, 1)

        mock_cl.find_project.assert_not_called()
        mock_cl.get_projects_by_role.assert_called_with(
            user='old_owner',
            rolename='owner',
        )
        mock_fim.set_project_name.assert_called_with(
            'project-fake',
            'new_name'
        )
        mock_fim.set_project_description.assert_called_with(
            'project-fake',
            'new_description'
        )
        mock_fim.set_project_owner.assert_not_called()
        mock_fim.set_project_status.assert_not_called()

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.update_fim_project.FIMClient')
    def test_search_by_owner_fail_update_owner(self,
                                               mock_fc,
                                               mock_crc):
        mock_cl = mock_crc.return_value
        mock_cl.get_projects_by_role.return_value = [fixtures.PROJECT_OUTPUT]
        mock_fim = mock_fc.return_value
        mock_fim.set_project_name.return_value = 0
        mock_fim.set_project_description.return_value = 0
        mock_fim.set_project_owner.return_value = -1

        with self.assertRaises(SystemExit) as cm:
            update_fim_project.main([
                '--owner', 'old_owner',
                '--set-name', 'new_name',
                '--set-description', 'new_description',
                '--set-owner', 'new_owner',
                '--set-enabled',
                '--execute'
            ])
        self.assertEqual(cm.exception.code, 1)

        mock_cl.find_project.assert_not_called()
        mock_cl.get_projects_by_role.assert_called_with(
            user='old_owner',
            rolename='owner',
        )
        mock_fim.set_project_name.assert_called_with(
            'project-fake',
            'new_name'
        )
        mock_fim.set_project_description.assert_called_with(
            'project-fake',
            'new_description'
        )
        mock_fim.set_project_owner.assert_called_with(
            'project-fake',
            'new_owner'
        )
        mock_fim.set_project_status.assert_not_called()

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.update_fim_project.FIMClient')
    def test_search_by_owner_fail_update_status(self,
                                                mock_fc,
                                                mock_crc):
        mock_cl = mock_crc.return_value
        mock_cl.get_projects_by_role.return_value = [fixtures.PROJECT_OUTPUT]
        mock_fim = mock_fc.return_value
        mock_fim.set_project_name.return_value = 0
        mock_fim.set_project_description.return_value = 0
        mock_fim.set_project_owner.return_value = 0
        mock_fim.set_project_status.return_value = -1

        with self.assertRaises(SystemExit) as cm:
            update_fim_project.main([
                '--owner', 'old_owner',
                '--set-name', 'new_name',
                '--set-description', 'new_description',
                '--set-owner', 'new_owner',
                '--set-enabled',
                '--execute'
            ])
        self.assertEqual(cm.exception.code, 1)

        mock_cl.find_project.assert_not_called()
        mock_cl.get_projects_by_role.assert_called_with(
            user='old_owner',
            rolename='owner',
        )
        mock_fim.set_project_name.assert_called_with(
            'project-fake',
            'new_name'
        )
        mock_fim.set_project_description.assert_called_with(
            'project-fake',
            'new_description'
        )
        mock_fim.set_project_owner.assert_called_with(
            'project-fake',
            'new_owner'
        )
        mock_fim.set_project_status.assert_called_with(
            'project-fake',
            True
        )

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.update_fim_project.FIMClient')
    def test_search_by_owner_execute(self,
                                     mock_fc,
                                     mock_crc):
        mock_cl = mock_crc.return_value
        mock_cl.get_projects_by_role.return_value = [fixtures.PROJECT_OUTPUT]
        mock_fim = mock_fc.return_value
        mock_fim.set_project_name.return_value = 0
        mock_fim.set_project_description.return_value = 0
        mock_fim.set_project_owner.return_value = 0
        mock_fim.set_project_status.return_value = 0

        update_fim_project.main([
            '--owner', 'old_owner',
            '--set-name', 'new_name',
            '--set-description', 'new_description',
            '--set-owner', 'new_owner',
            '--set-enabled',
            '--execute'
        ])

        mock_cl.find_project.assert_not_called()
        mock_cl.get_projects_by_role.assert_called_with(
            user='old_owner',
            rolename='owner',
        )
        mock_fim.set_project_name.assert_called_with(
            'project-fake',
            'new_name'
        )
        mock_fim.set_project_description.assert_called_with(
            'project-fake',
            'new_description'
        )
        mock_fim.set_project_owner.assert_called_with(
            'project-fake',
            'new_owner'
        )
        mock_fim.set_project_status.assert_called_with(
            'project-fake',
            True
        )
