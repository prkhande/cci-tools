import logging

from ccitools.cmd import wait_until_machine_is_sshable
from tenacity import stop_after_attempt
from tenacity import wait_none
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.cmd.wait_until_machine_is_sshable.ssh_executor')
@mock.patch('time.sleep')
class TestWaitUntilMachineIsSSHable(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self, mock_time, mock_exec):
        with self.assertRaises(SystemExit) as cm:
            wait_until_machine_is_sshable.main([])
        self.assertEqual(cm.exception.code, 2)

    def test_dryrun_hosts(self, mock_time, mock_exec):
        wait_until_machine_is_sshable.main([
            '--hosts', 'my_host1 my_host2',
            '--dryrun'
        ])

    def test_perform_hosts(self, mock_time, mock_exec):
        # Case with 2 nodes all accessible by ssh
        mock_exec.return_value = [], None
        wait_until_machine_is_sshable.main([
            '--hosts', 'my_host1 my_host2',
            '--perform'
        ])

        # Case with a single node that fails initially
        mock_exec.side_effect = iter([
            Exception('fail'),
            ([], None),
        ])
        cmd = wait_until_machine_is_sshable.WaitUtilMachineIsSSHable()
        cmd.wait_ssh.retry.wait = wait_none()
        cmd.main([
            '--hosts', 'my_host1',
            '--perform'
        ])

        # Case with a single node that fails always
        mock_exec.side_effect = iter([
            Exception('fail'),
        ])
        cmd = wait_until_machine_is_sshable.WaitUtilMachineIsSSHable()
        cmd.wait_ssh.retry.wait = wait_none()
        cmd.wait_ssh.retry.stop = stop_after_attempt(1)
        with self.assertRaises(SystemExit) as cm:
            cmd.main([
                '--hosts', 'my_host1',
                '--perform'
            ])
        self.assertEqual(cm.exception.code,
                         'Some hosts are not SSHable. Exiting...')
