class Resource(object):
    """Base class for resources.

    This is pretty much just a bag for attributes.
    """

    def __init__(self, info):
        """Populate and bind to a manager.

        :param info: dictionary representing resource attributes
        """
        self._info = info
        self._add_details(info)

    def __repr__(self):
        """Return object representation."""
        reprkeys = sorted(k
                          for k in self.__dict__.keys()
                          if k[0] != '_')
        info = ", ".join("%s=%s" % (k, getattr(self, k)) for k in reprkeys)
        return "<%s %s>" % (self.__class__.__name__, info)

    def _add_details(self, info):
        for (k, v) in info.items():
            try:
                setattr(self, k, v)
            except AttributeError:
                # In this case we already defined the attribute on the class
                continue
            self._info[k] = v

    def __getattr__(self, k):
        """Return object representation."""
        if k not in self.__dict__ or k not in self._info:
            raise AttributeError(k)
        else:
            if k in self.__dict__:
                return self.__dict__[k]
            return self._info[k]

    def __eq__(self, other):
        """Check if the objects are equal."""
        if not isinstance(other, Resource):
            return NotImplemented
        # two resources of different types are not equal
        if not isinstance(other, self.__class__):
            return False
        return self._info == other._info

    def __ne__(self, other):
        """Check if the objects are non equal."""
        return not self.__eq__(other)
