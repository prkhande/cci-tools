import argparse
import logging

from ccitools import conf
from ccitools.tests import fixtures as base_fixtures
from ccitools.tests.utils import fixtures
from ccitools.utils.cloud import CloudRegionClient
from keystoneclient import exceptions as keystone_exceptions
from manilaclient import api_versions as manila_api_versions
from novaclient import exceptions as nova_exceptions
from radosgw.exception import NoSuchUser
from unittest import mock
from unittest import TestCase

# patch mock module's internal data structures to support round():
mock._all_magics.add('__round__')
mock._magics.add('__round__')

CONF = conf.CONF


@mock.patch('ccitools.utils.cloud.keystone_session')
@mock.patch('ccitools.utils.cloud.cloud_config')
class TestCloud(TestCase):

    maxDiff = None

    def setUp(self):
        logging.disable(logging.WARNING)

    def test_get_regions_per_service_empty_catalog(self, mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.EMPTY_CATALOG['catalog']
        )

        client = CloudRegionClient()
        regions = client.get_regions_per_service('empty')

        mock_auth_ref.assert_called_with(session_mock)
        self.assertEqual([], regions)

        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.EMPTY_CATALOG
        )

        client = CloudRegionClient()
        regions = client.get_regions_per_service('empty')

        mock_auth_ref.assert_called_with(session_mock)
        self.assertEqual([], regions)

    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_get_regions_per_service_catalog(self, mock_kc, mock_cc,
                                             mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_kc.Client.return_value
        mock_cl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG['catalog']
        )

        client = CloudRegionClient()
        regions = client.get_regions_per_service('identity')
        mock_auth_ref.assert_called_with(session_mock)
        self.assertEqual(['cern'], regions)

        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )

        client = CloudRegionClient()
        regions = client.get_regions_per_service('identity')
        mock_auth_ref.assert_called_with(session_mock)
        self.assertEqual(['cern'], regions)

    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_is_region_available_in_region(self, mock_kc, mock_cc,
                                           mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_kc.Client.return_value
        mock_cl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )

        client = CloudRegionClient()
        self.assertEqual(
            client.is_service_available_in_region(
                service_type='identity',
                region='fake'),
            False)
        mock_auth_ref.assert_called_with(session_mock)

        self.assertEqual(
            client.is_service_available_in_region(
                service_type='identity',
                region='cern'),
            True)
        mock_auth_ref.assert_called_with(session_mock)

    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_is_production_region(self, mock_kc, mock_cc,
                                  mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_kc.Client.return_value

        mock_cl.regions.get.return_value = (
            fixtures.DEVELOPMENT_REGION
        )
        client = CloudRegionClient()
        self.assertEqual(
            client.is_production_region(
                region='development'),
            False)
        mock_kc.Client.assert_called_with(
            session=session_mock,
            region_name='cern'
        )

        mock_cl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        self.assertEqual(
            client.is_production_region(
                region='production'),
            True)
        mock_kc.Client.assert_called_with(
            session=session_mock,
            region_name='cern'
        )

    def test_get_servers_by_names_empty_params(self, mock_cc, mock_ks):
        client = CloudRegionClient()
        self.assertEqual(
            client.get_servers_by_names([]),
            [])

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_get_servers_by_names_empty_result(self, mock_nc, mock_kc,
                                               mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_nc.Client.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )

        mock_cl.servers.list.return_value = []
        client = CloudRegionClient()

        self.assertEqual(
            client.get_servers_by_names(
                ['non_existing']),
            [])

        mock_nc.Client.assert_called_with(
            version='2.56',
            session=session_mock,
            region_name='cern')
        mock_cl.servers.list.assert_called_with(
            search_opts={
                'all_tenants': True,
                'name': '^non_existing$'
            }
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_get_servers_by_names(self, mock_nc, mock_kc,
                                  mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_nc.Client.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        vm = base_fixtures.Resource(fixtures.SERVER)
        mock_cl.servers.list.return_value = [
            vm
        ]
        client = CloudRegionClient()

        self.assertEqual(
            client.get_servers_by_names(
                [vm.name]),
            [vm])

        mock_nc.Client.assert_called_with(
            version='2.56',
            session=session_mock,
            region_name='cern')
        mock_cl.servers.list.assert_called_with(
            search_opts={
                'all_tenants': True,
                'name': '^%s$' % vm.name
            }
        )

    def test_get_server_by_name_empty_params(self, mock_cc, mock_ks):
        client = CloudRegionClient()
        self.assertIsNone(
            client.get_servers_by_name(None))

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_get_server_by_name_empty_result(self, mock_nc, mock_kc,
                                             mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_nc.Client.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        mock_cl.servers.list.return_value = []
        client = CloudRegionClient()

        self.assertIsNone(
            client.get_servers_by_name('non_existing')
        )

        mock_nc.Client.assert_called_with(
            version='2.56',
            session=session_mock,
            region_name='cern')
        mock_cl.servers.list.assert_called_with(
            search_opts={
                'all_tenants': True,
                'name': '^non_existing$'
            }
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_get_servers_by_name(self, mock_nc, mock_kc, mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_nc.Client.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        vm = base_fixtures.Resource(fixtures.SERVER)
        mock_cl.servers.list.return_value = [
            vm
        ]
        client = CloudRegionClient()

        self.assertEqual(
            client.get_servers_by_name(
                vm.name),
            vm)

        mock_nc.Client.assert_called_with(
            version='2.56',
            session=session_mock,
            region_name='cern')
        mock_cl.servers.list.assert_called_with(
            search_opts={
                'all_tenants': True,
                'name': '^%s$' % vm.name
            }
        )

    def test_get_servers_by_hypervisors_empty_params(self, mock_cc, mock_ks):
        client = CloudRegionClient()
        self.assertEqual(
            client.get_servers_by_hypervisors([]),
            [])

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_get_servers_by_hypervisors_empty_result(self, mock_nc, mock_kc,
                                                     mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_nc.Client.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        mock_cl.servers.list.return_value = []
        client = CloudRegionClient()

        self.assertEqual(
            client.get_servers_by_hypervisors(
                ['non_existing']),
            [])

        mock_nc.Client.assert_called_with(
            version='2.56',
            session=session_mock,
            region_name='cern')
        mock_cl.servers.list.assert_called_with(
            search_opts={
                'all_tenants': True,
                'host': 'non_existing.cern.ch'
            }
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_get_servers_by_hypervisors(self, mock_nc, mock_kc,
                                        mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_nc.Client.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        vm = base_fixtures.Resource(fixtures.SERVER)
        mock_cl.servers.list.return_value = [
            vm
        ]
        client = CloudRegionClient()

        self.assertEqual(
            client.get_servers_by_hypervisors(
                [vm.host]),
            [vm])

        mock_nc.Client.assert_called_with(
            version='2.56',
            session=session_mock,
            region_name='cern')
        mock_cl.servers.list.assert_called_with(
            search_opts={
                'all_tenants': True,
                'host': 'fake_host'
            }
        )

        mock_cl.servers.list.side_effect = iter([
            [],
            [vm]
        ])

        self.assertEqual(
            client.get_servers_by_hypervisors(
                [vm.host]),
            [vm])

        mock_nc.Client.assert_called_with(
            version='2.56',
            session=session_mock,
            region_name='cern')
        mock_cl.servers.list.assert_called_with(
            search_opts={
                'all_tenants': True,
                'host': 'fake_host.cern.ch'
            }
        )

    def test_get_servers_by_hypervisor_empty_params(self, mock_cc, mock_ks):
        client = CloudRegionClient()
        self.assertEqual(
            client.get_servers_by_hypervisor(None),
            [])

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_get_servers_by_hypervisor_empty_result(self, mock_nc, mock_kc,
                                                    mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_nc.Client.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        mock_cl.servers.list.return_value = []
        client = CloudRegionClient()

        self.assertEqual(
            client.get_servers_by_hypervisor(
                'non_existing'),
            [])

        mock_nc.Client.assert_called_with(
            version='2.56',
            session=session_mock,
            region_name='cern')
        mock_cl.servers.list.assert_called_with(
            search_opts={
                'all_tenants': True,
                'host': 'non_existing.cern.ch'
            }
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_get_servers_by_hypervisor(self, mock_nc, mock_kc,
                                       mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_nc.Client.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        vm = base_fixtures.Resource(fixtures.SERVER)
        mock_cl.servers.list.return_value = [
            vm
        ]
        client = CloudRegionClient()

        self.assertEqual(
            client.get_servers_by_hypervisor(
                vm.host),
            [vm])

        mock_nc.Client.assert_called_with(
            version='2.56',
            session=session_mock,
            region_name='cern')
        mock_cl.servers.list.assert_called_with(
            search_opts={
                'all_tenants': True,
                'host': 'fake_host'
            }
        )

        mock_cl.servers.list.side_effect = iter([
            [],
            [vm]
        ])

        self.assertEqual(
            client.get_servers_by_hypervisor(
                vm.host),
            [vm])

        mock_nc.Client.assert_called_with(
            version='2.56',
            session=session_mock,
            region_name='cern')
        mock_cl.servers.list.assert_called_with(
            search_opts={
                'all_tenants': True,
                'host': 'fake_host.cern.ch'
            }
        )

    def test_get_servers_by_projects_empty_params(self, mock_cc, mock_ks):
        client = CloudRegionClient()
        self.assertEqual(
            client.get_servers_by_projects([]),
            [])

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_get_servers_by_projects_empty_result(self, mock_nc, mock_kc,
                                                  mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_nc.Client.return_value
        mock_kl = mock_kc.Client.return_value
        project = base_fixtures.Resource(fixtures.PROJECT)
        mock_kl.projects.find.return_value = project
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )

        mock_cl.servers.list.return_value = []
        client = CloudRegionClient()

        self.assertEqual(
            client.get_servers_by_projects(
                ['non_existing']),
            [])

        mock_nc.Client.assert_called_with(
            version='2.56',
            session=session_mock,
            region_name='cern')
        mock_cl.servers.list.assert_called_with(
            search_opts={
                'all_tenants': True,
                'project_id': 'fake_id'
            }
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_get_servers_by_projects(self, mock_nc, mock_kc,
                                     mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_nc.Client.return_value
        mock_kl = mock_kc.Client.return_value
        project = base_fixtures.Resource(fixtures.PROJECT)
        mock_kl.projects.find.return_value = project
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        vm = base_fixtures.Resource(fixtures.SERVER)
        mock_cl.servers.list.return_value = [
            vm
        ]
        client = CloudRegionClient()

        self.assertEqual(
            client.get_servers_by_projects(
                [getattr(vm, 'tenant_id')]),
            [vm])

        mock_nc.Client.assert_called_with(
            version='2.56',
            session=session_mock,
            region_name='cern')
        mock_cl.servers.list.assert_called_with(
            search_opts={
                'all_tenants': True,
                'project_id': 'fake_id'
            }
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_get_servers_raise_not_found(self, mock_nc, mock_kc,
                                         mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_nc.Client.return_value
        mock_kl = mock_kc.Client.return_value
        mock_cl.servers.list.side_effect = iter([
            nova_exceptions.NotFound(404)
        ])
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        client = CloudRegionClient()
        self.assertEqual(client.get_servers(), [])

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_get_servers_by_user_name(self, mock_nc, mock_kc,
                                      mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_nc.Client.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        vm = base_fixtures.Resource(fixtures.SERVER)
        mock_cl.servers.list.return_value = [
            vm
        ]
        client = CloudRegionClient()

        self.assertEqual(
            client.get_servers(
                user_name=getattr(vm, 'user_id')),
            [vm])

        mock_nc.Client.assert_called_with(
            version='2.56',
            session=session_mock,
            region_name='cern')
        mock_cl.servers.list.assert_called_with(
            search_opts={
                'all_tenants': True,
                'user_id': getattr(vm, 'user_id')
            }
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_get_servers_by_status(self, mock_nc, mock_kc,
                                   mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_nc.Client.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        vm = base_fixtures.Resource(fixtures.SERVER)
        mock_cl.servers.list.return_value = [
            vm
        ]
        client = CloudRegionClient()

        self.assertEqual(
            client.get_servers(status=vm.status),
            [vm])

        mock_nc.Client.assert_called_with(
            version='2.56',
            session=session_mock,
            region_name='cern')
        mock_cl.servers.list.assert_called_with(
            search_opts={
                'all_tenants': True,
                'status': vm.status
            }
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_get_server_raise_not_found(self, mock_nc, mock_kc,
                                        mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_nc.Client.return_value
        mock_cl.servers.get.side_effect = iter([
            nova_exceptions.NotFound(404)
        ])
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        client = CloudRegionClient()
        self.assertIsNone(client.get_server('fake_id'))

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_get_server(self, mock_nc, mock_kc,
                        mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_nc.Client.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        vm = base_fixtures.Resource(fixtures.SERVER)
        mock_cl.servers.get.return_value = vm
        client = CloudRegionClient()

        self.assertEqual(client.get_server(vm.id), vm)

        mock_nc.Client.assert_called_with(
            version='2.56',
            session=session_mock,
            region_name='cern')
        mock_cl.servers.get.assert_called_with(
            vm.id
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_get_services_empty_result(self, mock_nc, mock_kc,
                                       mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_nc.Client.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        mock_cl.services.list.return_value = []
        client = CloudRegionClient()

        self.assertEqual(client.get_nova_services_list(), [])

        mock_nc.Client.assert_called_with(
            version='2.56',
            session=session_mock,
            region_name='cern')
        mock_cl.services.list.assert_called_with(
            None,
            'nova-compute'
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_get_services(self, mock_nc, mock_kc, mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_nc.Client.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        srv = base_fixtures.Resource(fixtures.SERVICE)
        mock_cl.services.list.return_value = [srv]

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )

        client = CloudRegionClient()

        self.assertEqual(client.get_nova_services_list(), [srv])
        mock_cl.services.list.assert_called_with(
            None,
            'nova-compute'
        )

    def test_disable_nova_service(self, mock_cc, mock_ks):
        service_mock = mock.Mock()
        client = CloudRegionClient()
        client.disable_nova_service(service_mock)
        service_mock.manager.disable.assert_called_with(
            service_mock.id
        )

        client.disable_nova_service(service_mock, 'reason')
        service_mock.manager.disable_log_reason.assert_called_with(
            service_mock.id,
            'reason'
        )

    def test_enable_nova_service(self, mock_cc, mock_ks):
        service_mock = mock.Mock()
        client = CloudRegionClient()
        client.enable_nova_service(service_mock)
        service_mock.manager.enable.assert_called_with(
            service_mock.id
        )

    def test_delete_nova_service(self, mock_cc, mock_ks):
        service_mock = mock.Mock()
        client = CloudRegionClient()
        client.delete_nova_service(service_mock)
        service_mock.manager.delete.assert_called_with(
            service_mock.id
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_get_aggregates(self, mock_nc, mock_kc, mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_cl = mock_nc.Client.return_value
        aggr = base_fixtures.Resource(fixtures.AGGREGATE)
        mock_cl.aggregates.list.return_value = [aggr]

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )

        client = CloudRegionClient()

        self.assertEqual(client.get_nova_aggregates(), [aggr])
        mock_cl.aggregates.list.assert_called_with()

    def test_get_servers_by_last_action_user_empty_list(self, mock_cc,
                                                        mock_ks):
        mock_server = mock.Mock()
        mock_server.manager.api.instance_action.list.return_value = []

        client = CloudRegionClient()
        self.assertEqual(
            client.get_servers_by_last_action_user(
                [mock_server],
                'fake_user'
            ),
            []
        )
        mock_server.manager.api.instance_action.list.assert_called_with(
            mock_server
        )

    def test_get_servers_by_last_action_user(self, mock_cc, mock_ks):
        mock_server = mock.Mock()
        action = base_fixtures.Resource(fixtures.ACTION)
        mock_server.manager.api.instance_action.list.return_value = [
            action
        ]

        client = CloudRegionClient()
        self.assertEqual(
            client.get_servers_by_last_action_user(
                [mock_server],
                'fake_user'
            ),
            [mock_server]
        )
        mock_server.manager.api.instance_action.list.assert_called_with(
            mock_server
        )

        client = CloudRegionClient()
        self.assertEqual(
            client.get_servers_by_last_action_user(
                [mock_server],
                'admin'
            ),
            []
        )
        mock_server.manager.api.instance_action.list.assert_called_with(
            mock_server
        )

    def test_delete_servers(self, mock_cc, mock_ks):
        # Test with empty list
        server_mock = mock.Mock()
        client = CloudRegionClient()

        client.delete_servers([])

        server_mock.delete.assert_not_called()

        # Test delete server in error
        server_mock = mock.Mock()
        server_mock.name = 'fake_host'
        server_mock.status = 'error'

        client.delete_servers([server_mock])

        server_mock.reset_state.assert_not_called()
        server_mock.delete.assert_called_with()

        # Test delete server in creating
        server_mock = mock.Mock()
        server_mock.name = 'fake_host'
        server_mock.status = 'creating'

        client = CloudRegionClient()
        client.delete_servers([server_mock])

        server_mock.reset_state.assert_called_with(
            state='error'
        )
        server_mock.delete.assert_called_with()

        # Test delete server with side effect
        server_mock = mock.Mock()
        server_mock.name = 'fake_host'
        server_mock.status = 'creating'
        server_mock.delete.side_effect = iter([
            Exception('fail')
        ])

        client = CloudRegionClient()
        client.delete_servers([server_mock])

        server_mock.reset_state.assert_called_with(
            state='error'
        )
        server_mock.delete.assert_called_with()

        # Test delete server with force
        server_mock = mock.Mock()
        server_mock.name = 'fake_host'
        server_mock.status = 'error'
        client = CloudRegionClient()

        client.delete_servers([server_mock], force=True)

        server_mock.reset_state.assert_not_called()
        server_mock.force_delete.assert_called_with()

    def test_reset_state_servers(self, mock_cc, mock_ks):
        # Test with empty list
        server_mock = mock.Mock()
        client = CloudRegionClient()

        client.reset_state_servers([])

        server_mock.reset_state.assert_not_called()

        # Test reset server in error
        server_mock = mock.Mock()
        server_mock.name = 'fake_host'
        server_mock.status = 'error'

        client.reset_state_servers([server_mock])

        server_mock.reset_state.assert_not_called()

        # Test reset server in creating
        server_mock = mock.Mock()
        server_mock.name = 'fake_host'
        server_mock.status = 'creating'

        client.reset_state_servers([server_mock])

        server_mock.reset_state.assert_called_with(
            state='error'
        )

        # Test with server with side effect
        server_mock = mock.Mock()
        server_mock.name = 'fake_host'
        server_mock.status = 'creating'
        server_mock.reset_state.side_effect = iter([
            Exception('fail')
        ])

        client.reset_state_servers([server_mock])

        server_mock.reset_state.assert_called_with(
            state='error'
        )

    def test_power_on_servers(self, mock_cc, mock_ks):
        server_mock = mock.Mock()
        setattr(server_mock, 'OS-EXT-STS:vm_state', 'active')

        client = CloudRegionClient()
        client.power_on_servers([server_mock])
        server_mock.start.assert_not_called()

        setattr(server_mock, 'OS-EXT-STS:vm_state', 'shutoff')
        client.power_on_servers([server_mock])
        server_mock.start.assert_called_with()

    def test_power_off_servers(self, mock_cc, mock_ks):
        server_mock = mock.Mock()
        setattr(server_mock, 'OS-EXT-STS:vm_state', 'stopped')

        client = CloudRegionClient()
        client.power_off_servers([server_mock])
        server_mock.stop.assert_not_called()

        setattr(server_mock, 'OS-EXT-STS:vm_state', 'active')
        client.power_off_servers([server_mock])
        server_mock.stop.assert_called_with()

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.neutron_client')
    def test_get_neutron_agents_empty_list(self, mock_nc, mock_kc,
                                           mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_cl = mock_nc.Client.return_value

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        mock_cl.list_agents.return_value = []
        client = CloudRegionClient()

        self.assertEqual(
            client.get_neutron_agents_list(host='fake_host'), [])

        mock_nc.Client.assert_called_with(
            session=session_mock,
            region_name='sdn1')
        mock_cl.list_agents.assert_called_with(
            'fake_host',
            'neutron-linuxbridge-agent'
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.neutron_client')
    def test_get_neutron_agents_list(self, mock_nc, mock_kc,
                                     mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_cl = mock_nc.Client.return_value
        mock_cl.list_agents.return_value = {
            'agents': [
                fixtures.AGENT
            ]
        }

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )

        client = CloudRegionClient()

        self.assertEqual(
            client.get_neutron_agents_list(host='fake_host'),
            [fixtures.AGENT])
        mock_cl.list_agents.assert_called_with(
            'fake_host',
            'neutron-linuxbridge-agent'
        )

    @mock.patch('ccitools.utils.cloud.neutron_client')
    def test_change_status_neutron_agent(self, mock_nc, mock_cc, mock_ks):
        mock_cl = mock_nc.Client.return_value
        agent = fixtures.AGENT
        agent['region'] = 'production'

        client = CloudRegionClient()

        client.change_status_neutron_agent(
            agent=agent,
            status=True,
            reason='Enabled'
        )
        mock_cl.update_agent.assert_called_with(
            agent['id'],
            description='Enabled',
            is_admin_state_up=True,
            admin_state_up=True
        )

        client.change_status_neutron_agent(
            agent=agent,
            status=False,
            reason='Disabled'
        )
        mock_cl.update_agent.assert_called_with(
            agent['id'],
            description='Disabled',
            is_admin_state_up=False,
            admin_state_up=False
        )

    @mock.patch('ccitools.utils.cloud.neutron_client')
    def test_delete_neutron_agent(self, mock_nc, mock_cc, mock_ks):
        mock_cl = mock_nc.Client.return_value

        agent = fixtures.AGENT
        agent['region'] = 'development'

        client = CloudRegionClient()
        client.delete_neutron_agent(agent=agent)

        mock_cl.delete_agent.assert_called_with(
            agent['id']
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.cinder_client')
    def test_get_volumes_empty_list(self, mock_ci, mock_kc,
                                    mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_cl = mock_ci.Client.return_value
        mock_cl.volumes.list.return_value = []

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )

        client = CloudRegionClient()

        self.assertEqual(client.get_volumes(), [])
        mock_ci.Client.assert_called_with(
            session=session_mock,
            region_name='cern'
        )
        mock_cl.volumes.list.assert_called_with(
            search_opts={'all_tenants': True}
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.cinder_client')
    def test_get_volumes_by_user_name(self, mock_ci, mock_kc,
                                      mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_ci.Client.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        volume = base_fixtures.Resource(fixtures.VOLUME)
        mock_cl.volumes.list.return_value = [
            volume
        ]
        client = CloudRegionClient()

        self.assertEqual(
            client.get_volumes(
                user_name=getattr(volume, 'user_id')),
            [volume])

        mock_ci.Client.assert_called_with(
            session=session_mock,
            region_name='cern'
        )
        mock_cl.volumes.list.assert_called_with(
            search_opts={
                'all_tenants': True,
                'user_id': getattr(volume, 'user_id')
            }
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.cinder_client')
    def test_get_volumes_by_project_id(self, mock_ci, mock_kc,
                                       mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_cl = mock_ci.Client.return_value

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        volume = base_fixtures.Resource(fixtures.VOLUME)
        mock_cl.volumes.list.return_value = [
            volume
        ]
        client = CloudRegionClient()

        self.assertEqual(
            client.get_volumes(
                project_id=getattr(volume, 'tenant_id')),
            [volume])

        mock_ci.Client.assert_called_with(
            session=session_mock,
            region_name='cern'
        )
        mock_cl.volumes.list.assert_called_with(
            search_opts={
                'all_tenants': True,
                'project_id': getattr(volume, 'tenant_id')
            }
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.cinder_client')
    def test_get_volumes_by_status(self, mock_ci, mock_kc,
                                   mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_cl = mock_ci.Client.return_value

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        volume = base_fixtures.Resource(fixtures.VOLUME)
        mock_cl.volumes.list.return_value = [
            volume
        ]
        client = CloudRegionClient()

        self.assertEqual(
            client.get_volumes(
                status=volume.status),
            [volume])

        mock_ci.Client.assert_called_with(
            session=session_mock,
            region_name='cern'
        )
        mock_cl.volumes.list.assert_called_with(
            search_opts={
                'all_tenants': True,
                'status': volume.status
            }
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.cinder_client')
    def test_get_volumes_by_project(self, mock_ci, mock_kc,
                                    mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_ci.Client.return_value
        mock_kl = mock_kc.Client.return_value
        project = base_fixtures.Resource(fixtures.PROJECT)
        mock_kl.projects.find.return_value = project
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        volume = base_fixtures.Resource(fixtures.VOLUME)
        mock_cl.volumes.list.return_value = [
            volume
        ]
        client = CloudRegionClient()

        self.assertEqual(
            client.get_volumes(
                project_name=getattr(volume, 'tenant_id')),
            [volume])

        mock_ci.Client.assert_called_with(
            session=session_mock,
            region_name='cern'
        )
        mock_cl.volumes.list.assert_called_with(
            search_opts={
                'all_tenants': True,
                'project_id': 'fake_id'
            }
        )

    @mock.patch('ccitools.utils.cloud.cinder_client')
    def test_get_volume_types_empty_list(self, mock_ci, mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_ci.Client.return_value
        mock_cl.volume_types.list.return_value = []

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )

        self.assertEqual(
            CloudRegionClient().get_volume_types(
                region='production'
            ),
            []
        )
        mock_ci.Client.assert_called_with(
            session=session_mock,
            region_name='production'
        )
        mock_cl.volume_types.list.assert_called_with(
            is_public=True
        )

    @mock.patch('ccitools.utils.cloud.cinder_client')
    def test_get_volume_types(self, mock_ci, mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_ci.Client.return_value
        type_public = base_fixtures.Resource(fixtures.VOLUME_TYPE_PUBLIC)
        mock_cl.volume_types.list.return_value = [type_public]

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )

        self.assertEqual(
            CloudRegionClient().get_volume_types(
                region='production',
                show_all=False),
            [type_public]
        )
        mock_ci.Client.assert_called_with(
            session=session_mock,
            region_name='production'
        )
        mock_cl.volume_types.list.assert_called_with(
            is_public=True
        )

    @mock.patch('ccitools.utils.cloud.cinder_client')
    def test_get_volume_types_all(self, mock_ci, mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_ci.Client.return_value
        type_public = base_fixtures.Resource(fixtures.VOLUME_TYPE_PUBLIC)
        type_private = base_fixtures.Resource(fixtures.VOLUME_TYPE_PRIVATE)
        mock_cl.volume_types.list.return_value = [type_public, type_private]

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )

        self.assertEqual(
            CloudRegionClient().get_volume_types(
                region='production',
                show_all=True),
            [type_public, type_private]
        )
        mock_ci.Client.assert_called_with(
            session=session_mock,
            region_name='production'
        )
        mock_cl.volume_types.list.assert_called_with(
            is_public=None
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.cinder_client')
    def test_get_volumes_snapshots_empty_list(self, mock_ci, mock_kc,
                                              mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_cl = mock_ci.Client.return_value
        mock_cl.volume_snapshots.list.return_value = []

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )

        client = CloudRegionClient()

        self.assertEqual(client.get_volume_snapshots(), [])
        mock_ci.Client.assert_called_with(
            session=session_mock,
            region_name='cern'
        )
        mock_cl.volume_snapshots.list.assert_called_with(
            search_opts={'all_tenants': True}
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.cinder_client')
    def test_get_volumes_snapshots_by_user_name(self, mock_ci, mock_kc,
                                                mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_ci.Client.return_value
        mock_kl = mock_kc.Client.return_value

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        snapshot = base_fixtures.Resource(fixtures.VOLUME_SNAPSHOT)
        mock_cl.volume_snapshots.list.return_value = [
            snapshot
        ]
        client = CloudRegionClient()

        self.assertEqual(
            client.get_volume_snapshots(
                user_name=getattr(snapshot, 'user_id')),
            [snapshot])

        mock_ci.Client.assert_called_with(
            session=session_mock,
            region_name='cern'
        )
        mock_cl.volume_snapshots.list.assert_called_with(
            search_opts={
                'all_tenants': True,
                'user_id': getattr(snapshot, 'user_id')
            }
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.cinder_client')
    def test_get_volumes_snapshots_by_project(self, mock_ci, mock_kc,
                                              mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_ci.Client.return_value
        mock_kl = mock_kc.Client.return_value
        project = base_fixtures.Resource(fixtures.PROJECT)
        mock_kl.projects.find.return_value = project
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        snapshot = base_fixtures.Resource(fixtures.VOLUME_SNAPSHOT)
        mock_cl.volume_snapshots.list.return_value = [
            snapshot
        ]
        client = CloudRegionClient()

        self.assertEqual(
            client.get_volume_snapshots(
                project_name=getattr(snapshot, 'tenant_id')),
            [snapshot])

        mock_ci.Client.assert_called_with(
            session=session_mock,
            region_name='cern'
        )
        mock_cl.volume_snapshots.list.assert_called_with(
            search_opts={
                'all_tenants': True,
                'project_id': 'fake_id'
            }
        )

    def test_delete_volumes(self, mock_cc, mock_ks):
        # Test with empty list
        mock_volume = mock.Mock()
        client = CloudRegionClient()

        client.delete_volumes([])

        mock_volume.delete.assert_not_called()

        # Test delete volume operation in error
        mock_volume = mock.Mock()
        mock_volume.status = 'error'
        client.delete_volumes([mock_volume])

        mock_volume.reset_state.assert_not_called()
        mock_volume.delete.assert_called_with()

        # Test delete volume operation in creating
        mock_volume = mock.Mock()
        mock_volume.status = 'creating'
        client.delete_volumes([mock_volume])

        mock_volume.reset_state.assert_called_with(
            'error',
            attach_status='detached'
        )
        mock_volume.delete.assert_called_with()

        # Test with volume and side effect
        mock_volume = mock.Mock()
        mock_volume.status = 'creating'
        mock_volume.delete.side_effect = iter([
            Exception('fail')
        ])

        client.delete_volumes([mock_volume])

        mock_volume.reset_state.assert_called_with(
            'error',
            attach_status='detached'
        )
        mock_volume.delete.assert_called_with()

    def test_reset_state_volumes(self, mock_cc, mock_ks):
        # Test with empty list
        mock_volume = mock.Mock()
        client = CloudRegionClient()

        client.reset_state_volumes([])

        mock_volume.reset_state.assert_not_called()

        # Test reset volume in error
        mock_volume = mock.Mock()
        mock_volume.status = 'error'
        client.reset_state_volumes([mock_volume])

        mock_volume.reset_state.assert_not_called()

        # Test reset volume in creating
        mock_volume = mock.Mock()
        mock_volume.status = 'creating'
        client.reset_state_volumes([mock_volume])

        mock_volume.reset_state.assert_called_with(
            'error',
            attach_status='detached'
        )

        # Test with volume with side effect
        mock_volume = mock.Mock()
        mock_volume.status = 'creating'
        mock_volume.reset_state.side_effect = iter([
            Exception('fail')
        ])

        client.reset_state_volumes([mock_volume])

        mock_volume.reset_state.assert_called_with(
            'error',
            attach_status='detached'
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.cinder_client')
    def test_create_volume(self, mock_ci, mock_kc,
                           mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_cl = mock_ci.Client.return_value
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )

        client = CloudRegionClient()

        client.create_volume(
            name='Test',
            size=1,
            volume_type='standard'
        )
        mock_ci.Client.assert_called_with(
            session=session_mock,
            region_name='cern'
        )
        mock_cl.volumes.create.assert_called_with(
            name='Test',
            size=1,
            volume_type='standard',
            imageRef=None
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.cinder_client')
    def test_create_volume_throws_exception(
            self,
            mock_ci,
            mock_kc,
            mock_cc,
            mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_cl = mock_ci.Client.return_value
        mock_cl.volumes.create.side_effect = Exception()

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )

        client = CloudRegionClient()

        self.assertIsNone(
            client.create_volume(
                name='Test',
                size=1,
                volume_type='standard'
            )
        )
        mock_ci.Client.assert_called_with(
            session=session_mock,
            region_name='cern'
        )
        mock_cl.volumes.create.assert_called_with(
            name='Test',
            size=1,
            volume_type='standard',
            imageRef=None
        )

    def test_create_volume_snapshot(self, mock_cc, mock_ks):
        volume_mock = mock.Mock()
        client = CloudRegionClient()
        client.create_volume_snapshot(volume_mock, name='Test')
        volume_mock.manager.api.volume_snapshots.create.assert_called_with(
            name='Test',
            volume_id=volume_mock.id,
            force=True
        )

    def test_create_volume_snapshot_throws_exception(self, mock_cc, mock_ks):
        volume_mock = mock.Mock()
        volume_mock.manager.api.volume_snapshots.create.side_effect = (
            Exception()
        )
        client = CloudRegionClient()
        self.assertIsNone(
            client.create_volume_snapshot(volume_mock, name='Test'))

        volume_mock.manager.api.volume_snapshots.create.assert_called_with(
            name='Test',
            volume_id=volume_mock.id,
            force=True
        )

    @mock.patch('ccitools.utils.cloud.glance_client')
    def test_get_image(self, mock_gc, mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_gc.Client.return_value

        mock_cl.images.get.return_value = None

        client = CloudRegionClient()
        self.assertIsNone(
            client.get_image(id='None'))

        mock_gc.Client.assert_called_with(
            session=session_mock,
            region_name='cern'
        )
        mock_cl.images.get.assert_called_with('None')

    @mock.patch('ccitools.utils.cloud.glance_client')
    def test_update_image(self, mock_gc, mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_gc.Client.return_value

        mock_cl.images.update.return_value = None

        client = CloudRegionClient()
        self.assertIsNone(
            client.update_image(
                id='None',
                name='Test'
            )
        )

        mock_gc.Client.assert_called_with(
            session=session_mock,
            region_name='cern'
        )
        mock_cl.images.update.assert_called_with(
            'None',
            {'name': 'Test'}
        )

    @mock.patch('ccitools.utils.cloud.glance_client')
    def test_get_images_by_name_empty_list(self, mock_gc,
                                           mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_gc.Client.return_value

        mock_cl.images.list.return_value = []
        client = CloudRegionClient()

        self.assertEqual(
            client.get_images_by_name(
                name='fake'),
            [])

        mock_gc.Client.assert_called_with(
            session=session_mock,
            region_name='cern'
        )
        mock_cl.images.list.assert_called_with(
            filters={
                'name': 'fake'
            }
        )

    @mock.patch('ccitools.utils.cloud.glance_client')
    def test_get_images_by_name(self, mock_gc, mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_gc.Client.return_value

        image = base_fixtures.Resource(fixtures.IMAGE)
        mock_cl.images.list.return_value = [image]
        client = CloudRegionClient()

        self.assertEqual(
            client.get_images_by_name(
                name='fake'),
            [image])

        mock_gc.Client.assert_called_with(
            session=session_mock,
            region_name='cern'
        )
        mock_cl.images.list.assert_called_with(
            filters={
                'name': 'fake'
            }
        )

    @mock.patch('ccitools.utils.cloud.glance_client')
    def test_image_add_project(self, mock_gc, mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_gc.Client.return_value

        client = CloudRegionClient()
        self.assertIsNone(
            client.image_add_project(
                'None',
                'fake_project'
            )
        )

        mock_gc.Client.assert_called_with(
            session=session_mock,
            region_name='cern'
        )
        mock_cl.image_members.create.assert_called_with(
            'None',
            'fake_project'
        )
        mock_cl.image_members.update.assert_called_with(
            'None',
            'fake_project',
            'accepted'
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.glance_client')
    def test_get_images_by_project_empty_list(self, mock_gc, mock_kc,
                                              mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_gc.Client.return_value
        mock_kl = mock_kc.Client.return_value
        project = base_fixtures.Resource(fixtures.PROJECT)
        mock_kl.projects.find.return_value = project

        mock_cl.images.list.return_value = []
        client = CloudRegionClient()

        self.assertEqual(
            client.get_images_by_project(
                project_name='fake_id'),
            [])

        mock_gc.Client.assert_called_with(
            session=session_mock,
            region_name='cern'
        )
        mock_cl.images.list.assert_called_with(
            filters={
                'owner': 'fake_id',
                'visibility': 'private'
            }
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.glance_client')
    def test_get_images_by_project(self, mock_gc, mock_kc,
                                   mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_gc.Client.return_value
        mock_kl = mock_kc.Client.return_value
        project = base_fixtures.Resource(fixtures.PROJECT)
        mock_kl.projects.find.return_value = project

        image = base_fixtures.Resource(fixtures.IMAGE)
        mock_cl.images.list.return_value = [image]
        client = CloudRegionClient()

        self.assertEqual(
            client.get_images_by_project(
                project_name='fake_id'),
            [image])

        mock_gc.Client.assert_called_with(
            session=session_mock,
            region_name='cern'
        )
        mock_cl.images.list.assert_called_with(
            filters={
                'owner': 'fake_id',
                'visibility': 'private'
            }
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.manila_client')
    def test_get_shares_by_project_empty_list(self, mock_mc, mock_kc,
                                              mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_mc.Client.return_value
        mock_kl = mock_kc.Client.return_value
        project = base_fixtures.Resource(fixtures.PROJECT)
        mock_kl.projects.find.return_value = project
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        mock_cl.shares.list.return_value = []
        client = CloudRegionClient()

        self.assertEqual(
            client.get_shares_by_project(
                project_name='fake_id'),
            [])

        mock_mc.Client.assert_called_with(
            api_version=manila_api_versions.APIVersion('2.51'),
            session=session_mock,
            region_name='cern'
        )
        mock_cl.shares.list.assert_called_with(
            search_opts={
                'all_tenants': True,
                'project_id': 'fake_id'
            }
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.manila_client')
    def test_get_shares_by_project(self, mock_mc, mock_kc,
                                   mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_mc.Client.return_value
        mock_kl = mock_kc.Client.return_value
        project = base_fixtures.Resource(fixtures.PROJECT)
        mock_kl.projects.find.return_value = project
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        share = base_fixtures.Resource(fixtures.SHARE)
        mock_cl.shares.list.return_value = [share]
        client = CloudRegionClient()

        self.assertEqual(
            client.get_shares_by_project(
                project_name='fake_id'),
            [share])

        mock_mc.Client.assert_called_with(
            api_version=manila_api_versions.APIVersion('2.51'),
            session=session_mock,
            region_name='cern'
        )
        mock_cl.shares.list.assert_called_with(
            search_opts={
                'all_tenants': True,
                'project_id': 'fake_id'
            }
        )

    @mock.patch('ccitools.utils.cloud.manila_client')
    def test_get_share_types_empty_list(self, mock_mc, mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_mc.Client.return_value
        mock_cl.share_types.list.return_value = []

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )

        self.assertEqual(
            CloudRegionClient().get_share_types(region='production'),
            []
        )
        mock_mc.Client.assert_called_with(
            api_version=manila_api_versions.APIVersion('2.51'),
            session=session_mock,
            region_name='production'
        )
        mock_cl.share_types.list.assert_called_with(show_all=False)

    @mock.patch('ccitools.utils.cloud.manila_client')
    def test_get_share_types(self, mock_mc, mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_mc.Client.return_value
        type_public = base_fixtures.Resource(fixtures.SHARE_TYPE_PUBLIC)
        mock_cl.share_types.list.return_value = [type_public]

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )

        self.assertEqual(
            CloudRegionClient().get_share_types(region='production'),
            [type_public]
        )
        mock_mc.Client.assert_called_with(
            api_version=manila_api_versions.APIVersion('2.51'),
            session=session_mock,
            region_name='production'
        )
        mock_cl.share_types.list.assert_called_with(show_all=False)

    @mock.patch('ccitools.utils.cloud.manila_client')
    def test_get_share_types_all(self, mock_mc, mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_mc.Client.return_value

        type_public = base_fixtures.Resource(fixtures.SHARE_TYPE_PUBLIC)
        type_private = base_fixtures.Resource(fixtures.SHARE_TYPE_PRIVATE)
        mock_cl.share_types.list.return_value = [type_public, type_private]

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )

        self.assertEqual(
            CloudRegionClient().get_share_types(
                region='production',
                show_all=True),
            [type_public, type_private]
        )
        mock_mc.Client.assert_called_with(
            api_version=manila_api_versions.APIVersion('2.51'),
            session=session_mock,
            region_name='production'
        )
        mock_cl.share_types.list.assert_called_with(show_all=True)

    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_find_project_by_id(self, mock_kc, mock_cc, mock_ks):
        mock_cl = mock_kc.Client.return_value
        project = base_fixtures.Resource(fixtures.PROJECT)
        mock_cl.projects.find.return_value = project

        client = CloudRegionClient()

        self.assertEqual(
            client.find_project('fake_id'),
            project)

        mock_kc.Client.assert_called_with(
            session=client.session,
            region_name='cern')

        mock_cl.projects.find.assert_called_with(
            name='fake_id'
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_find_project_by_name(self, mock_kc, mock_cc, mock_ks):
        mock_cl = mock_kc.Client.return_value
        project = base_fixtures.Resource(fixtures.PROJECT)
        mock_cl.projects.find.side_effect = iter([
            keystone_exceptions.NotFound()
        ])
        mock_cl.projects.get.return_value = project

        client = CloudRegionClient()

        self.assertEqual(
            client.find_project('fake_name'),
            project)

        mock_kc.Client.assert_called_with(
            session=client.session,
            region_name='cern')

        mock_cl.projects.find.assert_called_with(
            name='fake_name'
        )
        mock_cl.projects.get.assert_called_with(
            'fake_name'
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_get_project(self, mock_kc, mock_cc, mock_ks):
        mock_cl = mock_kc.Client.return_value
        project = base_fixtures.Resource(fixtures.PROJECT)
        mock_cl.projects.get.return_value = project

        client = CloudRegionClient()

        self.assertEqual(
            client.get_project('fake_id'),
            project)

        mock_kc.Client.assert_called_with(
            session=client.session,
            region_name='cern')

        mock_cl.projects.get.assert_called_with(
            'fake_id'
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_get_project_members_empty_list(self, mock_kc, mock_cc, mock_ks):
        mock_cl = mock_kc.Client.return_value
        project = base_fixtures.Resource(fixtures.PROJECT)
        mock_cl.role_assignments.list.return_value = []

        client = CloudRegionClient()

        self.assertEqual(
            client.get_project_members(project),
            [])

        mock_kc.Client.assert_called_with(
            session=client.session,
            region_name='cern')

        mock_cl.role_assignments.list.assert_called_with(
            project=project
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_get_project_members(self, mock_kc, mock_cc, mock_ks):
        mock_cl = mock_kc.Client.return_value
        project = base_fixtures.Resource(fixtures.PROJECT)
        mock_cl.role_assignments.list.return_value = [
            base_fixtures.Resource(fixtures.ASIGN_USER),
            base_fixtures.Resource(fixtures.ASIGN_GROUP),
        ]

        client = CloudRegionClient()

        self.assertEqual(
            client.get_project_members(project),
            ['fake_user', 'fake_group'])

        mock_kc.Client.assert_called_with(
            session=client.session,
            region_name='cern')

        mock_cl.role_assignments.list.assert_called_with(
            project=project
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_get_project_owner_empty(self, mock_kc, mock_cc, mock_ks):
        mock_cl = mock_kc.Client.return_value
        project = base_fixtures.Resource(fixtures.PROJECT)
        mock_cl.role_assignments.list.return_value = []

        client = CloudRegionClient()

        self.assertIsNone(client.get_project_owner(project))

        mock_kc.Client.assert_called_with(
            session=client.session,
            region_name='cern')

        mock_cl.role_assignments.list.assert_called_with(
            project=project,
            include_names=True
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_get_project_owner(self, mock_kc, mock_cc, mock_ks):
        mock_cl = mock_kc.Client.return_value
        project = base_fixtures.Resource(fixtures.PROJECT)
        mock_cl.role_assignments.list.return_value = [
            base_fixtures.Resource(fixtures.ASIGN_GROUP),
            base_fixtures.Resource(fixtures.ASIGN_USER)
        ]

        client = CloudRegionClient()

        self.assertEqual(
            client.get_project_owner(project), 'fake_user')

        mock_kc.Client.assert_called_with(
            session=client.session,
            region_name='cern')

        mock_cl.role_assignments.list.assert_called_with(
            project=project,
            include_names=True
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_get_projects_by_role(self, mock_kc, mock_cc, mock_ks):
        mock_cl = mock_kc.Client.return_value
        project = base_fixtures.Resource(fixtures.PROJECT)
        mock_cl.projects.get.return_value = project
        mock_cl.role_assignments.list.return_value = [
            base_fixtures.Resource(fixtures.ASIGN_USER)
        ]

        mock_cl.roles.list.return_value = [fixtures.OWNER_ROLE]

        client = CloudRegionClient()

        self.assertEqual(
            client.get_projects_by_role(user='fake_user'), [project])

        mock_kc.Client.assert_called_with(
            session=client.session,
            region_name='cern')

        mock_cl.roles.list.assert_called_with(
            name='owner'
        )

        mock_cl.role_assignments.list.assert_called_with(
            user='fake_user',
            role=fixtures.OWNER_ROLE,
            include_names=True
        )

        mock_cl.projects.get.assert_called_with('1')

    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_grant_role_in_project(self, mock_kc, mock_cc, mock_ks):
        mock_cl = mock_kc.Client.return_value
        mock_cl.roles.list.return_value = [
            base_fixtures.Resource(fixtures.MEMBER_ROLE)
        ]

        client = CloudRegionClient()
        client.grant_role_in_project(
            project_id='fake_project',
            user_id='my_user'
        )

        mock_kc.Client.assert_called_with(
            session=client.session,
            region_name='cern')

        mock_cl.roles.grant.assert_called_with(
            fixtures.MEMBER_ROLE['id'],
            user='my_user',
            group=None,
            project='fake_project'
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_revoke_role_in_project(self, mock_kc, mock_cc, mock_ks):
        mock_cl = mock_kc.Client.return_value
        mock_cl.roles.list.return_value = [
            base_fixtures.Resource(fixtures.MEMBER_ROLE)
        ]

        client = CloudRegionClient()
        client.revoke_role_in_project(
            project_id='fake_project',
            user_id='my_user'
        )

        mock_kc.Client.assert_called_with(
            session=client.session,
            region_name='cern')

        mock_cl.roles.revoke.assert_called_with(
            fixtures.MEMBER_ROLE['id'],
            user='my_user',
            group=None,
            project='fake_project'
        )

    @mock.patch('ccitools.utils.cloud.ClientRest')
    def test_delete_project_fim_properties(self, mock_corn, mock_cc, mock_ks):
        cornerstone_mock = mock_corn.return_value

        client = CloudRegionClient()
        client.delete_project_fim_properties('fake_project')

        mock_corn.assert_called_with(
            kerberos=True,
            url='https://cornerstone.cern.ch/api/wsdl'
        )
        cornerstone_mock.project_properties_delete.assert_called_with = [
            ('fake_project', 'fim-skip'),
            ('fake_project', 'fim-lock')
        ]

    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_find_region_project_empty_list(self, mock_kc, mock_cc, mock_ks):
        mock_cl = mock_kc.Client.return_value
        project = base_fixtures.Resource(fixtures.PROJECT)
        mock_cl.endpoint_groups.list.return_value = []

        client = CloudRegionClient()

        self.assertEqual(
            client.find_region_project(project),
            'cern'
        )

        mock_cl.endpoint_groups.list.assert_called_with(
            project=project
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_find_region_project_cern(self, mock_kc, mock_cc, mock_ks):
        mock_cl = mock_kc.Client.return_value
        project = base_fixtures.Resource(fixtures.PROJECT)
        mock_cl.endpoint_groups.list.return_value = [
            base_fixtures.Resource(fixtures.ENDPOINT_GROUP_BASE)
        ]

        client = CloudRegionClient()

        self.assertEqual(
            client.find_region_project(project),
            'cern'
        )

        mock_cl.endpoint_groups.list.assert_called_with(
            project=project
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_find_region_project_batch(self, mock_kc, mock_cc, mock_ks):
        mock_cl = mock_kc.Client.return_value
        project = base_fixtures.Resource(fixtures.PROJECT)
        mock_cl.endpoint_groups.list.return_value = [
            base_fixtures.Resource(fixtures.ENDPOINT_GROUP_BATCH)
        ]

        client = CloudRegionClient()

        self.assertEqual(
            client.find_region_project(project),
            'batch'
        )

        mock_cl.endpoint_groups.list.assert_called_with(
            project=project
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_change_region_project_no_action(self, mock_kc, mock_cc, mock_ks):
        project = base_fixtures.Resource(fixtures.PROJECT)
        client = CloudRegionClient()
        client.change_region_project(project, 'cern', 'cern')

        mock_kc.Client.assert_not_called()

    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_change_region_project_only_remove(self, mock_kc, mock_cc,
                                               mock_ks):
        project = base_fixtures.Resource(fixtures.PROJECT)

        mock_cl = mock_kc.Client.return_value
        base_eg = base_fixtures.Resource(fixtures.ENDPOINT_GROUP_BASE)
        batch_eg = base_fixtures.Resource(fixtures.ENDPOINT_GROUP_BATCH)
        mock_ef = mock_cl.endpoint_filter

        mock_ef.list_endpoint_groups_for_project.side_effect = iter([
            [base_eg, batch_eg],
            [batch_eg],
        ])
        mock_cl.endpoint_groups.list.return_value = [
            base_eg,
            batch_eg
        ]

        client = CloudRegionClient()
        client.change_region_project(project, 'cern', 'batch')

        mock_kc.Client.assert_called_with(
            session=client.session,
            region_name='cern'
        )
        mock_ef.delete_endpoint_group_from_project.assert_called_with(
            endpoint_group=base_eg,
            project=project
        )
        mock_ef.add_endpoint_group_to_project.assert_not_called()

    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_change_region_project_only_add(self, mock_kc, mock_cc, mock_ks):
        project = base_fixtures.Resource(fixtures.PROJECT)

        mock_cl = mock_kc.Client.return_value
        base_eg = base_fixtures.Resource(fixtures.ENDPOINT_GROUP_BASE)
        batch_eg = base_fixtures.Resource(fixtures.ENDPOINT_GROUP_BATCH)
        mock_ef = mock_cl.endpoint_filter

        mock_ef.list_endpoint_groups_for_project.side_effect = iter([
            [base_eg],
            [],
        ])
        mock_cl.endpoint_groups.list.return_value = [
            base_eg,
            batch_eg
        ]

        client = CloudRegionClient()
        client.change_region_project(project, 'cern', 'batch')

        mock_kc.Client.assert_called_with(
            session=client.session,
            region_name='cern'
        )
        mock_ef.delete_endpoint_group_from_project.assert_called_with(
            endpoint_group=base_eg,
            project=project
        )
        mock_ef.add_endpoint_group_to_project.assert_called_with(
            endpoint_group=batch_eg,
            project=project
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_is_group_raise_exception(self, mock_kc, mock_cc, mock_ks):
        mock_cl = mock_kc.Client.return_value
        client = CloudRegionClient()

        mock_cl.groups.get.return_value = [
            base_fixtures.Resource(fixtures.GROUP)
        ]

        self.assertEqual(client.is_group('fake_group'), True)

        mock_kc.Client.assert_called_with(
            session=client.session,
            region_name='cern')
        mock_cl.groups.get.assert_called_with('fake_group')

        mock_cl.groups.get.side_effect = iter([
            keystone_exceptions.NotFound(404)
        ])

        self.assertEqual(client.is_group('fake_user'), False)

        mock_kc.Client.assert_called_with(
            session=client.session,
            region_name='cern')
        mock_cl.groups.get.assert_called_with('fake_user')

    @mock.patch('ccitools.utils.cloud.ClientRest')
    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_add_group_member_existing(self, mock_kc, mock_corn,
                                       mock_cc, mock_ks):
        cornerstone_mock = mock_corn.return_value
        mock_cl = mock_kc.Client.return_value
        project = base_fixtures.Resource(fixtures.PROJECT)
        mock_cl.role_assignments.list.return_value = [
            base_fixtures.Resource(fixtures.ASIGN_GROUP),
        ]

        client = CloudRegionClient()
        client.add_group_member(project, 'fake_group')

        mock_corn.assert_called_with(
            kerberos=True,
            url='https://cornerstone.cern.ch/api/wsdl'
        )
        cornerstone_mock.add_project_members.assert_not_called()

    @mock.patch('ccitools.utils.cloud.ClientRest')
    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_add_group_member_error(self, mock_kc, mock_corn,
                                    mock_cc, mock_ks):
        cornerstone_mock = mock_corn.return_value
        mock_cl = mock_kc.Client.return_value
        project = base_fixtures.Resource(fixtures.PROJECT)
        mock_cl.role_assignments.list.return_value = []
        cornerstone_mock.add_project_members.return_value = (
            base_fixtures.Resource(
                {
                    "status_code": 500,
                    "text": '{"property_value":["error"]}'
                }
            )
        )

        client = CloudRegionClient()

        with self.assertRaises(Exception):
            client.add_group_member(project, 'fake_group')

        mock_corn.assert_called_with(
            kerberos=True,
            url='https://cornerstone.cern.ch/api/wsdl'
        )
        cornerstone_mock.add_project_members.assert_called_with(
            project.id,
            'Member',
            'fake_group'
        )

    @mock.patch('ccitools.utils.cloud.ClientRest')
    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_add_group_member(self, mock_kc, mock_corn, mock_cc, mock_ks):
        cornerstone_mock = mock_corn.return_value
        mock_cl = mock_kc.Client.return_value
        project = base_fixtures.Resource(fixtures.PROJECT)
        mock_cl.role_assignments.list.return_value = []
        cornerstone_mock.add_project_members.return_value = (
            base_fixtures.Resource({"status_code": 200})
        )

        client = CloudRegionClient()
        client.add_group_member(project, 'fake_group')

        mock_corn.assert_called_with(
            kerberos=True,
            url='https://cornerstone.cern.ch/api/wsdl'
        )
        cornerstone_mock.add_project_members.assert_called_with(
            project.id,
            'Member',
            'fake_group'
        )

    @mock.patch('ccitools.utils.cloud.ClientRest')
    def test_set_project_property_error(self, mock_corn, mock_cc, mock_ks):
        cornerstone_mock = mock_corn.return_value
        project = base_fixtures.Resource(fixtures.PROJECT)
        cornerstone_mock.project_properties_add.return_value = (
            base_fixtures.Resource({"status_code": 500})
        )

        client = CloudRegionClient()

        with self.assertRaises(Exception):
            client.set_project_property(project, 'key', 'value')

        mock_corn.assert_called_with(
            kerberos=True,
            url='https://cornerstone.cern.ch/api/wsdl'
        )
        cornerstone_mock.project_properties_add.assert_called_with(
            project.id,
            'key',
            'value'
        )

    @mock.patch('ccitools.utils.cloud.ClientRest')
    def test_set_project_property(self, mock_corn, mock_cc, mock_ks):
        cornerstone_mock = mock_corn.return_value
        project = base_fixtures.Resource(fixtures.PROJECT)
        cornerstone_mock.project_properties_add.return_value = (
            base_fixtures.Resource({"status_code": 200})
        )

        client = CloudRegionClient()
        client.set_project_property(project, 'key', 'value')

        mock_corn.assert_called_with(
            kerberos=True,
            url='https://cornerstone.cern.ch/api/wsdl'
        )
        cornerstone_mock.project_properties_add.assert_called_with(
            project.id,
            'key',
            'value'
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.RadosGWAdminConnection')
    @mock.patch('ccitools.utils.cloud.neutron_client')
    @mock.patch('ccitools.utils.cloud.manila_client')
    @mock.patch('ccitools.utils.cloud.cinder_client')
    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_get_project_quota(self, mock_nova, mock_cinder, mock_manila,
                               mock_neutron, mock_rgw, mock_keystone,
                               mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        mock_kl = mock_keystone.Client.return_value
        mock_nt = mock_neutron.Client.return_value
        mock_nc = mock_nova.Client.return_value
        mock_ci = mock_cinder.Client.return_value
        mock_mc = mock_manila.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_kl.endpoint_groups.list.return_value = [
            base_fixtures.Resource(fixtures.ENDPOINT_GROUP_PRODUCTION),
            base_fixtures.Resource(fixtures.ENDPOINT_GROUP_SDN1),
            base_fixtures.Resource(fixtures.ENDPOINT_GROUP_S3)
        ]
        mock_ep = mock_kl.endpoint_filter
        mock_ep.list_endpoint_groups_for_project.return_value = [
            base_fixtures.Resource(fixtures.ENDPOINT_GROUP_PRODUCTION),
            base_fixtures.Resource(fixtures.ENDPOINT_GROUP_SDN1),
            base_fixtures.Resource(fixtures.ENDPOINT_GROUP_S3)
        ]
        mock_nt.show_quotas.return_value = fixtures.NEUTRON_QUOTA

        mock_nc.quotas.get.return_value = (
            base_fixtures.Resource(fixtures.NOVA_QUOTA_DETAIL)
        )
        mock_ci.quotas.get.return_value = (
            base_fixtures.Resource(fixtures.CINDER_QUOTA)
        )
        mock_mc.quotas.get.side_effect = iter([
            base_fixtures.Resource(fixtures.MANILA_QUOTA_GLOBAL_DETAIL),
            base_fixtures.Resource(fixtures.MANILA_QUOTA_PRODUCTION_DETAIL),
            base_fixtures.Resource(fixtures.MANILA_QUOTA_TESTING_DETAIL),
            base_fixtures.Resource(fixtures.MANILA_QUOTA_NON_PUBLIC_DETAIL)
        ])
        mock_mc.share_types.list.return_value = [
            base_fixtures.Resource(fixtures.SHARE_TYPE_PRODUCTION),
            base_fixtures.Resource(fixtures.SHARE_TYPE_TESTING),
            base_fixtures.Resource(fixtures.SHARE_TYPE_NON_PUBLIC)
        ]
        rgw_mock = mock_rgw.return_value
        project = base_fixtures.Resource(fixtures.PROJECT)

        rgw_mock.get_user.return_value = fixtures.S3_USER
        rgw_mock.get_quota.return_value = fixtures.S3_QUOTA

        client = CloudRegionClient()
        CloudRegionClient().get_project_quota(project_id=project.id)

        mock_neutron.Client.assert_called_with(
            session=client.session,
            region_name='sdn1'
        )
        mock_nt.show_quota.assert_called_with(
            project.id
        )

        mock_nova.Client.assert_called_with(
            version='2.56',
            session=client.session,
            region_name='cern'
        )
        mock_nc.quotas.get.assert_called_with(
            project.id,
            detail=True)
        mock_cinder.Client.assert_called_with(
            session=client.session,
            region_name='cern'
        )
        mock_ci.quotas.get.assert_called_with(project.id, usage=True)
        mock_manila.Client.assert_called_with(
            api_version=manila_api_versions.APIVersion('2.51'),
            session=client.session,
            region_name='cern'
        )
        mock_mc.quotas.get.assert_called_with(
            project.id,
            detail=True,
            share_type='non_public')
        rgw_mock.get_user.assert_called_with(
            uid=project.id,
            stats=True
        )
        rgw_mock.get_quota.assert_called_with(
            uid=project.id,
            quota_type='user'
        )
        rgw_mock.get_user.assert_called_with(
            uid=project.id,
            stats=True
        )

    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_set_nova_project_quota_empty_params(self, mock_nova,
                                                 mock_cc, mock_ks):
        mock_nc = mock_nova.Client.return_value

        client = CloudRegionClient()
        client.set_nova_project_quota(
            project_id='fake_id',
            cores=None,
            instances=None,
            ram=None,
            region='production')

        mock_nova.Client.assert_called_with(
            version='2.56',
            session=client.session,
            region_name='production'
        )
        mock_nc.quotas.update.assert_called_with(
            'fake_id')

    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_set_nova_project_quota(self, mock_nova,
                                    mock_cc, mock_ks):
        mock_nc = mock_nova.Client.return_value
        client = CloudRegionClient()
        client.set_nova_project_quota(
            project_id='fake_id',
            cores=1,
            instances=1,
            ram=1024,
            region='production')

        mock_nova.Client.assert_called_with(
            version='2.56',
            session=client.session,
            region_name='production'
        )
        mock_nc.quotas.update.assert_called_with(
            'fake_id',
            cores=1,
            instances=1,
            ram=1024)

    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_delete_nova_project_quota(self, mock_nova, mock_cc, mock_ks):
        mock_nc = mock_nova.Client.return_value
        client = CloudRegionClient()

        client.delete_nova_project_quota(
            project_id='fake_id',
            region='production')

        mock_nova.Client.assert_called_with(
            version='2.56',
            session=client.session,
            region_name='production'
        )
        mock_nc.quotas.delete.assert_called_with('fake_id')

    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_get_cinder_project_quota_no_available(self, mock_kc, mock_cc,
                                                   mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        client = CloudRegionClient()

        self.assertIsNone(client.get_cinder_project_quota('fake_id', 'fake'))

    @mock.patch('ccitools.utils.cloud.cinder_client')
    def test_delete_cinder_project_quota(self, mock_cinder, mock_cc, mock_ks):
        mock_cc = mock_cinder.Client.return_value
        client = CloudRegionClient()

        client.delete_cinder_project_quota(
            project_id='fake_id',
            region='cern')

        mock_cinder.Client.assert_called_with(
            session=client.session,
            region_name='cern'
        )
        mock_cc.quotas.delete.assert_called_with('fake_id')

    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_update_quota_cinder_no_available(self, mock_kc, mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        client = CloudRegionClient()

        client.update_quota_cinder(
            project_id='fake_id',
            volumes=0,
            gigabytes=0,
            snapshots=0,
            volume_type='standard',
            region='fake')

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.cinder_client')
    def test_update_quota_cinder_empty_params(self, mock_cinder, mock_kc,
                                              mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_ci = mock_cinder.Client.return_value
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        mock_cc.quotas.get.return_value = (
            base_fixtures.Resource(fixtures.CINDER_QUOTA)
        )

        client = CloudRegionClient()

        client.update_quota_cinder(
            project_id='fake_id',
            volumes=None,
            gigabytes=None,
            snapshots=None,
            volume_type='standard',
            region='cern')

        mock_cinder.Client.assert_called_with(
            session=client.session,
            region_name='cern'
        )

        mock_ci.quotas.get.assert_called_with('fake_id')

        mock_ci.quotas.update.assert_has_calls([
            mock.call('fake_id'),
            mock.call('fake_id', gigabytes=0, snapshots=0, volumes=0)
        ])

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.cinder_client')
    def test_update_quota_cinder(self, mock_cinder, mock_kc,
                                 mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_ci = mock_cinder.Client.return_value
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        mock_cc.quotas.get.return_value = (
            base_fixtures.Resource(fixtures.CINDER_QUOTA)
        )

        client = CloudRegionClient()

        client.update_quota_cinder(
            project_id='fake_id',
            volumes=1,
            gigabytes=1,
            snapshots=1,
            volume_type='standard',
            region='cern')

        mock_cinder.Client.assert_called_with(
            session=client.session,
            region_name='cern'
        )

        mock_ci.quotas.get.assert_called_with('fake_id')

        mock_ci.quotas.update.assert_has_calls([
            mock.call(
                'fake_id',
                gigabytes_standard=1,
                snapshots_standard=1,
                volumes_standard=1
            ),
            mock.call(
                'fake_id',
                gigabytes=0,
                snapshots=0,
                volumes=0
            )
        ])

    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_get_manila_project_quota_no_available(self, mock_kc,
                                                   mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        client = CloudRegionClient()

        self.assertEqual(
            client.get_manila_project_quota('fake_id', 'fake'),
            {}
        )

    @mock.patch('ccitools.utils.cloud.manila_client')
    def test_manila_project_quota(self, mock_manila, mock_cc, mock_ks):
        mock_mc = mock_manila.Client.return_value
        client = CloudRegionClient()

        client.delete_manila_project_quota(
            project_id='fake_id',
            region='production')

        mock_manila.Client.assert_called_with(
            api_version=manila_api_versions.APIVersion('2.51'),
            session=client.session,
            region_name='production'
        )
        mock_mc.quotas.delete.assert_called_with('fake_id')

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.manila_client')
    def test_update_quota_manila(self, mock_manila, mock_kc,
                                 mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_mc = mock_manila.Client.return_value
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        mock_mc.quotas.get.side_effect = iter([
            base_fixtures.Resource(fixtures.MANILA_QUOTA_GLOBAL),
            base_fixtures.Resource(fixtures.MANILA_QUOTA_PRODUCTION),
            base_fixtures.Resource(fixtures.MANILA_QUOTA_TESTING),
            base_fixtures.Resource(fixtures.MANILA_QUOTA_NON_PUBLIC),
            base_fixtures.Resource(fixtures.MANILA_QUOTA_GLOBAL),
            base_fixtures.Resource(fixtures.MANILA_QUOTA_PRODUCTION),
            base_fixtures.Resource(fixtures.MANILA_QUOTA_TESTING),
            base_fixtures.Resource(fixtures.MANILA_QUOTA_NON_PUBLIC),
            base_fixtures.Resource(fixtures.MANILA_QUOTA_GLOBAL),
            base_fixtures.Resource(fixtures.MANILA_QUOTA_PRODUCTION),
            base_fixtures.Resource(fixtures.MANILA_QUOTA_TESTING),
            base_fixtures.Resource(fixtures.MANILA_QUOTA_NON_PUBLIC)
        ])
        mock_mc.share_types.list.return_value = [
            base_fixtures.Resource(fixtures.SHARE_TYPE_PRODUCTION),
            base_fixtures.Resource(fixtures.SHARE_TYPE_TESTING),
            base_fixtures.Resource(fixtures.SHARE_TYPE_NON_PUBLIC)
        ]

        client = CloudRegionClient()

        client.update_quota_manila(
            project_id='fake_id',
            shares=1,
            gigabytes=1,
            share_type='production',
            region='cern')

        mock_manila.Client.assert_called_with(
            api_version=manila_api_versions.APIVersion('2.51'),
            session=client.session,
            region_name='cern'
        )

        mock_mc.quotas.get.assert_has_calls([
            mock.call('fake_id'),
            mock.call('fake_id', share_type='production'),
            mock.call('fake_id', share_type='testing'),
        ])

        mock_mc.quotas.update.assert_has_calls([
            mock.call(
                'fake_id',
                shares=1,
                gigabytes=1
            ),
            mock.call(
                'fake_id',
                gigabytes=1,
                shares=1,
                share_type='production'
            ),
            mock.call(
                'fake_id',
                gigabytes=0,
                shares=0
            )
        ])

    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_update_quota_manila_no_available(self, mock_kc,
                                              mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        client = CloudRegionClient()

        client.update_quota_manila(
            project_id='fake_id',
            shares=0,
            gigabytes=0,
            share_type='production',
            region='fake')

    @mock.patch('ccitools.utils.cloud.RadosGWAdminConnection')
    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_get_s3_project_quota_no_available(self, mock_kc, mock_rgw,
                                               mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        mock_ep = mock_kl.endpoint_filter
        mock_ep.list_endpoint_groups_for_project.return_value = []

        self.assertEqual(
            CloudRegionClient().get_s3_project_quota('fake_id', 'fake'),
            (0, 0)
        )

    @mock.patch('ccitools.utils.cloud.RadosGWAdminConnection')
    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_set_s3_project_quota_no_available(self, mock_kc, mock_rgw,
                                               mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        mock_kl.endpoint_groups.list.return_value = [
            base_fixtures.Resource(fixtures.ENDPOINT_GROUP_S3)
        ]
        client = CloudRegionClient()

        client.set_s3_project_quota(
            project_id='fake_id',
            containers=0,
            size_gb=0,
            region='fake')

    @mock.patch('ccitools.utils.cloud.RadosGWAdminConnection')
    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_set_s3_project_quota_empty(self, mock_kc, mock_rgw,
                                        mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        rgw_mock = mock_rgw.return_value
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        mock_kl.endpoint_groups.list.return_value = [
            base_fixtures.Resource(fixtures.ENDPOINT_GROUP_S3)
        ]
        mock_ep = mock_kl.endpoint_filter
        mock_ep.list_endpoint_groups_for_project.return_value = [
            base_fixtures.Resource(fixtures.ENDPOINT_GROUP_S3)
        ]

        client = CloudRegionClient()
        client.set_s3_project_quota(
            project_id='fake_id',
            containers=0,
            size_gb=0,
            region='production')

        rgw_mock.update_user.assert_called_with(
            uid='fake_id',
            max_buckets=0
        )
        rgw_mock.set_quota.assert_called_with(
            uid='fake_id',
            quota_type='user',
            enabled=True,
            max_size_kb=0
        )

    @mock.patch('ccitools.utils.cloud.RadosGWAdminConnection')
    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_set_s3_project_quota_project_with_tag(self, mock_kc, mock_rgw,
                                                   mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_kc.Client.return_value
        rgw_mock = mock_rgw.return_value
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        mock_cl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_ep = mock_cl.endpoint_filter
        mock_ep.list_endpoint_groups_for_project.return_value = [
            base_fixtures.Resource(fixtures.ENDPOINT_GROUP_S3)
        ]
        mock_cl.endpoint_groups.list.return_value = [
            base_fixtures.Resource(fixtures.ENDPOINT_GROUP_S3)
        ]
        mock_cl.projects.list_tags.return_value = ['s3quota']

        client = CloudRegionClient()
        client.set_s3_project_quota(
            project_id='fake_id',
            containers=1,
            size_gb=1,
            region='production')

        mock_kc.Client.assert_called_with(
            session=client.session,
            region_name='cern'
        )
        mock_cl.projects.list_tags.assert_called_with(
            'fake_id'
        )
        rgw_mock.update_user.assert_called_with(
            uid='fake_id',
            max_buckets=1
        )
        rgw_mock.set_quota.assert_called_with(
            uid='fake_id',
            quota_type='user',
            enabled=True,
            max_size_kb=1048576
        )

    @mock.patch('ccitools.utils.cloud.RadosGWAdminConnection')
    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.swift_client')
    def test_set_s3_project_quota_project_full(self, mock_sc, mock_kc,
                                               mock_rgw, mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_kc.Client.return_value
        rgw_mock = mock_rgw.return_value
        mock_auth_ref = session_mock.auth.get_auth_ref
        session_mock.auth.get_user_id.return_value = 'fake_user_id'
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        mock_cl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        eg = base_fixtures.Resource(fixtures.ENDPOINT_GROUP_S3)
        mock_ep = mock_cl.endpoint_filter
        mock_ep.list_endpoint_groups_for_project.return_value = [eg]
        mock_cl.endpoint_groups.list.return_value = [eg]
        mock_cl.projects.list_tags.return_value = ['s3quota']
        mock_cl.roles.list.return_value = [
            base_fixtures.Resource(fixtures.MEMBER_ROLE)
        ]
        rgw_mock.get_user.side_effect = iter([
            None,
            mock.MagicMock()
        ])

        client = CloudRegionClient()
        client.set_s3_project_quota(
            project_id='fake_id',
            containers=1,
            size_gb=1,
            region='production')

        mock_kc.Client.assert_called_with(
            session=client.session,
            region_name='cern'
        )
        mock_ep.add_endpoint_group_to_project.assert_called_with(
            endpoint_group=eg, project='fake_id')
        mock_cl.projects.list_tags.assert_called_with(
            'fake_id'
        )

        session_mock.auth.get_user_id.assert_called_with(
            session_mock
        )
        mock_cl.roles.list.assert_called_with(
            name='Member'
        )
        mock_cl.roles.grant.assert_called_with(
            'fake_role_id',
            user='fake_user_id',
            project='fake_id'
        )

        mock_sc.Connection.return_value.get_account.assert_called_with()

        mock_cl.roles.revoke.assert_called_with(
            'fake_role_id',
            user='fake_user_id',
            project='fake_id'
        )

        rgw_mock.get_user.assert_has_calls([
            mock.call(
                uid='fake_id',
                stats=False,
            ),
            mock.call(
                uid='fake_id',
                stats=False,
            ),
        ])

        rgw_mock.update_user.assert_called_with(
            uid='fake_id',
            max_buckets=1
        )
        rgw_mock.set_quota.assert_called_with(
            uid='fake_id',
            quota_type='user',
            enabled=True,
            max_size_kb=1048576
        )

    @mock.patch('ccitools.utils.cloud.RadosGWAdminConnection')
    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_set_s3_project_quota_project_no_tag(self, mock_kc, mock_rgw,
                                                 mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_kc.Client.return_value
        rgw_mock = mock_rgw.return_value
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        mock_cl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_ep = mock_cl.endpoint_filter
        mock_ep.list_endpoint_groups_for_project.return_value = [
            base_fixtures.Resource(fixtures.ENDPOINT_GROUP_S3)
        ]
        mock_cl.endpoint_groups.list.return_value = [
            base_fixtures.Resource(fixtures.ENDPOINT_GROUP_S3)
        ]
        mock_cl.projects.list_tags.return_value = []

        client = CloudRegionClient()
        client.set_s3_project_quota(
            project_id='fake_id',
            containers=1,
            size_gb=1,
            region='production')

        mock_kc.Client.assert_called_with(
            session=client.session,
            region_name='cern'
        )
        mock_cl.projects.list_tags.assert_called_with(
            'fake_id'
        )
        mock_cl.projects.add_tag.assert_called_with(
            'fake_id',
            's3quota'
        )
        rgw_mock.update_user.assert_called_with(
            uid='fake_id',
            max_buckets=1
        )
        rgw_mock.set_quota.assert_called_with(
            uid='fake_id',
            quota_type='user',
            enabled=True,
            max_size_kb=1048576
        )

    @mock.patch('ccitools.utils.cloud.RadosGWAdminConnection')
    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_set_s3_project_empty_quota_project_with_tag(self, mock_kc,
                                                         mock_rgw, mock_cc,
                                                         mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_kc.Client.return_value
        rgw_mock = mock_rgw.return_value
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        mock_cl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_ep = mock_cl.endpoint_filter
        mock_ep.list_endpoint_groups_for_project.return_value = [
            base_fixtures.Resource(fixtures.ENDPOINT_GROUP_S3)
        ]
        mock_cl.endpoint_groups.list.return_value = [
            base_fixtures.Resource(fixtures.ENDPOINT_GROUP_S3)
        ]
        mock_cl.projects.list_tags.return_value = ['s3quota']

        client = CloudRegionClient()
        client.set_s3_project_quota(
            project_id='fake_id',
            containers=0,
            size_gb=0,
            region='production')

        mock_kc.Client.assert_called_with(
            session=client.session,
            region_name='cern'
        )
        mock_cl.projects.list_tags.assert_called_with(
            'fake_id'
        )
        mock_cl.projects.delete_tag.assert_called_with(
            'fake_id',
            's3quota'
        )
        rgw_mock.update_user.assert_called_with(
            uid='fake_id',
            max_buckets=0
        )
        rgw_mock.set_quota.assert_called_with(
            uid='fake_id',
            quota_type='user',
            enabled=True,
            max_size_kb=0
        )

    @mock.patch('ccitools.utils.cloud.RadosGWAdminConnection')
    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_get_s3_user_wrong_region(self, mock_kc, mock_rgw,
                                      mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_kl = mock_kc.Client.return_value
        rgw_mock = mock_rgw.return_value
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        mock_kl.endpoint_groups.list.return_value = [
            base_fixtures.Resource(fixtures.ENDPOINT_GROUP_S3)
        ]

        client = CloudRegionClient()
        client.get_s3_user(
            project_id='fake_id',
            region='fake')

        rgw_mock.get_user.assert_not_called()

    @mock.patch('ccitools.utils.cloud.RadosGWAdminConnection')
    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_get_s3_user_raises_exception(self, mock_kc, mock_rgw,
                                          mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        rgw_mock = mock_rgw.return_value
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        mock_ep = mock_kl.endpoint_filter
        mock_ep.list_endpoint_groups_for_project.return_value = [
            base_fixtures.Resource(fixtures.ENDPOINT_GROUP_S3)
        ]
        rgw_mock.get_user.side_effect = iter(
            [NoSuchUser(status=404, reason='no such user')]
        )

        client = CloudRegionClient()
        self.assertIsNone(
            client.get_s3_user(
                project_id='fake_id',
                region='production'))

        rgw_mock.get_user.assert_called_with(
            uid='fake_id',
            stats=False
        )

    @mock.patch('ccitools.utils.cloud.RadosGWAdminConnection')
    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_get_s3_user(self, mock_kc, mock_rgw, mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        rgw_mock = mock_rgw.return_value
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        mock_ep = mock_kl.endpoint_filter
        mock_ep.list_endpoint_groups_for_project.return_value = [
            base_fixtures.Resource(fixtures.ENDPOINT_GROUP_S3)
        ]

        client = CloudRegionClient()
        client.get_s3_user(
            project_id='fake_id',
            region='production')

        rgw_mock.get_user.assert_called_with(
            uid='fake_id',
            stats=False
        )

    @mock.patch('ccitools.utils.cloud.RadosGWAdminConnection')
    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_get_s3_user_with_stats(self, mock_kc, mock_rgw,
                                    mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        rgw_mock = mock_rgw.return_value
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        mock_kl.endpoint_groups.list.return_value = [
            base_fixtures.Resource(fixtures.ENDPOINT_GROUP_S3)
        ]
        mock_ep = mock_kl.endpoint_filter
        mock_ep.list_endpoint_groups_for_project.return_value = [
            base_fixtures.Resource(fixtures.ENDPOINT_GROUP_S3)
        ]

        client = CloudRegionClient()
        client.get_s3_user(
            project_id='fake_id',
            region='production',
            stats=True)

        rgw_mock.get_user.assert_called_with(
            uid='fake_id',
            stats=True
        )

    @mock.patch('ccitools.utils.cloud.RadosGWAdminConnection')
    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_get_s3_project_usage(self, mock_kc, mock_rgw,
                                  mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        rgw_mock = mock_rgw.return_value
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        rgw_mock.get_user.return_value.stats.size_kb_utilized = 0
        mock_ep = mock_kl.endpoint_filter
        mock_ep.list_endpoint_groups_for_project.return_value = [
            base_fixtures.Resource(fixtures.ENDPOINT_GROUP_S3)
        ]

        client = CloudRegionClient()
        client.get_s3_project_usage(
            project_id='fake_id',
            region='production')

        rgw_mock.get_user.assert_called_with(
            uid='fake_id',
            stats=True
        )

    @mock.patch('ccitools.utils.cloud.RadosGWAdminConnection')
    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_get_s3_project_no_user(self, mock_kc, mock_rgw,
                                    mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        rgw_mock = mock_rgw.return_value
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        rgw_mock.get_user.return_value = None
        mock_ep = mock_kl.endpoint_filter
        mock_ep.list_endpoint_groups_for_project.return_value = [
            base_fixtures.Resource(fixtures.ENDPOINT_GROUP_S3)
        ]

        self.assertEqual(
            CloudRegionClient().get_s3_project_usage(
                project_id='fake_id',
                region='production'),
            (0, 0)
        )

        rgw_mock.get_user.assert_called_with(
            uid='fake_id',
            stats=True
        )

    @mock.patch('ccitools.utils.cloud.neutron_client')
    def test_delete_neutron_project_quota(self, mock_neutron,
                                          mock_cc, mock_ks):
        mock_nt = mock_neutron.Client.return_value
        client = CloudRegionClient()

        client.delete_neutron_project_quota(
            project_id='fake_id',
            region='production')

        mock_neutron.Client.assert_called_with(
            session=client.session,
            region_name='production'
        )
        mock_nt.delete_quota.assert_called_with('fake_id')

    def test_set_loadbalancer_quota_fail_for_regions(self, mock_cc, mock_ks):
        client = CloudRegionClient()
        self.assertRaises(Exception,
                          client.set_loadbalancer_quota,
                          project_id='fake_id',
                          loadbalancer=10,
                          region='batch')

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.neutron_client')
    def test_set_loadbalancer_quota(self, mock_neutron, mock_kc,
                                    mock_cc, mock_ks):
        mock_nt = mock_neutron.Client.return_value
        mock_cl = mock_kc.Client.return_value

        sdn1_eg = base_fixtures.Resource(fixtures.ENDPOINT_GROUP_SDN1)
        batch_eg = base_fixtures.Resource(fixtures.ENDPOINT_GROUP_BATCH)

        mock_cl.endpoint_groups.list.return_value = [sdn1_eg, batch_eg]
        mock_cl.endpoint_filter.check_endpoint_group_in_project \
            .return_value = True

        client = CloudRegionClient()

        region = 'sdn1'
        project_id = 'fake_id'
        loadbalancer = 10
        listener_count = loadbalancer * CONF.loadbalancer.listeners_per_lb
        member_count = loadbalancer * CONF.loadbalancer.members_per_lb

        client.set_loadbalancer_quota(
            project_id=project_id,
            loadbalancer=loadbalancer,
            region=region)

        mock_neutron.Client.assert_called_with(
            session=client.session,
            region_name=region)

        mock_nt.update_quota.assert_called_with(
            project_id,
            {
                'quota': {
                    'loadbalancer': loadbalancer,
                    'listener': listener_count,
                    'pool': listener_count,
                    'healthmonitor': listener_count,
                    'port': loadbalancer,
                    'member': member_count
                }
            }
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.neutron_client')
    def test_add_sdn1_endpoint_group_to_project_when_lb_quota_set(
        self, mock_neutron, mock_kc, mock_cc, mock_ks
    ):
        mock_cl = mock_kc.Client.return_value

        sdn1_eg = base_fixtures.Resource(fixtures.ENDPOINT_GROUP_SDN1)
        batch_eg = base_fixtures.Resource(fixtures.ENDPOINT_GROUP_BATCH)

        mock_cl.endpoint_groups.list.return_value = [sdn1_eg, batch_eg]

        client = CloudRegionClient()

        region = 'sdn1'
        project_id = 'fake_id'

        loadbalancer = 10
        mock_cl.endpoint_filter.check_endpoint_group_in_project \
            .return_value = False

        client.set_loadbalancer_quota(
            project_id=project_id,
            loadbalancer=loadbalancer,
            region=region)

        mock_cl.endpoint_filter.add_endpoint_group_to_project \
            .assert_called_with(
                endpoint_group=sdn1_eg, project=project_id)

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.neutron_client')
    def test_remove_sdn1_endpoint_group_from_project_when_lb_quota_is_zero(
        self, mock_neutron, mock_kc, mock_cc, mock_ks
    ):
        mock_cl = mock_kc.Client.return_value

        sdn1_eg = base_fixtures.Resource(fixtures.ENDPOINT_GROUP_SDN1)
        batch_eg = base_fixtures.Resource(fixtures.ENDPOINT_GROUP_BATCH)

        mock_cl.endpoint_groups.list.return_value = [sdn1_eg, batch_eg]

        client = CloudRegionClient()

        region = 'sdn1'
        project_id = 'fake_id'

        loadbalancer = 0

        client.set_loadbalancer_quota(
            project_id=project_id,
            loadbalancer=loadbalancer,
            region=region)

        mock_cl.endpoint_filter.delete_endpoint_group_from_project \
            .assert_called_with(endpoint_group=sdn1_eg, project=project_id)

    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_add_endpoint_group_raise_exception(self, mock_kc, mock_cc,
                                                mock_ks):
        mock_cl = mock_kc.Client.return_value
        mock_cl.endpoint_groups.list.return_value = []
        mock_ep = mock_cl.endpoint_filter

        with self.assertRaises(Exception):
            CloudRegionClient().addremove_endpointgroup(
                'non_existing',
                'fake_id',
                True
            )
        mock_ep.add_endpoint_group_to_project.assert_not_called()

    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_add_endpoint_group_already_existing(self, mock_kc, mock_cc,
                                                 mock_ks):
        mock_cl = mock_kc.Client.return_value
        eg = base_fixtures.Resource(fixtures.ENDPOINT_GROUP_S3)
        mock_cl.endpoint_groups.list.return_value = [eg]
        mock_ep = mock_cl.endpoint_filter
        mock_ep.add_endpoint_group_to_project.side_effect = iter(
            [keystone_exceptions.Conflict]
        )

        CloudRegionClient().addremove_endpointgroup(
            's3',
            'fake_id',
            True
        )
        mock_ep.add_endpoint_group_to_project.assert_called_with(
            endpoint_group=eg,
            project='fake_id'
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    def test_remove_endpoint_group_already_removed(self, mock_kc, mock_cc,
                                                   mock_ks):
        mock_cl = mock_kc.Client.return_value
        eg = base_fixtures.Resource(fixtures.ENDPOINT_GROUP_S3)
        mock_cl.endpoint_groups.list.return_value = [eg]
        mock_ep = mock_cl.endpoint_filter
        mock_ep.delete_endpoint_group_from_project.side_effect = iter(
            [keystone_exceptions.NotFound]
        )

        CloudRegionClient().addremove_endpointgroup(
            's3',
            'fake_id',
            False
        )
        mock_ep.delete_endpoint_group_from_project.assert_called_with(
            endpoint_group=eg,
            project='fake_id'
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_hypervisor_status_up(self, mock_nc, mock_kc,
                                  mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_nc.Client.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )

        mock_hv = mock.MagicMock()
        type(mock_hv)._info = mock.PropertyMock(
            return_value={"state": "up"})

        mock_cl.hypervisors.search.return_value = [mock_hv]

        CloudRegionClient().get_hypervisor_status('fake_hv')

        mock_nc.Client.assert_called_with(
            version='2.56',
            session=session_mock,
            region_name='cern')
        mock_cl.hypervisors.search.assert_called_with(
            'fake_hv',
            servers=False,
            detailed=False
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_hypervisor_status_down(self, mock_nc, mock_kc,
                                    mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_nc.Client.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )

        mock_hv = mock.MagicMock()
        type(mock_hv)._info = mock.PropertyMock(
            return_value={"state": "down"})

        mock_cl.hypervisors.search.return_value = [mock_hv]

        CloudRegionClient().get_hypervisor_status('fake_hv')

        mock_nc.Client.assert_called_with(
            version='2.56',
            session=session_mock,
            region_name='cern')
        mock_cl.hypervisors.search.assert_called_with(
            'fake_hv',
            servers=False,
            detailed=False
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_hypervisor_status_not_found(self, mock_nc, mock_kc,
                                         mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_nc.Client.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )

        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )

        mock_cl.hypervisors.search.side_effect = iter([
            nova_exceptions.NotFound(404)
        ])

        CloudRegionClient().get_hypervisor_status('fake_hv')

        mock_nc.Client.assert_called_with(
            version='2.56',
            session=session_mock,
            region_name='cern')
        mock_cl.hypervisors.search.assert_called_with(
            'fake_hv',
            servers=False,
            detailed=False
        )

    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_create_server(self, mock_nc, mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_cl = mock_nc.Client.return_value

        CloudRegionClient().create_server(
            name='fake_vm',
            image='image_id',
            flavor='flavor_id')

        mock_nc.Client.assert_called_with(
            version='2.56',
            session=session_mock,
            region_name='cern')

        mock_cl.servers.create.assert_called_with(
            name='fake_vm',
            image='image_id',
            flavor='flavor_id',
            block_device_mapping_v2=None,
            nics='auto',
            availability_zone=None,
            meta=None)

    @mock.patch('ccitools.utils.cloud.cinder_client')
    def test_transfer_volumes_empty_list(self, mock_ci, mock_cc, mock_ks):
        session_source = mock.MagicMock()
        session_target = mock.MagicMock()
        mock_ks.Session.side_effect = iter(
            [mock.MagicMock(), session_source, session_target]
        )

        CloudRegionClient().transfer_volumes(
            volumes=[],
            source_id='source_project_id',
            target_id='target_project_id'
        )

        mock_ci.Client.assert_has_calls([
            mock.call(
                session=session_source,
                region_name='cern',
            ),
            mock.call(
                session=session_target,
                region_name='cern',
            ),
        ])

    @mock.patch('ccitools.utils.cloud.cinder_client')
    def test_transfer_volumes(self, mock_ci, mock_cc, mock_ks):
        session_source = mock.MagicMock()
        session_target = mock.MagicMock()
        mock_ks.Session.side_effect = iter(
            [mock.MagicMock(), session_source, session_target]
        )
        mock_source = mock.MagicMock()

        mock_ci.Client.side_effect = iter(
            [mock_source, mock.MagicMock()]
        )

        CloudRegionClient().transfer_volumes(
            volumes=['first_id', 'second_id'],
            source_id='source_project_id',
            target_id='target_project_id'
        )

        mock_ci.Client.assert_has_calls([
            mock.call(
                session=session_source,
                region_name='cern',
            ),
            mock.call(
                session=session_target,
                region_name='cern',
            ),
        ])

        mock_source.transfers.create.assert_has_calls([
            mock.call('first_id'),
            mock.call('second_id'),
        ])

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.magnum_client')
    def test_get_clusters(self, mock_ma, mock_kc, mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_mc = mock_ma.Client.return_value

        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        mock_cluster = mock.MagicMock()
        type(mock_cluster).uuid = mock.PropertyMock(
            return_value='fake_uuid')
        mock_mc.clusters.list.return_value = [
            mock_cluster
        ]

        CloudRegionClient().get_clusters('fake_id')

        mock_ma.Client.assert_called_with(
            region_name='cern',
            session=session_mock
        )
        mock_mc.clusters.list.assert_called_with(
            detail=True
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.magnum_client')
    def test_delete_clusters_empty(self, mock_ma, mock_kc, mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_mc = mock_ma.Client.return_value

        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )

        CloudRegionClient().delete_clusters([])

        mock_mc.clusters.delete.assert_not_called()

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.magnum_client')
    def test_delete_clusters(self, mock_ma, mock_kc, mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value

        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )

        mock_cluster = mock.MagicMock()
        type(mock_cluster).uuid = mock.PropertyMock(
            return_value='fake_uuid')

        CloudRegionClient().delete_clusters([
            mock_cluster
        ])

        mock_cluster.manager.delete.assert_called_with(
            'fake_uuid'
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.magnum_client')
    def test_get_cluster_templates(self, mock_ma, mock_kc, mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_mc = mock_ma.Client.return_value

        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        mock_clustertemplate = mock.MagicMock()
        type(mock_clustertemplate).uuid = mock.PropertyMock(
            return_value='fake_uuid')
        mock_mc.cluster_templates.list.return_value = [
            mock_clustertemplate
        ]

        CloudRegionClient().get_cluster_templates('fake_id')

        mock_ma.Client.assert_called_with(
            region_name='cern',
            session=session_mock
        )
        mock_mc.cluster_templates.list.assert_called_with(
            detail=True
        )

    @mock.patch('ccitools.utils.cloud.magnum_client')
    def test_delete_cluster_templates_empty(self, mock_ma, mock_cc, mock_ks):
        mock_mc = mock_ma.Client.return_value

        CloudRegionClient().delete_cluster_templates([])

        mock_mc.cluster_templates.delete.assert_not_called()

    @mock.patch('ccitools.utils.cloud.magnum_client')
    def test_delete_cluster_templates(self, mock_ma, mock_cc, mock_ks):
        mock_clustertemplate = mock.MagicMock()
        type(mock_clustertemplate).uuid = mock.PropertyMock(
            return_value='fake_uuid')

        CloudRegionClient().delete_cluster_templates([
            mock_clustertemplate
        ])

        mock_clustertemplate.manager.delete.assert_called_with(
            'fake_uuid'
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.cinder_client')
    @mock.patch('ccitools.utils.cloud.manila_client')
    def test_merge_quota_metadata(self, mock_ma, mock_ci, mock_kc, mock_cc,
                                  mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_mc = mock_ma.Client.return_value
        mock_cl = mock_ci.Client.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_cl.volume_types.list.return_value = (
            fixtures.QUOTA_VOLUME_TYPES
        )
        mock_mc.share_types.list.return_value = (
            fixtures.QUOTA_SHARE_TYPES
        )
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )

        # Test empty quota and args
        self.assertEqual(
            fixtures.MERGE_EMPTY_QUOTA,
            CloudRegionClient().mergeQuotaMetadata(
                data=argparse.Namespace(),
                metadata={}
            )
        )
        # Test empty args and just empty quota dict
        self.assertEqual(
            fixtures.MERGE_EMPTY_QUOTA,
            CloudRegionClient().mergeQuotaMetadata(
                data=argparse.Namespace(),
                metadata=fixtures.MERGE_EMPTY_QUOTA
            )
        )
        # Test compute args and empty quota_dict
        self.assertEqual(
            fixtures.MERGE_QUOTA_COMPUTE,
            CloudRegionClient().mergeQuotaMetadata(
                data=argparse.Namespace(
                    cores=10,
                    instances=5,
                    ram=20
                ),
                metadata=fixtures.MERGE_EMPTY_QUOTA
            )
        )

        # Test compute and network dict and no args
        self.assertEqual(
            fixtures.MERGE_QUOTA_COMPUTE_AND_NETWORK,
            CloudRegionClient().mergeQuotaMetadata(
                data=argparse.Namespace(),
                metadata=fixtures.MERGE_QUOTA_COMPUTE_AND_NETWORK
            )
        )

        # Test compute and network dict args and empty quota dict
        self.assertEqual(
            fixtures.MERGE_QUOTA_COMPUTE_AND_NETWORK,
            CloudRegionClient().mergeQuotaMetadata(
                data=argparse.Namespace(
                    instances=5,
                    cores=10,
                    ram=20,
                    loadbalancer=2,
                    floatingip=4
                ),
                metadata=fixtures.MERGE_EMPTY_QUOTA
            )
        )

        # Test full_quota dict and no args
        self.assertEqual(
            fixtures.MERGE_FULL_QUOTA,
            CloudRegionClient().mergeQuotaMetadata(
                data=argparse.Namespace(),
                metadata=fixtures.MERGE_FULL_QUOTA
            )
        )

        # Test full arguments and empty quota_dict
        self.assertEqual(
            fixtures.MERGE_FULL_QUOTA,
            CloudRegionClient().mergeQuotaMetadata(
                data=argparse.Namespace(
                    instances=5,
                    cores=10,
                    ram=20,
                    standard_volumes=1,
                    standard_gigabytes=2,
                    io1_volumes=3,
                    io1_gigabytes=4,
                    cp1_volumes=5,
                    cp1_gigabytes=6,
                    cpio1_volumes=7,
                    cpio1_gigabytes=8,
                    vault_100_volumes=9,
                    vault_100_gigabytes=10,
                    vault_500_volumes=11,
                    vault_500_gigabytes=12,
                    geneva_testing_shares=1,
                    geneva_testing_gigabytes=2,
                    meyrin_shares=3,
                    meyrin_gigabytes=4,
                    s3_buckets=1,
                    s3_gigabytes=2,
                    loadbalancer=2,
                    floatingip=4

                ),
                metadata=fixtures.MERGE_EMPTY_QUOTA
            )
        )

        # Test full arguments that override the full one
        self.assertEqual(
            fixtures.MERGE_FULL_QUOTA_MODIFIED,
            CloudRegionClient().mergeQuotaMetadata(
                data=argparse.Namespace(
                    instances=10,
                    cores=20,
                    ram=40,
                    standard_volumes=2,
                    standard_gigabytes=4,
                    io1_volumes=6,
                    io1_gigabytes=8,
                    cp1_volumes=10,
                    cp1_gigabytes=12,
                    cpio1_volumes=14,
                    cpio1_gigabytes=16,
                    vault_100_volumes=18,
                    vault_100_gigabytes=20,
                    vault_500_volumes=22,
                    vault_500_gigabytes=24,
                    geneva_testing_shares=10,
                    geneva_testing_gigabytes=20,
                    meyrin_shares=30,
                    meyrin_gigabytes=40,
                    s3_buckets=100,
                    s3_gigabytes=200,
                    loadbalancer=4,
                    floatingip=8
                ),
                metadata=fixtures.MERGE_FULL_QUOTA
            )
        )

    def test_set_project_empty_quota(self, mock_cc, mock_ks):
        client = CloudRegionClient()
        client.set_project_quota(
            project_id='fake_id',
            quota={}
        )

    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_set_project_empty_compute_quota(self, mock_nova, mock_cc,
                                             mock_ks):
        mock_nc = mock_nova.Client.return_value

        client = CloudRegionClient()
        client.set_project_quota(
            project_id='fake_id',
            quota=fixtures.MERGE_QUOTA_COMPUTE['quota']
        )

        mock_nova.Client.assert_called_with(
            version='2.56',
            session=client.session,
            region_name='cern'
        )
        mock_nc.quotas.update.assert_called_with(
            'fake_id',
            cores=10,
            instances=5,
            ram=20480
        )

    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_set_project_compute_quota(self, mock_nova, mock_cc, mock_ks):
        mock_nc = mock_nova.Client.return_value

        client = CloudRegionClient()
        client.set_project_quota(
            project_id='fake_id',
            quota=fixtures.MERGE_QUOTA_COMPUTE['quota']
        )

        mock_nova.Client.assert_called_with(
            version='2.56',
            session=client.session,
            region_name='cern'
        )
        mock_nc.quotas.update.assert_called_with(
            'fake_id',
            cores=10,
            instances=5,
            ram=20480
        )

    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_set_project_instances_compute_quota(self, mock_nova, mock_cc,
                                                 mock_ks):
        mock_nc = mock_nova.Client.return_value

        client = CloudRegionClient()
        client.set_project_quota(
            project_id='fake_id',
            quota=fixtures.MERGE_QUOTA_INSTANCES_COMPUTE['quota']
        )

        mock_nova.Client.assert_called_with(
            version='2.56',
            session=client.session,
            region_name='cern'
        )
        mock_nc.quotas.update.assert_called_with(
            'fake_id',
            instances=5,
        )

    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_set_project_unlimited_ram_compute_quota(self, mock_nova, mock_cc,
                                                     mock_ks):
        mock_nc = mock_nova.Client.return_value

        client = CloudRegionClient()
        client.set_project_quota(
            project_id='fake_id',
            quota=fixtures.MERGE_QUOTA_UNLIMITED_RAM_COMPUTE['quota']
        )

        mock_nova.Client.assert_called_with(
            version='2.56',
            session=client.session,
            region_name='cern'
        )
        mock_nc.quotas.update.assert_called_with(
            'fake_id',
            ram=-1
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.RadosGWAdminConnection')
    def test_set_project_object_quota(self, mock_rgw, mock_keystone,
                                      mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        mock_kl = mock_keystone.Client.return_value
        rgw_mock = mock_rgw.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_kl.endpoint_groups.list.return_value = [
            base_fixtures.Resource(fixtures.ENDPOINT_GROUP_S3)
        ]
        mock_ep = mock_kl.endpoint_filter
        mock_ep.list_endpoint_groups_for_project.return_value = [
            base_fixtures.Resource(fixtures.ENDPOINT_GROUP_S3)
        ]

        client = CloudRegionClient()
        client.set_project_quota(
            project_id='fake_id',
            quota=fixtures.OBJECT_QUOTA['quota']
        )

        rgw_mock.update_user.assert_called_with(
            uid='fake_id',
            max_buckets=1
        )
        rgw_mock.set_quota.assert_called_with(
            uid='fake_id',
            quota_type='user',
            enabled=True,
            max_size_kb=2097152
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.RadosGWAdminConnection')
    @mock.patch('ccitools.utils.cloud.neutron_client')
    @mock.patch('ccitools.utils.cloud.manila_client')
    @mock.patch('ccitools.utils.cloud.cinder_client')
    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_set_project_full_quota(self, mock_nova, mock_cinder, mock_manila,
                                    mock_neutron, mock_rgw, mock_keystone,
                                    mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        mock_kl = mock_keystone.Client.return_value
        mock_nt = mock_neutron.Client.return_value
        mock_nc = mock_nova.Client.return_value
        mock_ci = mock_cinder.Client.return_value
        mock_mc = mock_manila.Client.return_value
        rgw_mock = mock_rgw.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_kl.endpoint_groups.list.return_value = [
            base_fixtures.Resource(fixtures.ENDPOINT_GROUP_S3),
            base_fixtures.Resource(fixtures.ENDPOINT_GROUP_SDN1)
        ]
        mock_ep = mock_kl.endpoint_filter
        mock_ep.list_endpoint_groups_for_project.return_value = [
            base_fixtures.Resource(fixtures.ENDPOINT_GROUP_S3),
            base_fixtures.Resource(fixtures.ENDPOINT_GROUP_SDN1)
        ]

        mock_mc.quotas.get.side_effect = iter([
            base_fixtures.Resource(fixtures.MANILA_QUOTA_GLOBAL),
            base_fixtures.Resource(fixtures.MANILA_QUOTA_PRODUCTION),
            base_fixtures.Resource(fixtures.MANILA_QUOTA_TESTING),
            base_fixtures.Resource(fixtures.MANILA_QUOTA_NON_PUBLIC),
            base_fixtures.Resource(fixtures.MANILA_QUOTA_GLOBAL),
            base_fixtures.Resource(fixtures.MANILA_QUOTA_PRODUCTION),
            base_fixtures.Resource(fixtures.MANILA_QUOTA_TESTING),
            base_fixtures.Resource(fixtures.MANILA_QUOTA_NON_PUBLIC),
            base_fixtures.Resource(fixtures.MANILA_QUOTA_GLOBAL),
            base_fixtures.Resource(fixtures.MANILA_QUOTA_PRODUCTION),
            base_fixtures.Resource(fixtures.MANILA_QUOTA_TESTING),
            base_fixtures.Resource(fixtures.MANILA_QUOTA_NON_PUBLIC)
        ])
        mock_mc.share_types.list.return_value = [
            base_fixtures.Resource(fixtures.SHARE_TYPE_PRODUCTION),
            base_fixtures.Resource(fixtures.SHARE_TYPE_TESTING),
            base_fixtures.Resource(fixtures.SHARE_TYPE_NON_PUBLIC)
        ]

        client = CloudRegionClient()
        client.set_project_quota(
            project_id='fake_id',
            quota=fixtures.PRODUCTION_QUOTA['quota']
        )

        mock_nova.Client.assert_called_with(
            version='2.56',
            session=client.session,
            region_name='cern'
        )
        mock_nc.quotas.update.assert_called_with(
            'fake_id',
            cores=10,
            instances=5,
            ram=20480
        )

        mock_cinder.Client.assert_called_with(
            session=client.session,
            region_name='cern'
        )

        mock_ci.quotas.get.assert_called_with('fake_id')

        mock_ci.quotas.update.assert_has_calls([
            mock.call(
                'fake_id',
                gigabytes_standard=2,
                snapshots_standard=-1,
                volumes_standard=1
            ),
            mock.call(
                'fake_id',
                gigabytes=0,
                snapshots=0,
                volumes=0
            )
        ])

        mock_manila.Client.assert_called_with(
            api_version=manila_api_versions.APIVersion('2.51'),
            session=client.session,
            region_name='cern'
        )

        mock_mc.quotas.get.assert_has_calls([
            mock.call('fake_id'),
            mock.call('fake_id', share_type='production'),
            mock.call('fake_id', share_type='testing'),
        ])

        mock_mc.quotas.update.assert_has_calls([
            mock.call(
                'fake_id',
                shares=1,
                gigabytes=2
            ),
            mock.call(
                'fake_id',
                gigabytes=2,
                shares=1,
                share_type='production'
            ),
            mock.call(
                'fake_id',
                gigabytes=0,
                shares=0
            )
        ])

        mock_neutron.Client.assert_called_with(
            session=client.session,
            region_name='sdn1')

        listener_count = 2 * CONF.loadbalancer.listeners_per_lb

        mock_nt.update_quota.assert_called_with(
            'fake_id',
            {
                'quota': {
                    'loadbalancer': 2,
                    'listener': listener_count,
                    'pool': listener_count,
                    'healthmonitor': listener_count,
                    'port': 2,
                    'member': 50
                }
            }
        )

        rgw_mock.update_user.assert_called_with(
            uid='fake_id',
            max_buckets=1
        )
        rgw_mock.set_quota.assert_called_with(
            uid='fake_id',
            quota_type='user',
            enabled=True,
            max_size_kb=2097152
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.cinder_client')
    @mock.patch('ccitools.utils.cloud.manila_client')
    def test_service_quota_iterator(self, mock_ma, mock_ci, mock_kc, mock_cc,
                                    mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_mc = mock_ma.Client.return_value
        mock_cl = mock_ci.Client.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_cl.volume_types.list.return_value = (
            fixtures.QUOTA_VOLUME_TYPES
        )
        mock_mc.share_types.list.return_value = (
            fixtures.QUOTA_SHARE_TYPES
        )
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )
        mock_callback = mock.MagicMock()

        CloudRegionClient().service_quota_iterator(mock_callback.callback)

        mock_callback.callback.assert_has_calls([
            mock.call(
                region='batch',
                service='compute',
                key='instances',
                resource=None),
            mock.call(
                region='batch',
                service='compute',
                key='cores',
                resource=None),
            mock.call(
                region='batch',
                service='compute',
                key='ram',
                resource=None),
            mock.call(
                region='cern',
                service='compute',
                key='instances',
                resource=None),
            mock.call(
                region='cern',
                service='compute',
                key='cores',
                resource=None),
            mock.call(
                region='cern',
                service='compute',
                key='ram',
                resource=None),
            mock.call(
                region='cern',
                service='blockstorage',
                key='volumes',
                resource='standard'),
            mock.call(
                region='cern',
                service='blockstorage',
                key='gigabytes',
                resource='standard'),
            mock.call(
                region='cern',
                service='blockstorage',
                key='volumes',
                resource='io1'),
            mock.call(
                region='cern',
                service='blockstorage',
                key='gigabytes',
                resource='io1'),
            mock.call(
                region='cern',
                service='blockstorage',
                key='volumes',
                resource='cp1'),
            mock.call(
                region='cern',
                service='blockstorage',
                key='gigabytes',
                resource='cp1'),
            mock.call(
                region='cern',
                service='blockstorage',
                key='volumes',
                resource='cpio1'),
            mock.call(
                region='cern',
                service='blockstorage',
                key='gigabytes',
                resource='cpio1'),
            mock.call(
                region='cern',
                service='blockstorage',
                key='volumes',
                resource='vault-100'),
            mock.call(
                region='cern',
                service='blockstorage',
                key='gigabytes',
                resource='vault-100'),
            mock.call(
                region='cern',
                service='blockstorage',
                key='volumes',
                resource='vault-500'),
            mock.call(
                region='cern',
                service='blockstorage',
                key='gigabytes',
                resource='vault-500'),
            mock.call(
                region='cern',
                service='fileshare',
                key='shares',
                resource='Geneva CephFS Testing'),
            mock.call(
                region='cern',
                service='fileshare',
                key='gigabytes',
                resource='Geneva CephFS Testing'),
            mock.call(
                region='cern',
                service='fileshare',
                key='shares',
                resource='Meyrin CephFS'),
            mock.call(
                region='cern',
                service='fileshare',
                key='gigabytes',
                resource='Meyrin CephFS'),
            mock.call(
                region='cern',
                service='object',
                key='buckets',
                resource=None),
            mock.call(
                region='cern',
                service='object',
                key='gigabytes',
                resource=None),
            mock.call(
                region='point8',
                service='compute',
                key='instances',
                resource=None),
            mock.call(
                region='point8',
                service='compute',
                key='cores',
                resource=None),
            mock.call(
                region='point8',
                service='compute',
                key='ram',
                resource=None),
            mock.call(
                region='sdn1',
                service='network',
                key='loadbalancers',
                resource=None),
            mock.call(
                region='sdn1',
                service='network',
                key='floating_ips',
                resource=None)
        ])

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.cinder_client')
    @mock.patch('ccitools.utils.cloud.manila_client')
    def test_filter_quota(self, mock_ma, mock_ci, mock_kc, mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_mc = mock_ma.Client.return_value
        mock_cl = mock_ci.Client.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_cl.volume_types.list.return_value = (
            fixtures.QUOTA_VOLUME_TYPES
        )
        mock_mc.share_types.list.return_value = (
            fixtures.QUOTA_SHARE_TYPES
        )
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )

        self.assertEqual(
            fixtures.CLOUD_FILTER_QUOTA['filtered'],
            CloudRegionClient().filter_quota(
                fixtures.CLOUD_FILTER_QUOTA['quota'],
                fixtures.CLOUD_FILTER_QUOTA['current'])
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.cinder_client')
    @mock.patch('ccitools.utils.cloud.manila_client')
    def test_filter_quota_single_region(self, mock_ma, mock_ci, mock_kc,
                                        mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_mc = mock_ma.Client.return_value
        mock_cl = mock_ci.Client.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_cl.volume_types.list.return_value = (
            fixtures.QUOTA_VOLUME_TYPES
        )
        mock_mc.share_types.list.return_value = (
            fixtures.QUOTA_SHARE_TYPES
        )
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )

        self.assertEqual(
            fixtures.FILTER_QUOTA_EMPTY['filtered'],
            CloudRegionClient().filter_quota(
                fixtures.FILTER_QUOTA_EMPTY['quota'],
                fixtures.FILTER_QUOTA_EMPTY['current'])
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.cinder_client')
    @mock.patch('ccitools.utils.cloud.manila_client')
    def test_filter_quota_missing_data(self, mock_ma, mock_ci, mock_kc,
                                       mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_mc = mock_ma.Client.return_value
        mock_cl = mock_ci.Client.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_cl.volume_types.list.return_value = (
            fixtures.QUOTA_VOLUME_TYPES
        )
        mock_mc.share_types.list.return_value = (
            fixtures.QUOTA_SHARE_TYPES
        )
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )

        self.assertEqual(
            fixtures.FILTER_QUOTA_MISSING_DATA['filtered'],
            CloudRegionClient().filter_quota(
                fixtures.FILTER_QUOTA_MISSING_DATA['quota'],
                fixtures.FILTER_QUOTA_MISSING_DATA['current'])
        )

    @mock.patch('ccitools.utils.cloud.keystone_client')
    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_get_hyperv_list(self, mock_nc, mock_kc, mock_cc, mock_ks):
        session_mock = mock_ks.Session.return_value
        mock_kl = mock_kc.Client.return_value
        mock_kl.regions.get.return_value = (
            fixtures.PRODUCTION_REGION
        )
        mock_cl = mock_nc.Client.return_value
        hv1 = base_fixtures.Resource(fixtures.HYPERVISOR_ONE)
        hv2 = base_fixtures.Resource(fixtures.HYPERVISOR_TWO)
        mock_cl.hypervisors.list.side_effect = iter([
            Exception(),
            [hv1, hv2]
        ])
        mock_auth_ref = session_mock.auth.get_auth_ref
        mock_auth_ref.return_value.service_catalog.catalog = (
            fixtures.CATALOG
        )

        client = CloudRegionClient()

        self.assertEqual(client.get_hypervisors_list(
            pattern='fake_hostname', limit=100), [hv1])
