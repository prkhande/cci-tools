import ldap
import ldap.sasl
import logging

from ccitools.errors import CciXldapNotFoundError
from ccitools.tests.utils import fixtures
from ccitools.utils.xldap import XldapClient
from unittest import mock
from unittest import TestCase


@mock.patch.object(ldap, 'initialize')
class TestXldap(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)
        self.client = XldapClient('localhost')

    @mock.patch.object(ldap.sasl, 'gssapi')
    def test_client_kerberized(self, mock_gss, mock_obj):
        mock_obj.return_value.sasl_interactive_bind_s = mock.Mock()
        mock_gss.return_value = mock.Mock()
        client = XldapClient('localhost', True)
        conn = client._init_connection()

        # mock_gss.assert_called_once()

        mock_obj.return_value.sasl_interactive_bind_s.assert_called_once_with(
            '',
            mock_gss.return_value
        )
        self.assertIsNotNone(conn)

    def test_get_vm_main_user(self, mock_obj):
        mock_obj.return_value.search_s = mock.Mock(
            return_value=fixtures.LDAP_SEARCH_VM_MAINUSER
        )

        user = self.client.get_vm_main_user('example')
        self.assertEqual(user, 'mainuser')

        user = self.client.get_vm_main_user('example.cern.ch')
        self.assertEqual(user, 'mainuser')

        mock_obj.return_value.search_s.assert_has_calls(
            [
                mock.call(
                    'DC=cern,DC=ch',
                    ldap.SCOPE_SUBTREE,
                    '(&(objectClass=computer)(cn=example))',
                    ['manager']),
                mock.call(
                    'DC=cern,DC=ch',
                    ldap.SCOPE_SUBTREE,
                    '(&(objectClass=computer)(cn=example))',
                    ['manager']),
            ]
        )

    def test_get_vm_main_user_not_found(self, mock_obj):
        mock_obj.return_value.search_s = mock.Mock(
            return_value=[]
        )

        self.assertRaises(
            CciXldapNotFoundError,
            self.client.get_vm_main_user,
            'example'
        )

    def test_get_vm_owner(self, mock_obj):
        mock_obj.return_value.search_s = mock.Mock(
            return_value=fixtures.LDAP_SEARCH_VM_OWNER
        )

        user = self.client.get_vm_owner('example')
        self.assertEqual(user, 'owner')

        user = self.client.get_vm_owner('example.cern.ch')
        self.assertEqual(user, 'owner')

        mock_obj.return_value.search_s.assert_has_calls(
            [
                mock.call(
                    'DC=cern,DC=ch',
                    ldap.SCOPE_SUBTREE,
                    '(&(objectClass=computer)(cn=example))',
                    ['managedBy']),
                mock.call(
                    'DC=cern,DC=ch',
                    ldap.SCOPE_SUBTREE,
                    '(&(objectClass=computer)(cn=example))',
                    ['managedBy']),
            ]
        )

    def test_get_vm_owner_not_found(self, mock_obj):
        mock_obj.return_value.search_s = mock.Mock(
            return_value=[]
        )

        self.assertRaises(
            CciXldapNotFoundError,
            self.client.get_vm_owner,
            'example'
        )

    def test_get_egroup_email(self, mock_obj):
        mock_obj.return_value.search_s = mock.Mock(
            return_value=fixtures.LDAP_SEARCH_GROUP_MAIL
        )

        mail = self.client.get_egroup_email('group')

        mock_obj.return_value.search_s.assert_called_once_with(
            'OU=e-groups,OU=Workgroups,DC=cern,DC=ch',
            ldap.SCOPE_SUBTREE,
            '(&(objectClass=group)(cn=group))',
            ['mail'],
        )
        self.assertEqual(mail, 'group@cern.ch')

    def test_get_egroup_email_not_found(self, mock_obj):
        mock_obj.return_value.search_s = mock.Mock(
            return_value=[]
        )

        self.assertRaises(
            CciXldapNotFoundError,
            self.client.get_egroup_email,
            'group'
        )

    def test_get_owner_primary_account(self, mock_obj):
        mock_obj.return_value.search_s = mock.Mock(
            return_value=fixtures.LDAP_SEARCH_PRIMARY
        )

        account = self.client.get_primary_account('primary')

        mock_obj.return_value.search_s.assert_called_once_with(
            'OU=Users,OU=Organic Units,DC=cern,DC=ch',
            ldap.SCOPE_SUBTREE,
            '(&(objectClass=user)(cn=primary))',
            ['cernAccountType', 'cernAccountOwner', 'sAMAccountName'],
        )
        self.assertEqual(account, 'primary')

    def test_get_owner_secondary_account(self, mock_obj):
        mock_obj.return_value.search_s = mock.Mock(
            return_value=fixtures.LDAP_SEARCH_SECONDARY
        )

        account = self.client.get_primary_account('secondary')

        mock_obj.return_value.search_s.assert_called_once_with(
            'OU=Users,OU=Organic Units,DC=cern,DC=ch',
            ldap.SCOPE_SUBTREE,
            '(&(objectClass=user)(cn=secondary))',
            ['cernAccountType', 'cernAccountOwner', 'sAMAccountName'],
        )
        self.assertEqual(account, 'primary')

    def test_get_owner_service_account(self, mock_obj):
        mock_obj.return_value.search_s = mock.Mock(
            return_value=fixtures.LDAP_SEARCH_SERVICE
        )

        account = self.client.get_primary_account('service')

        mock_obj.return_value.search_s.assert_called_once_with(
            'OU=Users,OU=Organic Units,DC=cern,DC=ch',
            ldap.SCOPE_SUBTREE,
            '(&(objectClass=user)(cn=service))',
            ['cernAccountType', 'cernAccountOwner', 'sAMAccountName'],
        )
        self.assertEqual(account, 'primary')

    def test_get_owner_account_not_found(self, mock_obj):
        mock_obj.return_value.search_s = mock.Mock(
            return_value=[]
        )

        self.assertRaises(
            CciXldapNotFoundError,
            self.client.get_primary_account,
            'non_existent'
        )

    def test_get_owner_account_fake(self, mock_obj):
        mock_obj.return_value.search_s = mock.Mock(
            return_value=fixtures.LDAP_SEARCH_FAKE
        )

        self.assertRaises(
            CciXldapNotFoundError,
            self.client.get_primary_account,
            'fake'
        )

    def test_get_user_email(self, mock_obj):
        mock_obj.return_value.search_s = mock.Mock(
            return_value=fixtures.LDAP_SEARCH_USER_MAIL
        )

        mail = self.client.get_user_email('user')

        mock_obj.return_value.search_s.assert_called_once_with(
            'OU=Users,OU=Organic Units,DC=cern,DC=ch',
            ldap.SCOPE_SUBTREE,
            '(&(objectClass=user)(cn=user))',
            ['mail'],
        )
        self.assertEqual(mail, 'user@cern.ch')

    def test_get_user_email_not_found(self, mock_obj):
        mock_obj.return_value.search_s = mock.Mock(
            return_value=[]
        )

        self.assertRaises(
            CciXldapNotFoundError,
            self.client.get_user_email,
            'non_existent'
        )

    def test_get_email_from_user(self, mock_obj):
        mock_obj.return_value.search_s = mock.Mock(
            return_value=fixtures.LDAP_SEARCH_USER_MAIL
        )

        mail = self.client.get_email('user')

        mock_obj.return_value.search_s.assert_has_calls(
            [
                mock.call(
                    'OU=Users,OU=Organic Units,DC=cern,DC=ch',
                    ldap.SCOPE_SUBTREE,
                    '(&(objectClass=user)(cn=user))',
                    None),
                mock.call(
                    'OU=Users,OU=Organic Units,DC=cern,DC=ch',
                    ldap.SCOPE_SUBTREE,
                    '(&(objectClass=user)(cn=user))',
                    ['mail'])
            ])

        self.assertEqual(mail, 'user@cern.ch')

    def test_get_email_from_group(self, mock_obj):
        mock_obj.return_value.search_s = mock.Mock()
        mock_obj.return_value.search_s.side_effect = ([
            [],  # Return empty in is_existing_user
            fixtures.LDAP_SEARCH_GROUP_MAIL,
            fixtures.LDAP_SEARCH_GROUP_MAIL,
        ])

        mail = self.client.get_email('group')

        mock_obj.return_value.search_s.assert_has_calls(
            [
                mock.call(
                    'OU=Users,OU=Organic Units,DC=cern,DC=ch',
                    ldap.SCOPE_SUBTREE,
                    '(&(objectClass=user)(cn=group))',
                    None),
                mock.call(
                    'OU=e-groups,OU=Workgroups,DC=cern,DC=ch',
                    ldap.SCOPE_SUBTREE,
                    '(&(objectClass=group)(cn=group))',
                    None),
                mock.call(
                    'OU=e-groups,OU=Workgroups,DC=cern,DC=ch',
                    ldap.SCOPE_SUBTREE,
                    '(&(objectClass=group)(cn=group))',
                    ['mail'])
            ])

        self.assertEqual(mail, 'group@cern.ch')

    def test_get_email_not_found(self, mock_obj):
        mock_obj.return_value.search_s = mock.Mock()
        mock_obj.return_value.search_s.side_effect = iter([
            [],  # Return empty in is_existing_user
            [],  # Return empty in is_existing_group
        ])

        self.assertRaises(
            CciXldapNotFoundError,
            self.client.get_email,
            'non_existent'
        )

    def test_is_existing_user(self, mock_obj):
        mock_obj.return_value.search_s = mock.Mock(
            return_value=fixtures.LDAP_SEARCH_USER_MAIL
        )

        retCode = self.client.is_existing_user('user')

        mock_obj.return_value.search_s.assert_called_once_with(
            'OU=Users,OU=Organic Units,DC=cern,DC=ch',
            ldap.SCOPE_SUBTREE,
            '(&(objectClass=user)(cn=user))',
            None,
        )
        self.assertTrue(retCode)

    def test_is_existing_user_not_found(self, mock_obj):
        mock_obj.return_value.search_s = mock.Mock(
            return_value=[]
        )

        self.assertFalse(self.client.is_existing_user('non_existent'))

    def test_is_existing_group(self, mock_obj):
        mock_obj.return_value.search_s = mock.Mock(
            return_value=fixtures.LDAP_SEARCH_GROUP_MAIL
        )

        retCode = self.client.is_existing_egroup('group')

        mock_obj.return_value.search_s.assert_called_once_with(
            'OU=e-groups,OU=Workgroups,DC=cern,DC=ch',
            ldap.SCOPE_SUBTREE,
            '(&(objectClass=group)(cn=group))',
            None,
        )
        self.assertTrue(retCode)

    def test_is_existing_group_not_found(self, mock_obj):
        mock_obj.return_value.search_s = mock.Mock(
            return_value=[]
        )

        self.assertFalse(self.client.is_existing_egroup('non_existent'))
