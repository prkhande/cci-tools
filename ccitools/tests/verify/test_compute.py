import logging

from ccitools.errors import SSHException
from ccitools.verify.compute import NetworkSpeedAction
from ccitools.verify.compute import RENEGOTIATE_SPEED_SCRIPT
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.verify.compute.ssh_executor')
@mock.patch('ccitools.verify.base.ssh_executor')
class TestNetworkSpeedAction(TestCase):
    def setUp(self):
        logging.disable(logging.CRITICAL)
        self.client = NetworkSpeedAction(host='fake.domain')
        self.client.delay = 0

    def test_try_fix(self, mock_common, mock_compute):
        self.client.try_fix()
        mock_compute.assert_called_with(
            'fake.domain',
            RENEGOTIATE_SPEED_SCRIPT
        )

    def test_try_fix_raise(self, mock_common, mock_compute):
        mock_compute.side_effect = iter([SSHException()])
        self.client.try_fix()

    def test_check_alarm_state(self, mock_common, mock_compute):
        mock_common.return_value = [], None
        self.assertFalse(self.client.check_alarm_state())

        mock_common.return_value = (
            ['fake.domain/another_alarm'], None)
        self.assertFalse(self.client.check_alarm_state())

        error_str = (
            'fake.domain/exec_compute/gauge-network_speed')
        mock_common.return_value = ([error_str], None)
        self.assertTrue(self.client.check_alarm_state())

        mock_common.return_value = [], None
        self.assertFalse(self.client.check_alarm_state(after_try=True))
