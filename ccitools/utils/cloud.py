import argparse
import json
import logging

from ccitools import conf
from cinderclient.v3 import client as cinder_client
from cornerstoneclient.clients.client_rest import ClientRest
from glanceclient.v2 import client as glance_client
from keystoneauth1 import session as keystone_session
from keystoneclient import exceptions as keystone_exceptions
from keystoneclient.v3 import client as keystone_client
from magnumclient import client as magnum_client
from manilaclient import api_versions as manila_api_versions
from manilaclient.v2 import client as manila_client
from neutronclient.common import exceptions as neutron_exceptions
from neutronclient.v2_0 import client as neutron_client
from novaclient import client as nova_client
from novaclient import exceptions as nova_exceptions
from os_client_config import config as cloud_config
from radosgw.connection import RadosGWAdminConnection
from radosgw.exception import NoSuchUser
from swiftclient import client as swift_client

# configure logging
logger = logging.getLogger(__name__)

CONF = conf.CONF
COMPUTE_API_VERSION = '2.56'
SHARE_API_VERSION = '2.51'


class CloudRegionClient(object):
    """Cloud client encapsulating the main operations in the OpenStack cloud.

    This class is designed as a facade to facilitate the interaction with
    all the components of the CERN OpenStack Cloud.
    """

    def __init__(self, cloud='cern', namespace=None, clients=True):
        """Initialize the facade to the Cloud operations.

        :param session: OS session to use in the clients
        """
        self.cloud = cloud
        self.session = self.get_session(
            cloud=self.cloud,
            namespace=namespace)
        if clients:
            self.cornerstone = ClientRest(
                kerberos=True,
                url=CONF.cornerstone.endpoint
            )
            self.rgw = RadosGWAdminConnection(
                host=CONF.s3.host,
                port=CONF.s3.port,
                access_key=CONF.s3.access_key,
                secret_key=CONF.s3.secret_key,
                aws_signature=CONF.s3.aws_signature,
                is_secure=CONF.s3.is_secure
            )

    def get_session(self, cloud, namespace=None):
        # Create session based on the arguments passed by parameter
        try:
            cloud_cfg = cloud_config.OpenStackConfig()
        except (IOError, OSError) as e:
            logger.critical("Could not read clouds.yaml configuration file")
            raise e

        cloud_obj = cloud_cfg.get_one_cloud(
            cloud=cloud,
            argparse=namespace)
        return keystone_session.Session(auth=cloud_obj.get_auth())

    def get_regions_per_service(self, service_type, session=None,
                                iface='public'):
        if not session:
            session = self.session
        # Retrieve the catalog from the session
        cat_obj = session.auth.get_auth_ref(session).service_catalog.catalog

        catalog = cat_obj['catalog'] if 'catalog' in cat_obj else cat_obj

        # Get the endpoints for a service type
        if catalog:
            endpoints = [entry['endpoints'] for entry in catalog
                         if entry['type'] == service_type].pop()
        else:
            endpoints = []

        # Retrieve the unique regions with the interface requested
        regions = list(set([endpoint['region'] for endpoint in endpoints
                            if endpoint['interface'] == iface]))
        # Return only the regions that are in production
        return [r for r in regions if self.is_production_region(r)]

    def get_hypervisors_list(self, pattern, limit=100, detailed=False):
        hypervisors = []
        for region in self.get_regions_per_service(service_type='compute'):
            nc = nova_client.Client(version=COMPUTE_API_VERSION,
                                    session=self.session,
                                    region_name=region)
            marker = None
            while True:
                try:
                    hv = nc.hypervisors.list(
                        detailed, marker=marker, limit=limit)

                    for srv in hv:
                        if pattern in srv.hypervisor_hostname:
                            hypervisors.append(srv)
                        marker = srv.id
                    if len(hv) < limit:
                        break
                except Exception:
                    logger.info("An error occurred at region %s", region)

        return hypervisors

    def is_service_available_in_region(self,
                                       service_type,
                                       region,
                                       session=None,
                                       iface='public'):
        return region in self.get_regions_per_service(
            service_type, session, iface)

    def is_production_region(self, region):
        # TODO(jcastro) Make keystone aware of regions,
        # Not a priority as Keystone is unique on the setup
        kc = keystone_client.Client(session=self.session,
                                    region_name='cern')
        return kc.regions.get(region).description in ['production']

    def get_servers_by_names(self, server_names):
        """List all the servers by server names.

        :param server_names
        """
        vms = []
        for server_name in server_names:
            vm = self.get_servers_by_name(server_name)
            if vm:
                vms.append(vm)
        return vms

    def get_servers_by_name(self, server_name):
        """Retrieve the server with the following name.

        :param server_name
        """
        if not server_name:
            return None

        server_name = server_name.replace(".cern.ch", "")
        vm_list = self.get_servers(instance_name=server_name)

        if not vm_list:
            logger.info("VM '%s' not found in any region", server_name)

        return vm_list.pop() if vm_list else None

    def get_hypervisor_status(self, hypervisor):
        # get status of the hypervisor
        # if the status is up then proceed
        for region in \
                self.get_regions_per_service(service_type='compute'):
            nc = nova_client.Client(version=COMPUTE_API_VERSION,
                                    session=self.session,
                                    region_name=region)
            try:
                match = nc.hypervisors.search(hypervisor,
                                              servers=False,
                                              detailed=False)
                hyper_uuid = match[0]
                if hyper_uuid._info["state"] == "up":
                    logger.info("Host state : %s", hyper_uuid._info["state"])
                    return "up"
                else:
                    logger.info("Hypervisor %s is down", hypervisor)
                    return "down"
            except nova_exceptions.NotFound:
                logger.info("Hypervisor %s not found in region %s",
                            hypervisor, region)

    def get_servers_by_hypervisors(self, hypervisors_names):
        """List all the servers running on the specified Hypervisors.

        :param hypervisor_names
        """
        vms = []
        for name in hypervisors_names:
            for vm in self.get_servers_by_hypervisor(name):
                vms.append(vm)
        return vms

    def get_servers_by_hypervisor(self, hypervisor_name):
        """List all the servers running on the specified Hypervisor.

        :param hypervisor_name
        """
        # we remove the .cern.ch part, because we need to check if it
        # goes with or without .cern.ch
        if not hypervisor_name:
            return []

        hypervisor = hypervisor_name.replace(".cern.ch", "")

        vm_list = self.get_servers(hypervisor=hypervisor)

        if not vm_list:
            vm_list = self.get_servers(hypervisor=hypervisor + ".cern.ch")
            if not vm_list:
                logger.error("Host '%s' does not have VMs in any region",
                             hypervisor)
        return vm_list

    def get_servers_by_projects(self, projects_names):
        """List all the servers running on the specified project.

        :param project_names
        """
        vms = []
        for project_name in projects_names:
            for vm in self.get_servers(project_name=project_name):
                vms.append(vm)
        return vms

    def get_servers(self, instance_name=None, project_name=None,
                    user_name=None, hypervisor=None, status=None):
        """List all servers that match any of the filters.

        :param instance_name: Name of the instance
        :param user_name: Name of OpenStack user
        :param project_name: OpenStack project where to search in
        :param host: name of the server where the machines are running
        :param status: status of the machine to search
        """
        vms = {}
        search_opts = {'all_tenants': True}

        if instance_name:
            search_opts['name'] = '^' + instance_name + '$'

        if project_name:
            project = self.find_project(project_name)
            search_opts['project_id'] = project.id

        if user_name:
            search_opts['user_id'] = user_name

        if hypervisor:
            search_opts['host'] = hypervisor

        if status:
            search_opts['status'] = status

        for region in self.get_regions_per_service(service_type='compute'):
            nc = nova_client.Client(version=COMPUTE_API_VERSION,
                                    session=self.session,
                                    region_name=region)
            try:
                for vm in nc.servers.list(search_opts=search_opts):
                    if vm.id not in vms:
                        vms[vm.id] = vm
            except nova_exceptions.NotFound:
                pass
        return list(vms.values())

    def get_server(self, instance_id):
        vm = None
        for region in self.get_regions_per_service(service_type='compute'):
            nc = nova_client.Client(version=COMPUTE_API_VERSION,
                                    session=self.session,
                                    region_name=region)
            try:
                vm = nc.servers.get(instance_id)
            except nova_exceptions.NotFound:
                pass
        return vm

    def get_nova_services_list(self, host=None, binary='nova-compute'):
        services = []
        for region in self.get_regions_per_service(service_type='compute'):
            nc = nova_client.Client(version=COMPUTE_API_VERSION,
                                    session=self.session,
                                    region_name=region)
            for srv in nc.services.list(host, binary):
                services.append(srv)
        return services

    def disable_nova_service(self, service, reason=None):
        """Disable a nova service with a possible reason.

        :param service: service to be disabled
        :param reason: reason to set in the log
        """
        if reason:
            service.manager.disable_log_reason(
                service.id,
                reason
            )
        else:
            service.manager.disable(service.id)

    def enable_nova_service(self, service):
        """Enable a nova service.

        :param service: service to be enabled
        """
        service.manager.enable(service.id)

    def delete_nova_service(self, service):
        """Delete a nova service.

        :param service: service to be removed
        """
        service.manager.delete(service.id)

    def get_nova_aggregates(self):
        aggregates = []
        for region in self.get_regions_per_service(service_type='compute'):
            nc = nova_client.Client(version=COMPUTE_API_VERSION,
                                    session=self.session,
                                    region_name=region)
            for aggr in nc.aggregates.list():
                aggregates.append(aggr)
        return aggregates

    def get_servers_by_last_action_user(self, servers, username):
        """Search for servers whose last action was done by user.

        :param servers: List of target servers
        :param username: User that performed the operation
        :returns: List of matched servers
        """
        matched_servers = []
        for server in servers:
            actions = server.manager.api.instance_action.list(server)
            if actions:
                if getattr(actions[0], 'user_id') == username:
                    matched_servers.append(server)

        return matched_servers

    def create_server(self, name, image, flavor, block_device_mapping_v2=None,
                      nics='auto', availability_zone=None, meta=None,
                      region='cern'):
        nc = nova_client.Client(version=COMPUTE_API_VERSION,
                                session=self.session,
                                region_name=region)
        nc.servers.create(
            name=name,
            image=image,
            flavor=flavor,
            block_device_mapping_v2=block_device_mapping_v2,
            nics=nics,
            availability_zone=availability_zone,
            meta=meta)

    def delete_servers(self, servers, force=False):
        """Delete the servers of a given list.

        : param servers: List of servers to be deleted.
        : param force: trigger force deletion if true.
        """
        for server in servers:
            try:
                # Reset the state of the server if needed
                if server.status not in ['active', 'error']:
                    logger.info("Reset to error '%s'", server.name)
                    server.reset_state(state='error')

                logger.info("Deleting server %s (created: %s ) ...",
                            server.name, server.created)
                if force:
                    server.force_delete()
                else:
                    server.delete()
            except Exception as ex:
                logger.error("Server %s could not be deleted.\n"
                             "Reason: %s", server.name, ex)
            else:
                logger.info("Done! Waiting for confirmation of deletion.")

    def reset_state_servers(self, servers, state='error'):
        """Reset the state on servers if they are on a transient state.

        : param servers: List of servers to be reset.
        : param state: state to be reset.
        """
        for server in servers:
            self.reset_state_server(server, state)

    def reset_state_server(self, server, state='error'):
        """Reset the state of a server if it is on a transient state.

        : param servers: Servers to be reset.
        : param state: state to be reset.
        """
        logger.debug("Checking state of server '%s'")
        if server.status in ['active', 'error']:
            logger.info("There is no need to reset the state of '%s'"
                        ", skipping...")
        else:
            try:
                server.reset_state(state=state)
            except Exception as ex:
                logger.error("Server %s could not be reset.\n"
                             "Reason: %s", server.name, ex)
            else:
                logger.info("Server '%s' has been reset to state '%s'",
                            server.name,
                            state)

    def power_on_servers(self, servers):
        """Power on servers of a given list.

        :param servers: List of servers to be started.
        """
        for server in servers:
            if getattr(server, 'OS-EXT-STS:vm_state') != 'active':
                logger.info("Power on server '%s' ...", server.name)
                server.start()
            else:
                logger.debug("Server '%s' is already active.", server.name)

    def power_off_servers(self, servers):
        """Power off servers of a given list.

        :param servers: List of servers to be stopped.
        """
        for server in servers:
            if getattr(server, 'OS-EXT-STS:vm_state') != 'stopped':
                logger.info("Power off server '%s' ...", server.name)
                server.stop()
            else:
                logger.debug("Server '%s' is already stopped.", server.name)

    def get_neutron_agents_list(self,
                                host,
                                binary='neutron-linuxbridge-agent'):
        agents = []
        for region in self.get_regions_per_service(service_type='network'):
            nc = neutron_client.Client(session=self.session,
                                       region_name=region)
            output = nc.list_agents(host, binary)
            if output and 'agents' in output:
                for agent in output['agents']:
                    # Append the region in the dictionary
                    agent['region_name'] = region
                    agents.append(agent)
        return agents

    def change_status_neutron_agent(self, agent, status=True, reason=''):
        """Disable a neutron agent with a possible reason.

        :param agent: agent to be disabled
        :param status: enable/disable agent
        :param reason: reason to set in the log
        """
        # We retrieve the region we've set in the agent_list
        region = agent['region']

        nc = neutron_client.Client(session=self.session,
                                   region_name=region)
        nc.update_agent(agent['id'],
                        description=reason,
                        is_admin_state_up=status,
                        admin_state_up=status)

    def delete_neutron_agent(self, agent):
        """Delete a neutron agent.

        :param agent: agent to be removed
        """
        # We retrieve the region we've set in the agent_list
        region = agent['region']
        nc = neutron_client.Client(session=self.session,
                                   region_name=region)

        nc.delete_agent(agent['id'])

    def get_volumes(self, user_name=None, project_id=None, project_name=None,
                    status=None):
        """List all volumes of the specified user and projet.

        :param user_name: Name of OpenStack user
        :param project_name: OpenStack project where to search in
        :param status: status of the machine to search
        """
        volumes = {}
        search_opts = {'all_tenants': True}

        if user_name:
            search_opts['user_id'] = user_name

        if status:
            search_opts['status'] = status

        if project_id:
            search_opts['project_id'] = project_id

        if project_name:
            project = self.find_project(project_name)
            search_opts['project_id'] = project.id

        for region in self.get_regions_per_service(service_type='volume'):
            cc = cinder_client.Client(session=self.session, region_name=region)
            for vol in cc.volumes.list(search_opts=search_opts):
                if vol.id not in volumes:
                    volumes[vol.id] = vol
        return list(volumes.values())

    def get_volume_snapshots(self, project_name=None, user_name=None,):
        """List all volumes snapshots of the specified user and project.

        :param user_name: Name of OpenStack user
        :param project_name: OpenStack project where to search in
        """
        snapshots = {}
        search_opts = {'all_tenants': True}

        if user_name:
            search_opts['user_id'] = user_name

        if project_name:
            project = self.find_project(project_name)
            search_opts['project_id'] = project.id

        for region in self.get_regions_per_service(service_type='volume'):
            cc = cinder_client.Client(session=self.session, region_name=region)
            for snap in cc.volume_snapshots.list(search_opts=search_opts):
                if snap.id not in snapshots:
                    snapshots[snap.id] = snap
        return list(snapshots.values())

    def delete_volumes(self, volumes_pending):
        """Delete volumes from a given list.

        :param volumes_pending: List of volumes to delete
        """
        for volume in volumes_pending:
            try:
                # Reset the state of the volume if needed
                if volume.status not in ['available', 'error']:
                    logger.info("Reset to error '%s'", volume.name)
                    volume.reset_state('error', attach_status='detached')

                logger.info("Deleting volume %s (created: %s ) ...",
                            volume.name, volume.created_at)
                volume.delete()
            except Exception as ex:
                logger.error("Volume %s could not be deleted.\n"
                             "Reason: %s", volume.name, ex)
            else:
                logger.info("Done! Waiting for confirmation of deletion.")

    def reset_state_volumes(self, volumes, state='error',
                            attach_status='detached'):
        """Reset the state on volumes if they are on a transient state.

        : param volumes: List of volumes to be reset.
        : param state: state to be reset.
        : param attach_status: attach_status to be reset.
        """
        for volume in volumes:
            self.reset_state_volume(volume, state, attach_status)

    def reset_state_volume(self, volume, state='error',
                           attach_status='detached'):
        """Reset the state of a volume if it is on a transient state.

        : param volume: Volume to be reset.
        : param state: state to be reset.
        : param attach_status: attach_status to be reset.
        """
        logger.debug("Checking state of volume '%s'")
        if volume.status in ['available', 'error']:
            logger.info("There is no need to reset the state of '%s'"
                        ", skipping...", volume.name)
        else:
            try:
                volume.reset_state(
                    state,
                    attach_status=attach_status)
            except Exception as ex:
                logger.error("Volume %s could not be reset.\n"
                             "Reason: %s", volume.name, ex)
            else:
                logger.info("Volume '%s' has been reset to state '%s'",
                            volume.name,
                            state)

    def create_volume(self, name, size, volume_type, image=None,
                      region='cern'):
        try:
            return cinder_client.Client(
                session=self.session,
                region_name=region).volumes.create(
                    size=size,
                    name=name,
                    volume_type=volume_type,
                    imageRef=image,
            )
        except Exception as ex:
            logger.error("Volume %s could not be created.\n"
                         "Reason: %s", name, ex)
            return None

    def create_volume_snapshot(self, volume, name, force=True):
        try:
            return volume.manager.api.volume_snapshots.create(
                volume_id=volume.id,
                force=force,
                name=name)
        except Exception as ex:
            logger.error("Volume %s could not be snapshotted.\n"
                         "Reason: %s", volume.name, ex)
            return None

    def get_image(self, id):
        """Get an image."""
        # TODO(jcastro) Make Glance aware of regions,
        # Not a priority as Glance is unique on the setup.
        gc = glance_client.Client(session=self.session,
                                  region_name='cern')
        return gc.images.get(id)

    def update_image(self, id, **kwargs):
        """Update the attributes of an image."""
        # TODO(jcastro) Make Glance aware of regions,
        # Not a priority as Glance is unique on the setup.
        gc = glance_client.Client(session=self.session,
                                  region_name='cern')
        return gc.images.update(id, kwargs)

    def get_images_by_name(self, name):
        """List all the images."""
        # TODO(jcastro) Make Glance aware of regions,
        # Not a priority as Glance is unique on the setup.
        images = []
        gc = glance_client.Client(session=self.session,
                                  region_name='cern')

        for image in gc.images.list(filters={'name': name.lower()}):
            images.append(image)
        return images

    def image_add_project(self, image_id, project_id):
        gc = glance_client.Client(session=self.session,
                                  region_name='cern')
        gc.image_members.create(image_id, project_id)
        gc.image_members.update(image_id, project_id, 'accepted')

    def get_images_by_project(self, project_name):
        """List all the instance images saved on the tenant.

        :param project_name
        """
        # TODO(jcastro) Make Glance aware of regions,
        # Not a priority as Glance is unique on the setup.
        images = []
        project = self.find_project(project_name)
        gc = glance_client.Client(session=self.session,
                                  region_name='cern')

        for image in gc.images.list(filters={'owner': project.id,
                                             'visibility': 'private'}):
            images.append(image)
        return images

    def get_shares_by_project(self, project_name):
        """List all the shares on the specified tenant.

        :param project_name
        """
        shares = {}
        search_opts = {'all_tenants': True}

        project = self.find_project(project_name)
        search_opts['project_id'] = project.id

        for region in self.get_regions_per_service(service_type='sharev2'):
            mc = manila_client.Client(
                api_version=manila_api_versions.APIVersion(SHARE_API_VERSION),
                session=self.session,
                region_name=region)

            for share in mc.shares.list(search_opts=search_opts):
                if share.id not in shares:
                    shares[share.id] = share
        return list(shares.values())

    def list_projects(self, domain=None, user=None, parent=None, **kwargs):
        """List projects."""
        # TODO(jcastro) Make keystone aware of regions,
        # Not a priority as Keystone is unique on the setup.
        kc = keystone_client.Client(session=self.session,
                                    region_name='cern')
        return kc.projects.list(
            domain=domain,
            user=user,
            parent=parent,
            **kwargs)

    def find_project(self, reference):
        """Find a project by name or ID.

        :param reference: either project name or ID
        """
        logger.debug("Looking for project '%s'...", reference)

        # TODO(jcastro) Make keystone aware of regions,
        # Not a priority as Keystone is unique on the setup.
        kc = keystone_client.Client(session=self.session,
                                    region_name='cern')
        try:
            return kc.projects.find(name=reference)
        except keystone_exceptions.NotFound:
            return kc.projects.get(reference)

    def get_project(self, project_id):
        """Get a project by ID.

        :param reference: either project name or ID
        """
        logger.debug("Looking for project '%s'...", project_id)

        # TODO(jcastro) Make keystone aware of regions,
        # Not a priority as Keystone is unique on the setup.
        kc = keystone_client.Client(session=self.session,
                                    region_name='cern')
        return kc.projects.get(project_id)

    def update_project(self, project, **kwargs):
        # TODO(jcastro) Make keystone aware of regions,
        # Not a priority as Keystone is unique on the setup.
        kc = keystone_client.Client(session=self.session,
                                    region_name='cern')
        return kc.projects.update(
            project=project,
            **kwargs)

    def get_project_members(self, project):
        """Return a list of all the members of a project.

        :param project
        """
        # TODO(jcastro) Make keystone aware of regions,
        # Not a priority as Keystone is unique on the setup.
        members = []
        logger.debug("Trying to get members of '%s'...", project.name)
        kc = keystone_client.Client(session=self.session,
                                    region_name='cern')

        role_assignments = kc.role_assignments.list(project=project)

        for role_assignment in role_assignments:
            if (hasattr(role_assignment, 'user')
                    and role_assignment.user['id'] not in members):

                members.append(role_assignment.user['id'])
                logger.debug("Found user '%s' as member of project '%s'",
                             role_assignment.user['id'], project.name)

            if (hasattr(role_assignment, 'group')
                    and role_assignment.group['id'] not in members):

                members.append(role_assignment.group['id'])
                logger.debug("Found egroup '%s' as member of project '%s'",
                             role_assignment.group['id'], project.name)
        return members

    def get_project_owner(self, project):
        """Return a the owner of a project.

        :param project
        """
        # TODO(jcastro) Make keystone aware of regions,
        # Not a priority as Keystone is unique on the setup.
        logger.debug("Trying to get owner of '%s'...", project.name)
        kc = keystone_client.Client(session=self.session,
                                    region_name='cern')
        for role_assignment in kc.role_assignments.list(project=project,
                                                        include_names=True):
            if role_assignment.role['name'] == 'owner':
                return role_assignment.user['id']
        return None

    def get_projects_by_role(self, user, rolename='owner'):
        """Return all projects owned by a user.

        :param owner
        """
        # TODO(jcastro) Make keystone aware of regions,
        # Not a priority as Keystone is unique on the setup.
        logger.debug("Trying to get all projects owned by '%s'...",
                     user)
        kc = keystone_client.Client(session=self.session,
                                    region_name='cern')

        roles = kc.roles.list(name=rolename)
        role = roles[0] if roles else None

        projects = []
        for role_assignment in kc.role_assignments.list(user=user,
                                                        role=role,
                                                        include_names=True):
            if role_assignment.role['name'] == rolename:
                projects.append(
                    kc.projects.get(
                        role_assignment.scope['project']['id'])
                )
        return projects

    def grant_role_in_project(self,
                              project_id,
                              role='Member',
                              user_id=None,
                              group_id=None):
        # TODO(jcastro) Make keystone aware of regions,
        # Not a priority as Keystone is unique on the setup.
        kc = keystone_client.Client(session=self.session,
                                    region_name='cern')
        roles = kc.roles.list(name=role)
        role_id = roles[0].id if roles else None

        kc.roles.grant(role_id,
                       user=user_id,
                       group=group_id,
                       project=project_id)

    def revoke_role_in_project(self,
                               project_id,
                               role='Member',
                               user_id=None,
                               group_id=None):
        # TODO(jcastro) Make keystone aware of regions,
        # Not a priority as Keystone is unique on the setup.
        kc = keystone_client.Client(session=self.session,
                                    region_name='cern')
        roles = kc.roles.list(name=role)
        role_id = roles[0].id if roles else None
        kc.roles.revoke(role_id,
                        user=user_id,
                        group=group_id,
                        project=project_id)

    def delete_project_fim_properties(self, project_id):
        self.cornerstone.project_properties_delete(project_id, "fim-skip")
        self.cornerstone.project_properties_delete(project_id, "fim-lock")

    def find_region_project(self, project):
        # TODO(jcastro) Make keystone aware of regions,
        # Not a priority as Keystone is unique on the setup.
        kc = keystone_client.Client(session=self.session,
                                    region_name='cern')

        regions = list(set(
            [eg.filters['region_id'] for eg in kc.endpoint_groups.list(
                project=project)])
        )
        # If more than one region is defined return cern as default
        return 'cern' if not regions or 'cern' in regions else regions.pop()

    def change_region_project(self, project, old_region, new_region):
        if old_region == new_region:
            return

        # TODO(jcastro) Make keystone aware of regions,
        # Not a priority as Keystone is unique on the setup.
        kc = keystone_client.Client(session=self.session,
                                    region_name='cern')

        # Calculate endpoint groups to be removed
        for eg in kc.endpoint_filter.list_endpoint_groups_for_project(
                project=project):
            if eg.filters['region_id'] == old_region:
                kc.endpoint_filter.delete_endpoint_group_from_project(
                    endpoint_group=eg,
                    project=project,
                )

        # Calculate endpoint groups to be added
        egps = [eg.id for eg in kc.endpoint_filter.
                list_endpoint_groups_for_project(project=project)]
        for eg in kc.endpoint_groups.list():
            if eg.filters['region_id'] == new_region and eg.id not in egps:
                kc.endpoint_filter.add_endpoint_group_to_project(
                    endpoint_group=eg,
                    project=project,
                )

    def is_group(self, group):
        # TODO(jcastro) Make keystone aware of regions,
        # Not a priority as Keystone is unique on the setup.
        try:
            kc = keystone_client.Client(session=self.session,
                                        region_name='cern')
            kc.groups.get(group)
        except keystone_exceptions.NotFound:
            logger.debug("Egroup '%s' not found in the system", group)
            return False
        return True

    def add_group_member(self, project, member):
        project_members = self.get_project_members(project)
        if member.lower() in (name.lower() for name in project_members):
            logger.info("Egroup '%s' already a member of '%s'",
                        member, project.name)
            return

        logger.info("Adding group '%s' as member of '%s'...",
                    member, project.name)
        r = self.cornerstone.add_project_members(project.id,
                                                 "Member",
                                                 member)
        status_code = str(r.status_code)
        if not status_code.startswith('2'):
            raise Exception(
                "Cornerstone failed adding egroup '%s', error: %s" %
                (member, json.loads(r.text)['property_value'][0]))

        logger.info("'%s'has been added to '%s'", member, project.name)

    def set_project_property(self, project, key, value):
        r = self.cornerstone.project_properties_add(project.id,
                                                    key,
                                                    value)
        status_code = str(r.status_code)
        if not status_code.startswith('2'):
            raise Exception(
                "Cornerstone failed setting '%s' property, error: %s" %
                (key, json.loads(r.text)['property_value'][0]))

    def get_nova_project_quota(self, project_id, region, session=None,
                               detail=False):
        """Retrieve the project quota in Nova.

        :param project_id: Project identifier
        :param region: Region to fetch the quota
        :param session: Session to use to retrieve the quota
        """
        if not session:
            session = self.session

        return nova_client.Client(
            version=COMPUTE_API_VERSION,
            session=session,
            region_name=region).quotas.get(project_id, detail=detail)

    def set_nova_project_quota(self, project_id, cores, instances, ram,
                               region):
        """Update Nova quota.

        :project_id: The project id that will be updated
        :cores: Number of cores
        :instances: Number of instances
        :ram: Amount of RAM memory
        :region: Region to apply the changes
        """
        updates = {}
        if cores is not None:
            updates["cores"] = cores
        if instances is not None:
            updates["instances"] = instances
        if ram is not None:
            updates["ram"] = ram

        nc = nova_client.Client(version=COMPUTE_API_VERSION,
                                session=self.session,
                                region_name=region)
        nc.quotas.update(project_id, **updates)
        logger.info("Nova quota updated: %s", updates)

    def delete_nova_project_quota(self, project_id, region):
        nc = nova_client.Client(version=COMPUTE_API_VERSION,
                                session=self.session,
                                region_name=region)
        nc.quotas.delete(project_id)

    def get_volume_types(self, region, session=None, show_all=False):
        """List all volume types."""
        if not session:
            session = self.session
        volume_types = {}
        is_public = None if show_all else True

        cc = cinder_client.Client(
            session=session,
            region_name=region)
        for volume_type in cc.volume_types.list(is_public=is_public):
            if volume_type.id not in volume_types and \
                    volume_type.name != '__DEFAULT__':
                volume_types[volume_type.id] = volume_type
        return list(volume_types.values())

    def get_cinder_project_quota(self, project_id, region, session=None,
                                 usage=False):
        """Retrieve the project quota in Cinder.

        :param project_id: Project identifier
        :param region: Region to fetch the quota
        :param session: Session to use to retrieve the quota
        :param usage: Retrieve usage details
        """
        if not session:
            session = self.session

        if not self.is_service_available_in_region(
                service_type='volume',
                region=region,
                session=session):
            logger.info(
                "No volume service found in region %s skipping quota",
                region)
            return None

        return cinder_client.Client(
            session=session,
            region_name=region).quotas.get(project_id, usage=usage)

    def delete_cinder_project_quota(self, project_id, region):
        cc = cinder_client.Client(session=self.session,
                                  region_name=region)
        cc.quotas.delete(project_id)

    def _keep_updated_global_quota_cinder(self, project_id, cinderclient):
        updates = {}
        cinder_quota = cinderclient.quotas.get(project_id)

        global_volumes = 0
        global_gigabytes = 0

        for key in cinder_quota._info:
            if cinder_quota._info[key] != -1:
                if 'volumes_' in key:
                    global_volumes += int(cinder_quota._info[key])
                if 'gigabytes_' in key:
                    global_gigabytes += int(cinder_quota._info[key])

        # the * 2 is to give space to people who backup their machines every
        # day (this space is to allow them to create a new snapshot without
        # deleting the one from the previous day
        global_snapshots = global_volumes * 2

        updates['volumes'] = global_volumes
        updates['gigabytes'] = global_gigabytes
        updates['snapshots'] = global_snapshots

        cinderclient.quotas.update(project_id, **updates)
        logger.info("Cinder global updated, volumes: %s gigabytes: %s "
                    "snapshots: %s", global_volumes, global_gigabytes,
                    global_snapshots)

    def update_quota_cinder(self, project_id, volumes, gigabytes, snapshots,
                            volume_type, region):
        """Update Cinder quota for a specific volume type.

        Only updates if the specified value and the current are different

        :project_id: the project id that will be updated.
        :volumes: number of volumes.
        :gigabytes: amount of gigabytes available
        :volume_type: Volume Type
        :region: Region to apply the changes
        """
        if not self.is_service_available_in_region('volume', region):
            logger.info(
                "No volume service found in region %s skipping quota",
                region)
            return

        updates = {}
        if volumes is not None:
            updates['volumes_%s' % volume_type] = volumes
        if gigabytes is not None:
            updates['gigabytes_%s' % volume_type] = gigabytes
        if snapshots is not None:
            updates['snapshots_%s' % volume_type] = snapshots

        cc = cinder_client.Client(
            session=self.session,
            region_name=region)

        cc.quotas.update(project_id, **updates)
        logger.info("Cinder quota updated: %s", updates)
        self._keep_updated_global_quota_cinder(project_id, cc)
        logger.debug("Cinder global quota updated.")

    def get_share_types(self, region, session=None, show_all=False):
        """List all share types."""
        if not session:
            session = self.session
        share_types = {}
        mc = manila_client.Client(
            api_version=manila_api_versions.APIVersion(SHARE_API_VERSION),
            session=session,
            region_name=region)
        for share_type in mc.share_types.list(show_all=show_all):
            if share_type.id not in share_types:
                share_types[share_type.id] = share_type
        return list(share_types.values())

    def get_manila_project_quota(self, project_id, region, session=None,
                                 detail=False):
        """Retrieve the project quota in Manila.

        :param project_id: Project identifier
        :param region: Region to fetch the quota
        :param session: Session to use to retrieve the quota
        :param detail: Retrieve usage details
        """
        if not session:
            session = self.session
        quotas = {}

        if not self.is_service_available_in_region(service_type='sharev2',
                                                   region=region,
                                                   session=session):
            logger.info(
                "No share service found in region %s skipping quota",
                region)
            return quotas
        mc = manila_client.Client(
            api_version=manila_api_versions.APIVersion(SHARE_API_VERSION),
            session=session,
            region_name=region)

        quotas['global'] = mc.quotas.get(project_id, detail=detail)

        for share_type in mc.share_types.list():
            quotas[share_type.name] = mc.quotas.get(
                project_id,
                share_type=share_type.name, detail=detail)
        return quotas

    def delete_manila_project_quota(self, project_id, region):
        mc = manila_client.Client(
            api_version=manila_api_versions.APIVersion(SHARE_API_VERSION),
            session=self.session,
            region_name=region)
        mc.quotas.delete(project_id)

    def _keep_updated_global_quota_manila(self, project_id, manilaclient):
        """Helper function to update global quota of manila.

        :args project_id: The project that the quota needs to be updated
        :args manilaclient: manilaclient to do api calls
        """
        updates = {}
        global_shares = 0
        global_gigabytes = 0

        quota = manilaclient.quotas.get(project_id)

        for share_type in manilaclient.share_types.list():
            quota = manilaclient.quotas.get(
                project_id, share_type=share_type.name)
            global_shares += int(quota.shares)
            global_gigabytes += int(quota.gigabytes)

        updates['shares'] = global_shares
        updates['gigabytes'] = global_gigabytes

        manilaclient.quotas.update(project_id, **updates)
        logger.info("manila global updated, shares: %s gigabytes: %s ",
                    global_shares, global_gigabytes)

    def update_quota_manila(self, project_id, shares, gigabytes,
                            share_type, region):
        """Update Manila quota for a specific share type.

        :project_id: the project id that will be updated.
        :shares: number of shares.
        :gigabytes: amount of gigabytes available
        :share_type: share Type
        :region: Region to apply the changes
        """
        if not self.is_service_available_in_region('sharev2', region):
            logger.info(
                "No share service found in region %s skipping quota",
                region)
            return

        updates = {}

        if shares is not None:
            updates['shares'] = shares
        if gigabytes is not None:
            updates['gigabytes'] = gigabytes

        mc = manila_client.Client(
            api_version=manila_api_versions.APIVersion(SHARE_API_VERSION),
            session=self.session,
            region_name=region)

        # HACK: ensure we always have enough quota adding the values
        # we want to set. If you try to set a share quota that is bigger
        # than the global one, it fails with BadRequest
        quota = self.get_manila_project_quota(project_id, region)

        mc.quotas.update(
            project_id,
            shares=(quota['global'].shares + shares if shares
                    else quota['global'].shares),
            gigabytes=(quota['global'].gigabytes + gigabytes if gigabytes
                       else quota['global'].gigabytes))

        # set the requested quota
        mc.quotas.update(project_id, share_type=share_type, **updates)
        logger.info("Manila quota updated (%s): %s", share_type, updates)

        # we adjust the quota to the current values
        self._keep_updated_global_quota_manila(project_id, mc)
        logger.debug("Manila global quota updated.")

    def delete_neutron_project_quota(self, project_id, region):
        nc = neutron_client.Client(session=self.session,
                                   region_name=region)
        nc.delete_quota(project_id)

    def get_neutron_project_quota(self, project_id, region, session=None):
        """Retrieve the project quota in Neutron.

        :param project_id: Project identifier
        :param region: Region to fetch the quota
        :param session: Session to use to retrieve the quota
        """
        if not session:
            session = self.session
        nc = neutron_client.Client(session=session,
                                   region_name=region)
        return nc.show_quota(project_id)['quota']

    def addremove_endpointgroup(self, name, project_id, condition):
        # TODO(jcastro) Make keystone aware of regions,
        # Not a priority as Keystone is unique on the setup.
        kc = keystone_client.Client(session=self.session,
                                    region_name='cern')

        endpoint_group = next((eg for eg in kc.endpoint_groups.list()
                               if eg.name == name), None)

        if not endpoint_group:
            raise Exception("endpoint %s group not found", name)

        if condition:
            try:
                # add endpoint group project association
                kc.endpoint_filter.add_endpoint_group_to_project(
                    endpoint_group=endpoint_group, project=project_id)
            except keystone_exceptions.Conflict:
                logger.info("%s endpoint group association already exists",
                            name)
        else:
            try:
                # remove endpoint group project association
                kc.endpoint_filter.delete_endpoint_group_from_project(
                    endpoint_group=endpoint_group, project=project_id)
            except keystone_exceptions.NotFound:
                logger.info("%s endpoint group association doesn't exist",
                            name)

    def check_endpoint_group(self, name, project_id):
        # TODO(jcastro) Make keystone aware of regions,
        # Not a priority as Keystone is unique on the setup.
        kc = keystone_client.Client(session=self.session,
                                    region_name='cern')

        endpoint_group = next(
            (eg for eg in kc.endpoint_filter.list_endpoint_groups_for_project(
                project_id) if eg.name == name), None)

        return endpoint_group

    def addremove_project_tag(self, name, project_id, condition):
        # TODO(jcastro) Make keystone aware of regions,
        # Not a priority as Keystone is unique on the setup.
        kc = keystone_client.Client(session=self.session,
                                    region_name='cern')
        project_tags = kc.projects.list_tags(project_id)

        if condition and name not in project_tags:
            kc.projects.add_tag(project_id, name)
        elif not condition and name in project_tags:
            kc.projects.delete_tag(project_id, name)

    def get_loadbalancer_quota(self, project_id, region, session=None):
        if not self.check_endpoint_group('sdn1', project_id):
            logger.critical(
                "SDN1 is not enabled for the project %s skipping",
                project_id)
            return {'loadbalancer': 0, 'floatingip': 0}
        else:
            try:
                return self.get_neutron_project_quota(
                    project_id=project_id,
                    session=session,
                    region=region)
            except neutron_exceptions.NotFound:
                return {'loadbalancer': 0, 'floatingip': 0}

    def set_loadbalancer_quota(self, project_id, loadbalancer, region):
        if region != "sdn1":
            raise Exception(
                "Can't set load balancer quotas for region: %s" % region)

        self.addremove_endpointgroup(
            name='sdn1',
            project_id=project_id,
            condition=loadbalancer > 0
        )

        self.addremove_project_tag(
            name='sdn1quota',
            project_id=project_id,
            condition=loadbalancer > 0
        )

        nc = neutron_client.Client(session=self.session,
                                   region_name=region)
        updates = {}
        updates['loadbalancer'] = loadbalancer

        listener_count = loadbalancer * CONF.loadbalancer.listeners_per_lb
        member_count = loadbalancer * CONF.loadbalancer.members_per_lb

        updates['listener'] = listener_count

        # one pool per listener
        updates['pool'] = listener_count

        # one healthmonitor per listener
        updates['healthmonitor'] = listener_count

        updates['member'] = member_count
        updates['port'] = loadbalancer

        nc.update_quota(project_id, {'quota': updates})
        logger.info("load balancer quota updated (%s): %s",
                    project_id, updates)

    def get_s3_project_quota(self, project_id, region):
        """Retrieve the project quota in S3.

        :param id: Project identifier
        """
        if not self.check_endpoint_group('s3', project_id):
            logger.info(
                "S3 is not enabled for the project %s skipping",
                project_id)
            return 0, 0

        buckets = self.rgw.get_user(uid=project_id,
                                    stats=True).max_buckets

        quota_user = self.rgw.get_quota(uid=project_id,
                                        quota_type='user')
        if isinstance(quota_user, str):
            quota_user = json.loads(quota_user)

        size_kb = quota_user['max_size_kb']

        return buckets, int(size_kb / 1048576)

    def get_s3_user(self, project_id, region, stats=False):
        """Get S3 user for project.

        :param id: Project identifier
        """
        if not self.check_endpoint_group('s3', project_id):
            logger.info(
                "S3 is not enabled for the project %s skipping",
                project_id)
            return None
        try:
            return self.rgw.get_user(uid=project_id, stats=stats)
        except NoSuchUser:
            return None

    def get_s3_project_usage(self, project_id, region):
        user = self.get_s3_user(project_id, region, stats=True)
        if user:
            return (
                len(list(user.get_buckets())),
                round(user.stats.size_kb_actual / 1048576))
        else:
            return (0, 0)

    def create_s3_user_in_radosgw(self, project_id, region):
        # TODO(jcastro) Make keystone aware of regions,
        # Not a priority as Keystone is unique on the setup.
        kc = keystone_client.Client(session=self.session,
                                    region_name='cern')
        user_id = self.session.auth.get_user_id(self.session)
        roles = kc.roles.list(name='Member')
        role_id = roles[0].id if roles else None

        # Enable the project to be able to trigger creation of s3 account
        kc.projects.update(project_id, enabled=True)

        # Add temporary role to account
        kc.roles.grant(role_id, user=user_id, project=project_id)

        # retrieve temporary session
        temp_session = self.get_session(
            cloud=self.cloud,
            namespace=argparse.Namespace(project_id=project_id)
        )
        # Do query to create account in radosgw
        swift_client.Connection(session=temp_session).get_account()

        # Disable the project once the call was done
        kc.projects.update(project_id, enabled=False)

        # Remove temporary role in account
        kc.roles.revoke(role_id, user=user_id, project=project_id)

    def set_s3_project_quota(self, project_id, containers, size_gb, region):
        """Set the project quota in s3.

        :param projectid: Project identifier
        :param containers: Maximum number of containers allowed
        :param size_kb: Maximum size allowed.
        """
        size_kb = int(size_gb * 1048576)
        condition = (containers > 1 or size_kb > 1)

        self.addremove_endpointgroup(
            name='s3',
            project_id=project_id,
            condition=condition
        )

        self.addremove_project_tag(
            name='s3quota',
            project_id=project_id,
            condition=condition
        )

        user = self.get_s3_user(project_id, region)

        if condition and not user:
            self.create_s3_user_in_radosgw(project_id, region)
            user = self.get_s3_user(project_id, region)

        if user:
            self.rgw.update_user(uid=project_id,
                                 max_buckets=containers)

            self.rgw.set_quota(uid=project_id,
                               quota_type='user',
                               enabled=True,
                               max_size_kb=size_kb)

            logger.info("S3 quota updated: %s" % {
                'containers': containers,
                'size_gb': size_gb
            })
        else:
            logger.info(
                "No need to update S3 quota for the project %s skipping",
                project_id)

    def transfer_volumes(self, volumes, source_id, target_id, region='cern'):
        cinder_source = cinder_client.Client(
            region_name=region,
            session=self.get_session(
                self.cloud,
                namespace=argparse.Namespace(project_id=source_id)))
        cinder_target = cinder_client.Client(
            region_name=region,
            session=self.get_session(
                self.cloud,
                namespace=argparse.Namespace(project_id=target_id)))

        for volume_id in volumes:
            transfer_request = cinder_source.transfers.create(volume_id)
            cinder_target.transfers.accept(
                transfer_request.id,
                transfer_request.auth_key
            )

    def get_clusters(self, project_id):
        """List all Magnum clusters in a given project.

        :param project_id: OpenStack project to search in
        """
        clusters = []
        namespace = argparse.Namespace(project_id=project_id)
        session = self.get_session(self.cloud, namespace=namespace)

        for region in self.get_regions_per_service(
                session=session,
                service_type='container-infra'):
            mc = magnum_client.Client(session=session,
                                      region_name=region)
            for cluster in mc.clusters.list(detail=True):
                clusters.append(cluster)
        return clusters

    def delete_clusters(self, clusters):
        """Delete a set of clusters.

        :param project_id: OpenStack project to search in
        :param clusters: List of clusters to delete
        """
        for cluster in clusters:
            cluster.manager.delete(cluster.uuid)

    def get_cluster_templates(self, project_id):
        """List all Magnum cluster templates in a given project.

        :param project_id: OpenStack project to search in
        """
        templates = []
        namespace = argparse.Namespace(project_id=project_id)
        session = self.get_session(self.cloud, namespace=namespace)

        for region in self.get_regions_per_service(
                session=session,
                service_type='container-infra'):
            mc = magnum_client.Client(session=session,
                                      region_name=region)
            for template in mc.cluster_templates.list(detail=True):
                templates.append(template)
        return templates

    def delete_cluster_templates(self, templates):
        """Delete Magnum cluster templates in a given project.

        :param project_id: OpenStack project to search in
        :param templates: List of cluster templates to delete
        """
        for template in templates:
            template.manager.delete(template.uuid)

    def get_regions_and_services(self, show_all=False):
        return {
            "batch": [
                {
                    'name': 'compute',
                    'items': {
                        'instances': '',
                        'cores': '',
                        'ram': ''
                    }
                },
            ],
            "cern": [
                {
                    'name': 'compute',
                    'items': {
                        'instances': 'instances',
                        'cores': 'cores',
                        'ram': 'ram'
                    }
                },
                {
                    'name': 'blockstorage',
                    'types': {
                        v.name: v.description if v.description else v.name
                        for v in self.get_volume_types(
                            region='cern',
                            show_all=show_all)
                    },
                    'items': {
                        'volumes': '%s_volumes',
                        'gigabytes': '%s_gigabytes'
                    }
                },
                {
                    'name': 'fileshare',
                    'types': {
                        s.name: s.description if s.description else s.name
                        for s in self.get_share_types(
                            region='cern',
                            show_all=show_all)
                    },
                    'items': {
                        'shares': '%s_shares',
                        'gigabytes': '%s_gigabytes'
                    }
                },
                {
                    'name': 'object',
                    'items': {
                        'buckets': 's3_buckets',
                        'gigabytes': 's3_gigabytes'
                    }
                }
            ],
            "point8": [
                {
                    'name': 'compute',
                    'items': {
                        'instances': '',
                        'cores': '',
                        'ram': '',
                    }
                },
            ],
            "sdn1": [
                {
                    'name': 'network',
                    'items': {
                        'loadbalancers': 'loadbalancer',
                        'floating_ips': 'floatingip'
                    }
                },
            ]
        }

    def get_project_quota(self, project_id, session=None, impersonate=False):
        if not session:
            if impersonate:
                namespace = argparse.Namespace(project_id=project_id)
                session = self.get_session(self.cloud, namespace=namespace)
            else:
                session = self.session

        region = self.find_region_project(project_id)
        quota = {region: {}, 'sdn1': {}}
        current = {region: {}, 'sdn1': {}}

        # Retrieve compute quota
        nova_quota = self.get_nova_project_quota(
            project_id=project_id,
            region=region,
            session=session,
            detail=True
        )
        quota[region]['compute'] = {
            'instances': nova_quota.instances['limit'],
            'cores': nova_quota.cores['limit'],
            'ram': nova_quota.ram['limit'] / 1024
        }
        current[region]['compute'] = {
            'instances': nova_quota.instances['in_use'],
            'cores': nova_quota.cores['in_use'],
            'ram': nova_quota.ram['in_use'] / 1024
        }

        # Retrieve Cinder quota
        cinder_quota = self.get_cinder_project_quota(
            project_id=project_id,
            region=region,
            session=session,
            usage=True
        )
        quota[region]['blockstorage'] = {}
        current[region]['blockstorage'] = {}
        volume_types = [
            s.name for s in self.get_volume_types(
                region=region,
                session=session,
                show_all=True)]
        for vol_type in volume_types:
            quota[region]['blockstorage'][vol_type] = {
                'volumes': getattr(
                    cinder_quota,
                    'volumes_%s' % vol_type)['limit'],
                'gigabytes': getattr(
                    cinder_quota,
                    'gigabytes_%s' % vol_type)['limit']
            }
            current[region]['blockstorage'][vol_type] = {
                'volumes': getattr(
                    cinder_quota,
                    'volumes_%s' % vol_type)['in_use'],
                'gigabytes': getattr(
                    cinder_quota,
                    'gigabytes_%s' % vol_type)['in_use']
            }

        # Retrieve Manila quota
        manila_quota = self.get_manila_project_quota(
            project_id=project_id,
            region=region,
            session=session,
            detail=True
        )

        share_types = [
            s.name for s in self.get_share_types(
                region=region,
                session=session,
                show_all=True)]
        quota[region]['fileshare'] = {}
        current[region]['fileshare'] = {}
        for share_type in share_types:
            quota[region]['fileshare'][share_type] = {
                'shares': manila_quota[share_type].shares['limit'],
                'gigabytes': manila_quota[share_type].gigabytes['limit']
            }
            current[region]['fileshare'][share_type] = {
                'shares': manila_quota[share_type].shares['in_use'],
                'gigabytes': manila_quota[share_type].gigabytes['in_use'],
            }

        # Retrieve S3 quota
        buckets, gigabytes = self.get_s3_project_quota(
            project_id=project_id,
            region=region)
        quota[region]['object'] = {
            'buckets': buckets,
            'gigabytes': gigabytes
        }

        buckets_used, gigabytes_used = self.get_s3_project_usage(
            project_id=project_id,
            region=region)
        current[region]['object'] = {
            'buckets': buckets_used,
            'gigabytes': gigabytes_used
        }

        # Retrieve Network quota
        lb_quota = self.get_loadbalancer_quota(
            project_id=project_id,
            session=session,
            region='sdn1'
        )
        quota['sdn1']['network'] = {
            'loadbalancers': lb_quota['loadbalancer'],
            'floating_ips': lb_quota['floatingip']
        }
        current['sdn1']['network'] = {
            'loadbalancers': 0,
            'floating_ips': 0
        }

        return {'quota': quota, 'current': current}

    def set_project_quota(self, project_id, quota):
        """Set project quotas."""
        for region, region_quota in quota.items():
            # Update nova quota
            if 'compute' in region_quota.keys():
                amount_ram = None
                if region_quota['compute'].get('ram'):
                    if int(region_quota['compute']['ram']) > -1:
                        amount_ram = int(region_quota['compute']['ram']) * 1024
                    else:
                        amount_ram = -1
                self.set_nova_project_quota(
                    project_id,
                    region_quota['compute'].get('cores', None),
                    region_quota['compute'].get('instances', None),
                    amount_ram,
                    region)

            # Update cinder quota
            if 'blockstorage' in region_quota.keys():
                SNAPSHOT_QUOTA_PER_TYPE = -1
                for volume_type, q in region_quota['blockstorage'].items():
                    self.update_quota_cinder(
                        project_id,
                        q.get('volumes', None),
                        q.get('gigabytes', None),
                        SNAPSHOT_QUOTA_PER_TYPE,
                        volume_type,
                        region)

            # Update manila quota
            if 'fileshare' in region_quota.keys():
                for share_type, q in region_quota['fileshare'].items():
                    self.update_quota_manila(
                        project_id,
                        q.get('shares', None),
                        q.get('gigabytes', None),
                        share_type,
                        region)

            # Update load balancer quotas
            if 'network' in region_quota.keys():
                self.set_loadbalancer_quota(
                    project_id=project_id,
                    loadbalancer=region_quota['network'].get(
                        'loadbalancers', None),
                    region=region
                )

            # Update radosgw quota
            if 'object' in region_quota.keys():
                self.set_s3_project_quota(
                    project_id=project_id,
                    containers=region_quota['object'].get('buckets', 0),
                    size_gb=region_quota['object'].get('gigabytes', 0),
                    region=region)

    def mergeQuotaMetadata(self, data, metadata={}):
        if 'quota' not in metadata.keys():
            metadata['quota'] = {}
        for region, services in self.get_regions_and_services().items():
            quota_region = {}
            if region in metadata['quota'].keys():
                quota_region = metadata['quota'][region]
            for service in services:
                service_quota = {}
                if service['name'] in quota_region.keys():
                    service_quota = quota_region[service['name']]
                if 'types' not in service.keys():
                    for key, value in service['items'].items():
                        if hasattr(data, value):
                            service_quota[key] = int(getattr(data, value))
                else:
                    for resource_type, prefix in service['types'].items():
                        type_quota = {}
                        if resource_type in service_quota.keys():
                            type_quota = service_quota[resource_type]
                        for key, value in service['items'].items():
                            if hasattr(data, value % prefix):
                                type_quota[key] = int(getattr(
                                    data, value % prefix))
                        if type_quota:
                            service_quota[resource_type] = type_quota
                if service_quota:
                    quota_region[service['name']] = service_quota
            if quota_region:
                metadata['quota'][region] = quota_region
        return metadata

    def service_quota_iterator(self, callback, show_all=False, **kwargs):
        for region, services in self.get_regions_and_services(
                show_all=show_all).items():
            for service in services:
                if 'types' not in service.keys():
                    for key, value in service['items'].items():
                        callback(
                            region=region,
                            service=service['name'],
                            key=key,
                            resource=None,
                            **kwargs
                        )
                else:
                    for resource, _ in service['types'].items():
                        for key, value in service['items'].items():
                            callback(
                                region=region,
                                service=service['name'],
                                key=key,
                                resource=resource,
                                **kwargs
                            )

    def filter_quota(self, quota, current):
        for region, services in self.get_regions_and_services().items():
            if (region in quota.keys() and region in current.keys()):
                for service in services:
                    name = service['name']
                    if (name in quota[region].keys()
                            and name in current[region].keys()):
                        self._filter_service_quota(
                            region, service, quota, current)
                if not quota[region]:
                    del quota[region]
        return quota

    def _filter_service_quota(self, region, service, quota, current):
        name = service['name']
        if 'types' not in service.keys():
            for key, value in service['items'].items():
                if (key in quota[region][name].keys()
                        and key in current[region][name].keys()
                        and quota[region][name][key]
                        == current[region][name][key]):
                    del quota[region][name][key]
        else:
            for res, _ in service['types'].items():
                if (res in quota[region][name].keys()
                        and res in current[region][name].keys()):
                    for key, value in service['items'].items():
                        if (key in quota[region][name][res].keys()
                                and key in current[region][name][res].keys()
                                and quota[region][name][res][key]
                                == current[region][name][res][key]):
                            del quota[region][name][res][key]
                    if not quota[region][name][res]:
                        del quota[region][name][res]
        if not quota[region][name]:
            del quota[region][name]
