import logging

from ccitools.common import ssh_executor
from ccitools.errors import SSHException
from ccitools.verify import base

# configure logging
logger = logging.getLogger(__name__)

RENEGOTIATE_SPEED_SCRIPT = '''
adapter=$(brctl show | grep -v "bridge name" | grep -v nic | grep -v tap | cut -f6);
if [ -z "$adapter" ]; then
  adapter=$(route | grep "^default" | grep -o "[^ ]*$" | head -n1);
fi
ethtool -r $adapter
'''  # noqa: E501


class NetworkSpeedAction(base.ActionBase):

    def __init__(self, host, delay=120):
        super(NetworkSpeedAction, self).__init__(host, delay)
        self.pattern = (r'.*\/exec_compute\/gauge-network_speed')

    def try_fix(self):
        """Implement the fix for the exception thrown in collectd."""
        # Try to start the renegotiate the network speed in the machine
        try:
            logger.info("Triggering renegotiate network speed")
            ssh_executor(self.host, RENEGOTIATE_SPEED_SCRIPT)
        except SSHException:
            pass

    def check_alarm_state(self, after_try=False):
        ret = super(NetworkSpeedAction, self).check_alarm_state(after_try)
        if ret:
            self.reason = (
                " - %s --> Renegotiate network speed"
                "has not cleaned the alarm." % self.host
            )
        return ret
