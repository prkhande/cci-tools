import json
import logging

from ccitools.common import ssh_executor
from ccitools.verify import base


# configure logging
logger = logging.getLogger(__name__)


class PuppetAgentAction(base.ActionBase):

    def __init__(self, host):
        super(PuppetAgentAction, self).__init__(host)
        self.pattern = r'.*\/puppet\/puppet_time'

    def try_fix(self):
        """Implement the fix for the exception thrown in collectd."""
        pass

    def check_alarm_state(self, after_try=False):
        ret = super(PuppetAgentAction, self).check_alarm_state(after_try)
        if ret:
            message = self.puppet_agent_disabled_message()
            self.reason = " - %s --> Puppet was manually disabled. " \
                          "Reason: '%s'\n" % (self.host, message)
        return ret

    def puppet_agent_disabled_message(self):
        out, err = ssh_executor(
            self.host,
            "cat $(puppet config print vardir)/state/agent_disabled.lock")
        if out:
            return json.loads(out[0])['disabled_message']
        return None


class PuppetAgentRunErrorsAction(base.ActionBase):

    def __init__(self, host):
        super(PuppetAgentRunErrorsAction, self).__init__(host)
        self.pattern = r'.*\/puppet\/resources-failed'

    def try_fix(self):
        # There is no fix for this alarm, ignoring it.
        pass
