import logging

from ccitools.verify import base

# configure logging
logger = logging.getLogger(__name__)


class SwapFullAction(base.ActionBase):
    def __init__(self, host):
        super(SwapFullAction, self).__init__(host)
        self.pattern = r'.*\/swap-swapfile\/percent-free'

    def try_fix(self):
        # There is no fix for this alarm, ignoring it.
        pass
